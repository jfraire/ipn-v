#!/bin/bash

rm -rf $1

mkdir $1

mkdir $1/nodes
mkdir $1/planets

cp ../design/Assets/Data/JSON/contactPlan.json $1
cp ../design/Assets/Data/JSON/config.json $1

ls -1 ../design/Assets/Data/JSON/nodes/*.json | xargs realpath | xargs cp -t $1/nodes
ls -1 ../design/Assets/Data/JSON/planets/*.json | xargs realpath | xargs cp -t $1/planets

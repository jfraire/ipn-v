var MyPlugin = {
        SendWaitingForConfig: function()
            {
                //window.alert("waitingForConfig");
                //Session.set("simulationStart", true);
                Session.set("simulationState", "waitingForConfig");

            },
            
        SendReady: function()
        {
            //window.alert("ready");
            Session.set("simulationState", "ready");
        },
        
        UpdateTime: function(time)
        {
            //window.alert("Hello, world!");
            Session.set("currentTimeStamp", time);
            document.getElementById("timeBar").value = time;

        },   
        
        SendOriginShiftUpdate: function(originShiftState)
        {
            //Session.set("onGoingOriginShift", originShiftState);        
            document.getElementById("timeBar").inert = originShiftState;
        },
         
    };

mergeInto(LibraryManager.library, MyPlugin);

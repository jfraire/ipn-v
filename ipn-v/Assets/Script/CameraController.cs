﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// VR: Library
using UnityEngine.XR;

public class CameraController : MonoBehaviour
{
    // Call to other components
    IPNvController ipnvController;
    public UITimeSlider timeSlider;
    UIFiltersController uiFiltersController;
    UIHelpController uiHelpController;

    // Canvas
    public Canvas uiFiltersCanvas;
    public Canvas uiHelpCanvas;

    public GameObject lookingAtObject = null;
    public GameObject lastObjectLookedAt = null;
    private float horizontalRotation = 0.0f;
    private float verticalRotation = 0.0f;
    private float rotationSensitivity = 14.0f;
    private float zoomSpeed = 100.0f;
    public bool zooming = false;
    private float minZoomDistance;
    private float currentScale;
    private float zoomAmount;
    public GameObject lastObjectReached;
    public bool freeFly = false;
    private Quaternion lastRotation = Quaternion.identity;

    // Continuous movement
    public bool continuousRotateLeft = false;
    public bool continuousRotateRight = false;
    public bool continuousZoomIn = false;
    public bool continuousZoomOut = false;

    // VR: Controllers
    private InputDevice rightController;
    private InputDevice leftController;

    // Camera transition
    public bool cameraIsZoomingDuringTransition = false;

    void Start()
    {
        // VR: Initialize the controllers for Oculus Quest
        StartCoroutine(CheckForRightController());
        StartCoroutine(CheckForLeftController());

        InitializeCamera();
        zoomSpeed = 3f;
        rotationSensitivity = 10f;
    }

    private void InitializeCamera()
    {
        ipnvController = GameObject.Find("IPNvController").GetComponent<IPNvController>();
        currentScale = ipnvController.currentScale;

        uiFiltersController = uiFiltersCanvas.GetComponent<UIFiltersController>();
        uiHelpController = uiHelpCanvas.GetComponent<UIHelpController>();

        horizontalRotation = transform.rotation.y;
        verticalRotation = transform.rotation.x;
        lastRotation = transform.rotation;
    }

    // VR: Initialize the right controller for Oculus Quest
    private IEnumerator CheckForRightController()
    {
        while (!rightController.isValid) // Poll until we get a valid controller
        {
            List<InputDevice> devices = new List<InputDevice>();
            InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.Right | InputDeviceCharacteristics.Controller, devices);
            
            foreach (var device in devices)
            {
                //Debug.Log("Connected device: " + device.name + ", characteristics: " + device.characteristics);
                rightController = device; // Assign the right controller when found
            }

            yield return new WaitForSeconds(1.0f); // Check again in 1 second
        }

        Debug.Log("***********[IPN]*********** Axis Right controller detected: " + rightController.name);
    }
    // VR: Initialize the right controller for Oculus Quest
    private IEnumerator CheckForLeftController()
    {
        while (!leftController.isValid) // Poll until we get a valid controller
        {
            List<InputDevice> devices = new List<InputDevice>();
            InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.Left | InputDeviceCharacteristics.Controller, devices);
            
            foreach (var device in devices)
            {
                //Debug.Log("Connected device: " + device.name + ", characteristics: " + device.characteristics);
                leftController = device; // Assign the right controller when found
            }

            yield return new WaitForSeconds(1.0f); // Check again in 1 second
        }

        Debug.Log("***********[IPN]*********** Axis Left controller detected: " + leftController.name);
    }

    void Update()
    {
        if (ipnvController.currentStepTime < ipnvController.endTime && ipnvController.simulationStarted)
        {
            HandleInput();
        }
    }

    private void LateUpdate()
    {
        // If an object is centered and the filter window isn't open
        if (cameraIsZoomingDuringTransition == false)
        {
            HandleCameraNormalMovement();
        }
        // If an Origin Shift transition is ongoing
        else if (cameraIsZoomingDuringTransition == true)
        {
            HandleCameraTransitionZoom();
        }
    }

    private void HandleInput()
    {
        lastObjectLookedAt = lookingAtObject;

        //if (Application.isEditor)
        //{
        //    ToggleFreeFlyMode();
        //}

        // TODO reactivate free fly with new Origin Shift
        //ToggleFreeFlyMode();
        
    }


    //TODO understand why camera movement between nodes can be jittery if the simulation is running
    /// <summary>
    /// Makes sure the camera is looking at the object we are centering.
    /// </summary>
    public void KeepObjectInFocusDuringTransition()
    {
        Vector3 direction = ipnvController.centeredObjectOriginShift.transform.position - transform.position;

        Quaternion targetRotation = Quaternion.LookRotation(direction);

        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, (float)(ipnvController.elapsedTimeTransitionOriginShift / ipnvController.transitionTimeOriginShift));
    }

    /// <summary>
    /// Handles the camera movement during an Origin Shift transition to the desired centered object.
    /// The camera smoothly rotates and adjusts its position (zooming in or out) depending on the distance
    /// of the centered object from the origin.
    /// </summary>
    //private void HandleCameraTransitionMovement()
    //{
    //    // Check if the centered object origin shift is active
    //    if (ipnvController.centeredObjectOriginShift)
    //    {
    //        // Threshold angle to determine when the rotation is considered complete
    //        float threshold = 0.01f;

    //        // Speed at which the camera rotates towards the target position
    //        float rotationSpeed = 5f;

    //        // Calculate the direction to the centered object
    //        Vector3 directionToTarget = ipnvController.centeredObjectOriginShift.transform.position - transform.position;

    //        Vector3 zoomInPosition = new(5,5,5);
    //        Vector3 zoomOutPosition = new(50,50,50);

    //        if (ipnvController.centeredObjectOriginShift.tag == "planet")
    //        {
    //            // Calculate zoom positions based on the planet's radius and current scale
    //            zoomInPosition = -transform.forward * (ipnvController.centeredObjectOriginShift.GetComponent<PlanetController>().Radius / currentScale * 6f);
    //            zoomOutPosition = -transform.forward * (ipnvController.centeredObjectOriginShift.GetComponent<PlanetController>().Radius / currentScale * 100f);
    //        }

    //        //if (ipnvController.centeredObjectOriginShift.tag == "bundle")
    //        //{
    //        //    // Calculate zoom positions based on the planet's radius and current scale
    //        //    zoomInPosition = -transform.forward * (ipnvController.centeredObjectOriginShift.GetComponent<PlanetController>().Radius / currentScale * 6f);
    //        //    zoomOutPosition = -transform.forward * (ipnvController.centeredObjectOriginShift.GetComponent<PlanetController>().Radius / currentScale * 1000f);
    //        //}

    //        //if (ipnvController.centeredObjectOriginShift.tag == "node")
    //        //{
    //        //    PlanetController centralObject = ipnvController.centeredObjectOriginShift.GetComponent<NodeController>().CentralObject.GetComponent<PlanetController>();

    //        //    // Calculate zoom positions based on the planet's radius and current scale
    //        //    zoomInPosition = -transform.forward * (centralObject.Radius / currentScale * 6f);
    //        //    zoomOutPosition = -transform.forward * (centralObject.Radius / currentScale * 1000f);
    //        //}

    //        // Calculate the rotation needed to face the target centered object
    //        Quaternion targetRotation = Quaternion.LookRotation(directionToTarget);

    //        // Smoothly interpolate the camera's rotation towards the target rotation
    //        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);

    //        // Determine whether to zoom in or out based on the distance to the centered object
    //        if (Vector3.Distance(ipnvController.centeredObjectOriginShift.transform.position, Vector3.zero) > 10)
    //        {
    //            // Zoom out if the centered object is far from the origin
    //            transform.position = Vector3.Lerp(transform.position, zoomOutPosition, rotationSpeed * Time.deltaTime);
    //        }
    //        else
    //        {
    //            // Zoom in if the centered object is close to the origin
    //            transform.position = Vector3.Lerp(transform.position, zoomInPosition, rotationSpeed * Time.deltaTime);
    //        }

    //        // Check if the camera's rotation is complete
    //        if (Vector3.Distance(ipnvController.centeredObjectOriginShift.transform.position, Vector3.zero) < threshold)
    //        {
    //            // Check if the camera has reached the zoom-in position
    //            if (Vector3.Distance(transform.position, zoomInPosition) < 0.01)
    //            {
    //                Debug.Log("Camera rotation is complete.");

    //                // Transition is over, move to normal mode
    //                cameraIsTransitioning = false;
    //                lookingAtObject = ipnvController.centeredObjectOriginShift;
    //            }
    //        }
    //    }
    //}


    /// <summary>
    /// Handles the camera zoom in / zoom out during an Origin Shift transition to the desired centered object.
    /// </summary>
    private void HandleCameraTransitionZoom()
    {
        // Check if the centered object origin shift is active
        if (ipnvController.centeredObjectOriginShift)
        {
            float zoomSpeed = 5f;

            Vector3 zoomInPosition = new(5, 5, 5);
            Vector3 zoomOutPosition = new(50, 50, 50);

            if (ipnvController.centeredObjectOriginShift.tag == "planet")
            {
                // Calculate zoom positions based on the planet's radius and current scale
                zoomInPosition = -transform.forward * (ipnvController.centeredObjectOriginShift.GetComponent<PlanetController>().Radius / currentScale * 6f);
                zoomOutPosition = -transform.forward * (ipnvController.centeredObjectOriginShift.GetComponent<PlanetController>().Radius / currentScale * 100f);
            }

            // Determine whether to zoom in or out based on the distance to the centered object
            if (Vector3.Distance(ipnvController.centeredObjectOriginShift.transform.position, Vector3.zero) > 10)
            {
                // Zoom out if the centered object is far from the origin
                transform.position = Vector3.Lerp(transform.position, zoomOutPosition, zoomSpeed * Time.deltaTime);
            }
            else
            {
                // Zoom in if the centered object is close to the origin
                transform.position = Vector3.Lerp(transform.position, zoomInPosition, zoomSpeed * Time.deltaTime);
            }

            // Check if the camera has reached the zoom-in position
            if (Vector3.Distance(transform.position, zoomInPosition) < 0.01)
            {
                // Transition is over, move to normal mode
                cameraIsZoomingDuringTransition = false;
                lookingAtObject = ipnvController.centeredObjectOriginShift;

                Debug.Log("***********[IPN]*********** Camera zoom in is complete. Looking at: " + lookingAtObject.name);
            }
        }
    }


    /// <summary>
    /// Handles the camera movement when an object is centered (no ongoing Origin Shift transition)
    /// </summary>
    private void HandleCameraNormalMovement()
    {
        if (lookingAtObject && !freeFly)
        {
            if (!timeSlider.isDragging)
            {
                // Only let users move around if there are no UI windows open
                if (!uiFiltersController.isFilterWindowOpen && !uiHelpController.isHelpWindowOpen)
                {
                    HandleUserRotation();
                    HandleUserZooming();
                }

                // Continue continuous movement even if a UI window is open
                HandleContinuousMovement();
            }
        }

        if (freeFly)
        {
            HandleFreeFlyMovement();
        }
    }

    private void HandleContinuousMovement()
    {
        float continuousRotationSpeed = 5.0f;
        float continuousZoomSpeed = 1.0f; 

        if (continuousRotateLeft)
        {
            transform.RotateAround(lookingAtObject.transform.position, Vector3.up, -continuousRotationSpeed * Time.deltaTime);
        }
        if (continuousRotateRight)
        {
            transform.RotateAround(lookingAtObject.transform.position, Vector3.up, continuousRotationSpeed * Time.deltaTime);
        }
        if (continuousZoomIn)
        {
            // Calculate the minimum zoom distance based on the object's radius and current scale
            float objectRadius = 0.01f;
            if (lookingAtObject.GetComponent<PlanetController>() != null)
                objectRadius = lookingAtObject.GetComponent<PlanetController>().Radius;
            minZoomDistance = objectRadius / currentScale * 2.2f;


            // Calculate the potential new position after zooming
            Vector3 newPotentialPosition = transform.position + transform.forward * continuousZoomSpeed * Time.deltaTime;

            // Calculate the distance from the new potential position to the object
            float newDistanceToObject = Vector3.Distance(newPotentialPosition, lookingAtObject.transform.position);

            if (newDistanceToObject >= minZoomDistance)
                transform.position = newPotentialPosition;
            else
            {
                continuousZoomIn = false;
            }
        }
        if (continuousZoomOut)
        {
            transform.position -= transform.forward * continuousZoomSpeed * Time.deltaTime;
        }
        if (continuousRotateLeft || continuousRotateRight || continuousZoomIn || continuousZoomOut)
        {
            transform.LookAt(lookingAtObject.transform);
        }
    }

    // Toggle rotation and zoom (from button)
    public void ToggleContinuousRotationLeft()
    {
        continuousRotateRight = false; // Disable the other
        continuousRotateLeft = !continuousRotateLeft;
    }
    public void ToggleContinuousRotationRight()
    {
        continuousRotateLeft = false; // Disable the other
        continuousRotateRight = !continuousRotateRight;
    }
    public void ToggleContinuousZoomIn()
    {
        continuousZoomOut = false; // Disable the other
        continuousZoomIn = !continuousZoomIn;
    }
    public void ToggleContinuousZoomOut()
    {
        continuousZoomIn = false; // Disable the other
        continuousZoomOut = !continuousZoomOut;
    }

    private void HandleUserZooming()
    {
        // Calculate the minimum zoom distance based on the object's radius and current scale
        float objectRadius = 0.01f;
        if (lookingAtObject.tag == "planet")
        {
            objectRadius = lookingAtObject.GetComponent<PlanetController>().Radius;
        }
        else if (lookingAtObject.tag == "bundle" || lookingAtObject.tag == "node")
        {
            objectRadius = currentScale/3;
        }
        minZoomDistance = objectRadius / currentScale * 2.2f;

        // Handle zooming via scroll wheel and right-click drag
        float scrollWheel = Input.GetAxis("Mouse ScrollWheel");
        float mouseZoom = Input.GetButton("Fire2") ? -Input.GetAxis("Mouse Y") * zoomSpeed : 0;
        zoomAmount = (scrollWheel + mouseZoom) * zoomSpeed * objectRadius / currentScale * 3;

        // Handle zooming via keyboard input
        if (Input.GetKey(KeyCode.J))
            zoomAmount -= 2000;
        if (Input.GetKey(KeyCode.U))
            zoomAmount += 2000;
        // Increase zoom speed if Shift is held down
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            zoomAmount *= 100;

        // // VR: Handle zooming via left VR controller
        if (leftController.isValid)
        {
            Vector2 leftStickInput;
            if (leftController.TryGetFeatureValue(CommonUsages.primary2DAxis, out leftStickInput))
            {
                float controllerZoom = leftStickInput.y;
                
                // Calculate a scaling factor based on distance to control zoom speed.
                // Adjust the exponent (e.g., 1.5f or 2f... 10f?) to change the sensitivity of the zoom scaling.
                float distanceFactor = Mathf.Pow(objectRadius / currentScale, 1.1f);

                // Adjust the zoom amount with the distance factor
                zoomAmount += controllerZoom * zoomSpeed * distanceFactor * 0.2f;
                // zoomAmount += (controllerZoom) * (zoomSpeed/5) * objectRadius / currentScale * 3;
            }
        }

        // Calculate the potential new position after zooming
        Vector3 newPotentialPosition = transform.position + transform.forward * zoomAmount;

        // Calculate direction vectors before and after the potential zoom
        Vector3 oldDirectionToObject = (lookingAtObject.transform.position - transform.position).normalized;
        Vector3 newDirectionToObject = (lookingAtObject.transform.position - newPotentialPosition).normalized;

        // Calculate the distance from the new potential position to the object
        float newDistanceToObject = Vector3.Distance(newPotentialPosition, lookingAtObject.transform.position);

        // Check if the zoom would cause the camera to cross through or face away from the object
        if (Vector3.Dot(oldDirectionToObject, newDirectionToObject) <= 0 || newDistanceToObject < minZoomDistance)
        {
            // If so, adjust the position to stay at the minimum zoom distance
            newPotentialPosition = lookingAtObject.transform.position - oldDirectionToObject * minZoomDistance;
        }

        // Update the camera's position to the new potential position
        transform.position = newPotentialPosition;

        // Ensure the camera is pointing directly at the object
        //transform.LookAt(lookingAtObject.transform);      
    }

    private void HandleUserRotation()
    {
        // Handle mouse input
        if (Input.GetButton("Fire1"))
        {
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");

            transform.RotateAround(lookingAtObject.transform.position, Vector3.up, mouseX * rotationSensitivity);
            transform.RotateAround(lookingAtObject.transform.position, transform.right, -mouseY * rotationSensitivity);

            lastRotation = transform.rotation;
        }

        // VR: Handle VR controller input
        if (rightController.isValid)
        {
            Vector2 rightStickInput;
            if (rightController.TryGetFeatureValue(CommonUsages.primary2DAxis, out rightStickInput))
            {
                // Debug.Log("***********[IPN]*********** InputX " + rightStickInput.x + " InputY " + rightStickInput.y);
                float mouseX = rightStickInput.x;
                float mouseY = rightStickInput.y;

                transform.RotateAround(lookingAtObject.transform.position, Vector3.up, mouseX * (rotationSensitivity/5));
                transform.RotateAround(lookingAtObject.transform.position, transform.right, -mouseY * (rotationSensitivity/5));

                lastRotation = transform.rotation;

            }
        }
    }

    private void ToggleFreeFlyMode()
    {
        if (Input.anyKeyDown)
        {
            freeFly = Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) ||
                      Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.R) || Input.GetKey(KeyCode.F) ||
                      Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.E) || Input.GetKey(KeyCode.T) ||
                      Input.GetKey(KeyCode.G) || Input.GetKey(KeyCode.J) || Input.GetKey(KeyCode.U);
        }
    }

    private void HandleFreeFlyMovement()
    {
        float rotationSensitivity = 1.0f;
        float moveSpeed = 10.0f;
        float strafeSpeed = 5.0f;

        float currentMoveSpeed = moveSpeed;
        float currentStrafeSpeed = strafeSpeed;

        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            float speedMultiplier = 100;
            currentMoveSpeed *= speedMultiplier;
            currentStrafeSpeed *= speedMultiplier;
        }

        float moveAmount = currentMoveSpeed * Time.deltaTime;
        float strafeAmount = currentStrafeSpeed * Time.deltaTime;

        if (Input.GetKey(KeyCode.U))
            transform.Translate(Vector3.up * moveAmount, Space.World);
        if (Input.GetKey(KeyCode.J))
            transform.Translate(Vector3.down * moveAmount, Space.World);
        if (Input.GetKey(KeyCode.F))
            transform.Translate(Vector3.left * strafeAmount, Space.Self);
        if (Input.GetKey(KeyCode.H))
            transform.Translate(Vector3.right * strafeAmount, Space.Self);
        if (Input.GetKey(KeyCode.T))
            transform.Translate(Vector3.forward * moveAmount, Space.Self);
        if (Input.GetKey(KeyCode.G))
            transform.Translate(Vector3.back * moveAmount, Space.Self);

        if (Input.GetKey(KeyCode.A))
            transform.Rotate(Vector3.up, -rotationSensitivity);
        if (Input.GetKey(KeyCode.D))
            transform.Rotate(Vector3.up, rotationSensitivity);
        if (Input.GetKey(KeyCode.W))
            transform.Rotate(Vector3.left, rotationSensitivity);
        if (Input.GetKey(KeyCode.S))
            transform.Rotate(Vector3.left, -rotationSensitivity);
    }
}

﻿using UnityEngine;
using System.Collections;

public class LinkController : MonoBehaviour
{
    public LineRenderer lineRendererData;
    public LineRenderer lineRendererLoS;
    IPNvController ipnvController;
    public IPNvController.Contact contact;

    public GameObject linkHeadPrefab;
    private GameObject linkHead;
    private GameObject linkTail;

    public double lightTravelTime = 0;

    private float currentScale;

    private float baseWidth1 = 0.01f;
    private float baseWidth2 = 0.001f;
    private Camera mainCamera;

    void Awake()
    {
        mainCamera = mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        ipnvController = GameObject.Find("IPNvController").GetComponent<IPNvController>();

        lineRendererData = GetComponent<LineRenderer>();
        //lineRendererData.material = new Material(Shader.Find("Universal Render Pipeline/Unlit"));

        // Create a new child GameObject for the Line-of-Sight LineRenderer
        GameObject childObject = new GameObject("Propagation");
        childObject.transform.SetParent(transform, false); // Set the current GameObject as the parent
        childObject.transform.localPosition = Vector3.zero; // Reset local position
        childObject.transform.localRotation = Quaternion.identity; // Reset local rotation
        childObject.transform.localScale = Vector3.one; // Reset local scale

        // Add the Line-of-Sight LineRenderer to the child GameObject
        lineRendererLoS = childObject.AddComponent<LineRenderer>();
        // Material lineMaterial = new Material(Shader.Find("Unlit/Color"));
        Material lineMaterial = new Material(Shader.Find("Universal Render Pipeline/Unlit"));
        Color lineColor = Color.gray;
        lineColor.a = 0.5f;
        lineMaterial.color = lineColor;
        lineMaterial.renderQueue = 3000; // Transparent queue
        lineRendererLoS.material = lineMaterial;

        // Set other properties for the Line-of-Sight LineRenderer
        lineRendererLoS.startWidth = baseWidth2;
        lineRendererLoS.endWidth = baseWidth2;
        lineRendererLoS.startColor = lineColor;
        lineRendererLoS.endColor = lineColor;

        // Link Head and Tail
        linkHead = Instantiate(linkHeadPrefab);
        linkHead.name = $"LinkHead_{gameObject.name}";
        linkTail = Instantiate(linkHeadPrefab);
        linkHead.name = $"LinkTail_{gameObject.name}";
    }

    private void Start()
    {
        currentScale = ipnvController.currentScale;

        UpdateLinePositions();
        UpdateLineWidths();
    }

    void LateUpdate()
    {
        UpdateLinePositions();
        UpdateLineWidths();
    }

    //TODO update with double positions
    /// <summary>
    /// Handle update of the link's position.
    /// </summary>
    /// <remarks>
    /// This function MUST be called in LateUpdate, since it MUST run after simulation time and origin shift
    /// have been updated.
    /// </remarks>
    void UpdateLinePositions()
    {
        // Store the shifted position of the source and destination 
        double[] sourcePosition = Util.SubtractArrays(contact.source.GetComponent<NodeController>().globalCurrentPosNonShifted, ipnvController.originShiftValue);
        double[] destinationPosition = Util.SubtractArrays(contact.destination.GetComponent<NodeController>().globalCurrentPosNonShifted, ipnvController.originShiftValue);

        // Line of Sight Link
        lineRendererLoS.SetPosition(0, Util.DoubleArrayToVector3(sourcePosition));
        lineRendererLoS.SetPosition(1, Util.DoubleArrayToVector3(destinationPosition));

        // Propagation
        float speedOfLight = 299792; // Speed of light in kms per second
        double distanceToCover = Util.DistanceBetweenDoubleArrays(sourcePosition, destinationPosition) * currentScale;
        lightTravelTime = distanceToCover / speedOfLight; // Time for light to travel in seconds

        //if (lightTravelTime < contact.duration){
        float elapsedTime = (float)(ipnvController.currentContinuousTime - contact.startTime); // Assuming contact.startTime is the creation time
        float propagationTailStartTime = (float)(contact.endTime);

        double headDistanceCovered = System.Math.Clamp(elapsedTime / lightTravelTime, 0, 1) * distanceToCover;
        double tailElapsedTime = elapsedTime - (float)(propagationTailStartTime - contact.startTime);
        double tailDistanceCovered = System.Math.Clamp(tailElapsedTime / lightTravelTime, 0, 1) * distanceToCover;

        double[] propagationHeadPosition = Util.LerpDouble(Util.SubtractArrays(contact.source.GetComponent<NodeController>().globalCurrentPosNonShifted, ipnvController.originShiftValue), Util.SubtractArrays(contact.destination.GetComponent<NodeController>().globalCurrentPosNonShifted, ipnvController.originShiftValue), headDistanceCovered / distanceToCover);
        double[] propagationTailPosition = Util.LerpDouble(Util.SubtractArrays(contact.source.GetComponent<NodeController>().globalCurrentPosNonShifted, ipnvController.originShiftValue), Util.SubtractArrays(contact.destination.GetComponent<NodeController>().globalCurrentPosNonShifted, ipnvController.originShiftValue), tailDistanceCovered / distanceToCover);

        lineRendererData.positionCount = 2;
        lineRendererData.SetPosition(0, Util.DoubleArrayToVector3(propagationTailPosition));
        lineRendererData.SetPosition(1, Util.DoubleArrayToVector3(propagationHeadPosition));

        linkHead.transform.position = Util.DoubleArrayToVector3(propagationHeadPosition);
        linkTail.transform.position = Util.DoubleArrayToVector3(propagationTailPosition);
    }

    void UpdateLineWidths()
    {
        float distancePenalization = 0.000001f; 

        Vector3 cameraPosition = mainCamera.transform.position;
        Vector3 closestPoint = GetClosestPointOnLine(contact.source.transform.position, contact.destination.transform.position, cameraPosition);
        float distanceToCamera = Vector3.Distance(cameraPosition, closestPoint);
        float adjustedWidth1 = baseWidth1 * distanceToCamera / (1f + distancePenalization * distanceToCamera);
        float adjustedWidth2 = baseWidth2 * distanceToCamera / (1f + distancePenalization * distanceToCamera);

        lineRendererData.startWidth = adjustedWidth1;
        lineRendererData.endWidth = adjustedWidth1;
        lineRendererLoS.startWidth = adjustedWidth2;
        lineRendererLoS.endWidth = adjustedWidth2;

        // Head Sphere
        distanceToCamera = Vector3.Distance(cameraPosition, linkHead.transform.position);
        adjustedWidth1 = baseWidth1 * 0.4f * distanceToCamera / (1f + distancePenalization * distanceToCamera);
        if (adjustedWidth1 * 2 < 0.1f)
            adjustedWidth1 = 0.05f;
        linkHead.transform.localScale = new Vector3(adjustedWidth1 * 2, adjustedWidth1 * 2, adjustedWidth1 * 2);

        // Tail Sphere
        distanceToCamera = Vector3.Distance(cameraPosition, linkTail.transform.position);
        adjustedWidth1 = baseWidth1 * 0.4f * distanceToCamera / (1f + distancePenalization * distanceToCamera);
        if (adjustedWidth1 * 2 < 0.1f)
            adjustedWidth1 = 0.05f;
        linkTail.transform.localScale = new Vector3(adjustedWidth1 * 2, adjustedWidth1 * 2, adjustedWidth1 * 2);
    }

    Vector3 GetClosestPointOnLine(Vector3 lineStart, Vector3 lineEnd, Vector3 point)
    {
        Vector3 lineDirection = lineEnd - lineStart;
        float lineLength = lineDirection.magnitude;
        lineDirection.Normalize();

        float projection = Vector3.Dot(point - lineStart, lineDirection);
        projection = Mathf.Clamp(projection, 0f, lineLength);

        return lineStart + projection * lineDirection;
    }

    public void ToggleLinkVisibility(bool visibility)
    {
        gameObject.GetComponent<Renderer>().enabled = visibility;
        lineRendererLoS.enabled = visibility;
        linkHead.GetComponent<Renderer>().enabled = visibility;
        linkTail.GetComponent<Renderer>().enabled = visibility;
    }

    public void ChangeLinkColor(Color color)
    {
        Material linkMat = transform.GetComponent<Renderer>().material;
        linkMat.SetColor("_EmissionColor", color * 2);
        linkMat.SetColor("_BaseColor", color);
    }

    void OnDestroy()
    {
        if (linkHead != null)
        {
            Destroy(linkHead);
            Destroy(linkTail);
        }
    }
}

﻿using System;
using UnityEngine;

public class Util
{
    public static double[] AddArrays(double[] arr1, double[] arr2)
    {
        if (arr1.Length != arr2.Length)
        {
            throw new ArgumentException("Arrays must have the same length");
        }

        double[] result = new double[arr1.Length];

        for (int i = 0; i < arr1.Length; i++)
        {
            result[i] = arr1[i] + arr2[i];
        }

        return result;
    }

    public static double[] SubtractArrays(double[] arr1, double[] arr2)
    {
        if (arr1.Length != arr2.Length)
        {
            Debug.Log(arr1.Length + " " + arr2.Length);
            throw new ArgumentException("Arrays must have the same length");
        }

        double[] result = new double[arr1.Length];

        for (int i = 0; i < arr1.Length; i++)
        {
            result[i] = arr1[i] - arr2[i];
        }

        return result;
    }

    // Convert a double[3] to a Vector3
    public static Vector3 DoubleArrayToVector3(double[] array)
    {
        if (array == null || array.Length != 3)
        {
            Debug.LogError("Array must have exactly 3 elements.");
            return Vector3.zero; // Return a default value in case of an error
        }

        return new Vector3((float)array[0], (float)array[1], (float)array[2]);
    }

    // Lerp function for double[3]
    public static double[] LerpDouble(double[] v1, double[] v2, double t)
    {
        // Ensure t is clamped between 0 and 1
        t = Math.Clamp(t, 0.0, 1.0);

        // Create a new array to store the result
        double[] result = new double[3];

        // Perform the Lerp calculation for each component
        result[0] = v1[0] + (v2[0] - v1[0]) * t;
        result[1] = v1[1] + (v2[1] - v1[1]) * t;
        result[2] = v1[2] + (v2[2] - v1[2]) * t;

        return result;
    }

    // Convert double[][3] to a Vector3[]
    public static Vector3[] ConvertToVector3Array(double[][] array)
    {
        // Ensure each inner array has exactly 3 elements
        foreach (var item in array)
        {
            if (item.Length != 3)
            {
                throw new ArgumentException("Each sub-array must contain exactly 3 elements.");
            }
        }

        // Convert double[][] to Vector3[]
        Vector3[] vectors = new Vector3[array.Length];
        for (int i = 0; i < array.Length; i++)
        {
            vectors[i] = new Vector3((float)array[i][0], (float)array[i][1], (float)array[i][2]);
        }

        return vectors;
    }

    public static double[] MultiplyArrayByFloat(double[] array, float multiplior)
    {

        double[] result = new double[array.Length];

        for (int i = 0; i < array.Length; i++)
        {
            result[i] = array[i] * multiplior;
        }

        return result;
    }

    public static double[] DivideArrayByFloat(double[] array, float divisor)
    {
        if (divisor == 0)
        {
            throw new DivideByZeroException("Divisor cannot be zero.");
        }

        double[] result = new double[array.Length];

        for (int i = 0; i < array.Length; i++)
        {
            result[i] = array[i] / divisor;
        }

        return result;
    }

    //public static double[] SmoothStepDouble(double[] from, double[] to, double t)
    //{
    //    // Ensure the arrays have exactly 3 elements
    //    if (from.Length != 3 || to.Length != 3)
    //    {
    //        throw new ArgumentException("Both 'from' and 'to' arrays must have exactly 3 elements.");
    //    }

    //    t = Clamp01(t); // Clamp t to be between 0 and 1
    //    t = t * t * (3.0 - 2.0 * t); // Apply the SmoothStep formula

    //    // Interpolate each element in the array
    //    double[] result = new double[3];
    //    for (int i = 0; i < 3; i++)
    //    {
    //        result[i] = from[i] + (to[i] - from[i]) * t;
    //    }

    //    return result;
    //}

    public static double[] SmoothLerpDouble(double[] from, double[] to, double t)
    {
        // Ensure the arrays have exactly 3 elements
        if (from.Length != 3 || to.Length != 3)
        {
            throw new System.ArgumentException("Both 'from' and 'to' arrays must have exactly 3 elements.");
        }

        t = Clamp01(t); // Clamp t to be between 0 and 1
        t = t * t * t * (t * (t * 6.0 - 15.0) + 10.0); // Apply the SmootherStep formula

        // Interpolate each element in the array
        double[] result = new double[3];
        for (int i = 0; i < 3; i++)
        {
            result[i] = from[i] + (to[i] - from[i]) * t;
        }

        return result;
    }

    // Cubic ease-in-out function for a smoother start and end
    public static double EaseInOutCubic(double t)
    {
        return t < 0.5 ? 4 * t * t * t : 1 - Mathf.Pow(-2 * (float)t + 2, 3) / 2;
    }

    public static double Clamp01(double value)
    {
        if (value < 0.0) return 0.0;
        if (value > 1.0) return 1.0;
        return value;
    }

    public static double DistanceBetweenDoubleArrays(double[] pointA, double[] pointB)
    {
        if (pointA.Length != 3 || pointB.Length != 3)
        {
            throw new ArgumentException("Both arrays must have exactly 3 elements.");
        }

        double xDiff = pointA[0] - pointB[0];
        double yDiff = pointA[1] - pointB[1];
        double zDiff = pointA[2] - pointB[2];

        return Math.Sqrt(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);
    }
}

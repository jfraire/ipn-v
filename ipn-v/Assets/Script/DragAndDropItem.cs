﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragAndDropItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    // Call to other components
    private FiltersController filtersController;

    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;
    private RectTransform contentRectTransform;
    private RectTransform placeholderModelRectTransform;
    private ScrollRect scrollRect;
    private Vector2 originalPosition;
    private int originalSiblingIndex;

    public FiltersController.ContactFilter filter;

    private GameObject placeholder;

    public Texture2D hoverCursor; // Drag and drop a custom cursor texture

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        scrollRect = GetComponentInParent<ScrollRect>();
        placeholderModelRectTransform = transform.Find("placeholder_size").GetComponent<RectTransform>();
        contentRectTransform = scrollRect.content;
    }

    private void Start()
    {
        filtersController = GameObject.Find("FiltersController").GetComponent<FiltersController>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (contentRectTransform.childCount <= 1) return;

        originalPosition = rectTransform.anchoredPosition;
        originalSiblingIndex = rectTransform.GetSiblingIndex();

        // Temporarily remove the item from the hierarchy
        transform.SetParent(contentRectTransform.parent);

        CreatePlaceholder();

        canvasGroup.blocksRaycasts = false; // Disable raycast blocking
        canvasGroup.alpha = 0.6f; // Optional: make the item semi-transparent
    }

    // Method to check if ScrollView is scrollable
    private bool IsScrollable()
    {
        return contentRectTransform.rect.height > scrollRect.viewport.rect.height;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (contentRectTransform.childCount <= 1) return;

        Vector2 localPoint;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(contentRectTransform, eventData.position, eventData.pressEventCamera, out localPoint);

        // Get the normalized vertical scroll position (1 at the top, 0 at the bottom)
        float normalizedPosition = scrollRect.verticalNormalizedPosition;

        // Invert the normalized position (0 at the top, 1 at the bottom)
        float invertedPosition = 1f - normalizedPosition;

        // Calculate the actual scroll position
        float contentHeight = scrollRect.content.rect.height;
        float viewportHeight = scrollRect.viewport.rect.height;

        // Calculate the position along the y-axis (content's anchored position)
        float targetYPosition = Mathf.Lerp(0f, contentHeight - viewportHeight, invertedPosition);

        if(IsScrollable())
         localPoint.y += targetYPosition;

        // Custom boundaries
        //float topBoundary = -12f;
        //float bottomBoundary = -262f;

        // Clamp position to within boundaries
        //float clampedY = Mathf.Clamp(localPoint.y, bottomBoundary, topBoundary);

        // Update position
        rectTransform.anchoredPosition = new Vector2(originalPosition.x, localPoint.y);

        // Update placeholder position
        UpdatePlaceholderPosition();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (contentRectTransform.childCount <= 1) return;

        // Reinsert the item back into the hierarchy
        transform.SetParent(contentRectTransform);

        canvasGroup.blocksRaycasts = true;
        canvasGroup.alpha = 1f;

        // Place the dragged item in the placeholder's position
        transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());

        // Destroy the placeholder
        Destroy(placeholder);

        LayoutRebuilder.ForceRebuildLayoutImmediate(contentRectTransform);

        // Update the priority list based on sibling indices
        UpdatePriorityList();
    }

    private void CreatePlaceholder()
    {
        placeholder = new GameObject("Placeholder");
        RectTransform placeholderRect = placeholder.AddComponent<RectTransform>();
        placeholderRect.sizeDelta = placeholderModelRectTransform.sizeDelta;
        placeholder.transform.SetParent(contentRectTransform);
        placeholder.transform.SetSiblingIndex(originalSiblingIndex);

        Image placeholderImage = placeholder.AddComponent<Image>();
        placeholderImage.color = new Color(1f, 1f, 1f, 0.2f); // Semi-transparent placeholder
    }

    private void UpdatePlaceholderPosition()
    {
        float closestDistance = float.MaxValue;
        int newSiblingIndex = placeholder.transform.GetSiblingIndex();
        for (int i = 0; i < contentRectTransform.childCount; i++)
        {
            Transform child = contentRectTransform.GetChild(i);
            Transform nextChild = null;
            if (i!= contentRectTransform.childCount -1)
                nextChild = contentRectTransform.GetChild(i+1);

            if (child == placeholder.transform || child == transform) continue;
            float nonAbsDistance = rectTransform.position.y - child.position.y;
            float distance = Mathf.Abs(rectTransform.position.y - child.position.y);
            float distanceAfter = float.MaxValue;
            float nonAbsDistanceAfter = float.MaxValue;
            if (nextChild != null)
            {
                distanceAfter = Mathf.Abs(rectTransform.position.y - nextChild.position.y);
                nonAbsDistanceAfter = rectTransform.position.y - nextChild.position.y;
            }

            if (distance < closestDistance)
            {
                closestDistance = distance;
  
                if (distance < distanceAfter && nonAbsDistance < 0 && newSiblingIndex < child.GetSiblingIndex())
                {
                    newSiblingIndex = child.GetSiblingIndex();
                }
                if(distanceAfter < distance && nonAbsDistanceAfter < 0 && newSiblingIndex < child.GetSiblingIndex())
                {
                    newSiblingIndex = nextChild.GetSiblingIndex();
                }
                if (distance < distanceAfter && nonAbsDistance > 0 && newSiblingIndex > child.GetSiblingIndex())
                {
                    newSiblingIndex = child.GetSiblingIndex();
                }
            }
        }

        // Handle the special case where dragged below the last element
        if (newSiblingIndex >= contentRectTransform.childCount - 1)
        {
            placeholder.transform.SetSiblingIndex(contentRectTransform.childCount - 1);
        }
        else if (newSiblingIndex > placeholder.transform.GetSiblingIndex())
        {
            placeholder.transform.SetSiblingIndex(newSiblingIndex);
        }
        else
        {
            placeholder.transform.SetSiblingIndex(newSiblingIndex);
        }
    }

    private void UpdatePriorityList()
    {
        if(filter.filterType == FiltersController.FilterType.Highlight)
        {
            filtersController.prioritiesHighlightFilters.Clear();
            foreach (Transform child in contentRectTransform)
            {
                if (child.gameObject == placeholder)
                    continue;
                child.gameObject.GetComponent<DragAndDropItem>();
                filtersController.prioritiesHighlightFilters.Add(child.gameObject.GetComponent<DragAndDropItem>().filter); // Add the children in the order of their sibling indices
            }
            filtersController.ReCalculateAllHighlightFilters();
        }

        if (filter.filterType == FiltersController.FilterType.Show || filter.filterType == FiltersController.FilterType.Hide)
        {
            filtersController.prioritiesVisibilityFilters.Clear();
            foreach (Transform child in contentRectTransform)
            {
                if (child.gameObject == placeholder)
                    continue;
                filtersController.prioritiesVisibilityFilters.Add(child.gameObject.GetComponent<DragAndDropItem>().filter); // Add the children in the order of their sibling indices
            }
            filtersController.ReApplyAllVisibilityFilters();
        }
    }
}
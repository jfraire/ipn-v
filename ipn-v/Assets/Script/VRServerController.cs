using System;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using UnityEngine;

public class VRServerController : MonoBehaviour
{
    IPNvController ipnvController;
    BundleController bundleController;

    private string serverIP = "192.168.100.243"; // Replace with your server's IP address
    public int serverPort = 10203; // Server port
    private TcpClient client;
    private NetworkStream stream;
    private bool connected = false;
    private bool done = false;

    private void Start()
    {
        ipnvController = GameObject.Find("IPNvController").GetComponent<IPNvController>();

        // Attempt to connect to the server
        // ConnectToServer();
        // Start the coroutine to try connecting every 5 seconds if not connected
        StartCoroutine(TryConnectToServer());
    }

    private void Update()
    {
        // Try to get a handler for the bundle
        if (bundleController == null){
            try
            {
                bundleController = GameObject.Find("Bundle_401_1").GetComponent<BundleController>();
            }
            catch
            {
                // there is no such bundle
            }

        }

        // Check if bundle arrived to Earth (1st hop)
        if (done == false && bundleController != null && bundleController.currentHopIndex >= 1){
            
            // Send "DONE" Signal
            SendDoneSignal();

            ipnvController.audioSourceArrived.Play();
            done = true;

            ipnvController.PauseSimulation(true);
            ipnvController.UpdateVRMessage("Your Bundle arrived!\nSimulation Stopped");
        }
    }

    private IEnumerator TryConnectToServer()
    {
        // while (!connected)
        // {
        //     ConnectToServer(); // Try to connect
        //     yield return new WaitForSeconds(5f); // Wait 5 seconds before the next attempt
        // }

        while (true)
        {
            if (!connected)
            {
                ConnectToServer();
            }
            else
            {
                // Check if the connection is still alive
                if (!IsConnectionAlive())
                {
                    Debug.LogWarning("***********[IPN]*********** Connection lost. Attempting to reconnect...");
                    connected = false;
                    client?.Close();
                    stream = null;
                }
            }
            yield return new WaitForSeconds(5f); // Check every 5 seconds
        }
    }

    private bool IsConnectionAlive()
    {
        try
        {
            if (client != null && client.Connected)
            {
                // Check the stream for readability
                return !(client.Client.Poll(0, SelectMode.SelectRead) && client.Available == 0);
            }
        }
        catch (Exception e)
        {
            Debug.LogWarning($"Connection check failed: {e.Message}");
        }
        
        return false;
    }


    private async void ConnectToServer()
    {
        try
        {
            client = new TcpClient();
           // Debug.Log($"***********[IPN]*********** Connecting to {serverIP}:{serverPort}...");
            ipnvController.UpdateVRMessage($"Connecting to {serverIP}:{serverPort}...");
            await client.ConnectAsync(serverIP, serverPort);
            Debug.Log("***********[IPN]*********** Connected to server");
            ipnvController.UpdateVRMessage("Connected to server");
            connected = true;

            // Get the stream to listen for incoming data
            stream = client.GetStream();

            // Send initial identifier message to the server
            SendInitialMessage();

            ListenForStartSignal();
        }
        catch (Exception e)
        {
            //  Debug.Log($"***********[IPN]*********** Failed to connect to server: {e.Message}");
            ipnvController.UpdateVRMessage($"Failed to connect to server");
        }
    }

    private void SendInitialMessage()
    {
        if (stream != null)
        {
            try
            {
                byte[] initialMessage = Encoding.ASCII.GetBytes("IPNv");
                stream.Write(initialMessage, 0, initialMessage.Length);
                Debug.Log($"***********[IPN]*********** Sent initial identifier message: IPNv");
            }
            catch (Exception e)
            {
                Debug.Log($"***********[IPN]*********** Failed to send initial message: {e.Message}");
            }
        }
    }

    private async void ListenForStartSignal()
    {
        try
        {
            byte[] buffer = new byte[5]; // Buffer to receive "START"

            while (true)
            {
                int bytesRead = await stream.ReadAsync(buffer, 0, buffer.Length);
                string message = Encoding.ASCII.GetString(buffer, 0, bytesRead);

                if (message == "START")
                {
                    Debug.Log("***********[IPN]*********** Received START signal from server");
                    ipnvController.UpdateVRMessage("Received START signal from server...");
                    ipnvController.audioSourceStart.Play();
                    
                    // Reset time
                    ipnvController.PickSpecificTime(ipnvController.startTime);
                    Debug.Log($"***********[IPN]*********** Time reset to {ipnvController.startTime}.");

                    // Delay TriggerUnpauseEvent by 3 seconds
                    Invoke(nameof(TriggerStart), 3f);
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError($"***********[IPN]*********** Error while listening for start signal: {e.Message}");
        }
    }

    private void TriggerStart()
    {
        done = false;
        ipnvController.PauseSimulation(false);
        ipnvController.UpdateVRMessage("Simulation started\nfrom server");
    }

    private async void SendDoneSignal()
    {
        try
        {
            byte[] doneMessage = Encoding.ASCII.GetBytes("DONE");
            await stream.WriteAsync(doneMessage, 0, doneMessage.Length);
            Debug.Log($"***********[IPN]*********** Sent DONE signal to server.");
        }
        catch (Exception e)
        {
            Debug.LogError($"***********[IPN]*********** Failed to send DONE signal: {e.Message}");
        }
    }

    private void OnApplicationQuit()
    {
        // Clean up connections when the application exits
        if (stream != null) stream.Close();
        if (client != null) client.Close();
    }
}
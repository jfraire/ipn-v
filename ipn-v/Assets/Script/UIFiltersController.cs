﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;

public class UIFiltersController : MonoBehaviour
{
    // Other components
    FiltersController filtersController;
    UIController uiController;
    UIHelpController uiHelpController;
    CanvasFader canvasFader;

    // Canvas
    public Canvas uiMainCanvas;
    public Canvas uiFiltersCanvas;
    public Canvas uiHelpCanvas;

    // UI elements
    public TMP_InputField sourceNodeInput;
    public TMP_InputField destinationNodeInput;
    public Button applyButton;
    public Button resetAllButton;
    //public Button resetOneButton;
    public Button closeFilterWindowButton;
    public GameObject filterTogglePrefab; // Assign the prefab in the Inspector
    public Transform listContentVisibilityFilters;         // Assign the Content GameObject in the Inspector
    public Transform listContentHighlightFilters;         // Assign the Content GameObject in the Inspector
    public TMP_Dropdown filterTargetDropdown;
    public TMP_Dropdown colorDropdown;
    public TMP_Dropdown showOrHideDropdown;

    //public TMP_Dropdown filterTypeDropdown;
    public TMP_Dropdown filterTypeDropdownExistingFilters;

    public bool isFilterWindowOpen = false;

    Dictionary<String, GameObject> filtersToggleDictionary = new();

    // Use this for initialization
    void Start()
    {
        filtersController = GameObject.Find("FiltersController").GetComponent<FiltersController>();

        uiController = uiMainCanvas.GetComponent<UIController>();
        uiHelpController = uiHelpCanvas.GetComponent<UIHelpController>();
        canvasFader = gameObject.GetComponent<CanvasFader>();

        applyButton.onClick.AddListener(OnApplyButtonClicked);
        resetAllButton.onClick.AddListener(OnResetAllButtonClicked);
        //resetOneButton.onClick.AddListener(OnResetOneButtonClicked);
        closeFilterWindowButton.onClick.AddListener(() => ToggleFiltersWindowVisibility(false));
        filterTypeDropdownExistingFilters.onValueChanged.AddListener(ToggleChoiceDropdown);
        filterTypeDropdownExistingFilters.onValueChanged.AddListener(ToggleShowAndHighlightFilterLists);
    }

    void ToggleShowAndHighlightFilterLists(int index)
    {
        if(index==0)
        {
            listContentVisibilityFilters.parent.parent.gameObject.SetActive(true);
            listContentHighlightFilters.parent.parent.gameObject.SetActive(false);
        }
        else if (index == 1)
        {
            listContentVisibilityFilters.parent.parent.gameObject.SetActive(false);
            listContentHighlightFilters.parent.parent.gameObject.SetActive(true);
        }
    }

    void ToggleChoiceDropdown(int index)
    {
        switch (index)
        {
            case 0:
                showOrHideDropdown.gameObject.SetActive(true);
                colorDropdown.gameObject.SetActive(false);
                break;
            case 1:
                colorDropdown.gameObject.SetActive(true);
                showOrHideDropdown.gameObject.SetActive(false);
                break;
            default:
                showOrHideDropdown.gameObject.SetActive(true);
                colorDropdown.gameObject.SetActive(false);
                break;
        }
    }

    Color GetColorFromDropdown(int index)
    {
        var color = index switch
        {
            // Red
            0 => Color.red,
            // Blue
            1 => Color.blue,
            // Yellow
            2 => Color.yellow,
            // Pink
            3 => new Color(1f, 0.41f, 0.71f),
            // Orange
            4 => new Color(1f, 0.647f, 0f),
            // Green
            5 => Color.green,
            // Black
            6 => Color.black,
            // Grey
            7 => Color.grey,
            // Purple
            8 => new Color(0.5f, 0f, 0.5f),
            _ => Color.white,// Default case, returns white if no match
        };
        return color;
    }
    

    private void OnApplyButtonClicked()
    {
        // Get the values from the inputs
        string sourceValue = sourceNodeInput.text;
        string destinationValue = destinationNodeInput.text;

        // Get the filter type from the dropdown
        int visibilityOrHighlightDropdown = filterTypeDropdownExistingFilters.value;

        FiltersController.FilterType filterType;

        switch (visibilityOrHighlightDropdown)
        {
            case 0:
                int showOrHideDropdownValue = showOrHideDropdown.value;
                if (showOrHideDropdownValue == 0)
                    filterType = FiltersController.FilterType.Show;
                else
                    filterType = FiltersController.FilterType.Hide;
                break;
            case 1:
                filterType = FiltersController.FilterType.Highlight;
                break;
            default:
                filterType = FiltersController.FilterType.Show;
                break;
        }

        bool onlySource = destinationValue == "" && sourceValue != "";
        bool onlyDestination = sourceValue == "" && destinationValue != "";

        // Get the node GameObjects
        GameObject sourceNode = GameObject.Find(sourceValue);
        GameObject destinationNode = GameObject.Find(destinationValue);

        FiltersController.ContactFilter filter = new FiltersController.ContactFilter(sourceNode, destinationNode, filterType);

        int colorIndex = colorDropdown.value;

        if (filterType == FiltersController.FilterType.Highlight)
            filter.color = GetColorFromDropdown(colorIndex);

        // If the source node or the destination node are not found, ask the user to input another ID
        if((sourceNode == null && !onlyDestination) || (destinationNode == null && !onlySource))
        {
            if (sourceNode == null && !onlyDestination)
            {
                sourceNodeInput.text = "";
                sourceNodeInput.transform.Find("Text Area").transform.Find("Placeholder").GetComponent<TMP_Text>().text = "Can't find node";
            }
            else
            {
                sourceNodeInput.transform.Find("Text Area").transform.Find("Placeholder").GetComponent<TMP_Text>().text = "Any source Node ID";
            }
            if (destinationNode == null && !onlySource)
            {
                destinationNodeInput.text = "";
                destinationNodeInput.transform.Find("Text Area").transform.Find("Placeholder").GetComponent<TMP_Text>().text = "Can't find node";
            }
            else
            {
                destinationNodeInput.transform.Find("Text Area").transform.Find("Placeholder").GetComponent<TMP_Text>().text = "Any destination Node ID";
            }

            return;
        }

        else
        {
            // Reset the texts
            destinationNodeInput.text = "";
            sourceNodeInput.text = "";

            bool successfullyAddedFilter;

            if (onlySource || onlyDestination)
            {
                successfullyAddedFilter = filtersController.AddOneNodeContactFilter(filter, onlySource, onlyDestination);
            }

            else
            {
                successfullyAddedFilter = filtersController.AddTwoNodesContactFilter(filter);
            }


            if(successfullyAddedFilter) 
            {
                // Reset the placeholders
                sourceNodeInput.transform.Find("Text Area").transform.Find("Placeholder").GetComponent<TMP_Text>().text = "Any source node ID";
                destinationNodeInput.transform.Find("Text Area").transform.Find("Placeholder").GetComponent<TMP_Text>().text = "Any destination node ID";
            }
            else
            {
                // Reset the placeholders to inform the user the filter addition failed
                sourceNodeInput.transform.Find("Text Area").transform.Find("Placeholder").GetComponent<TMP_Text>().text = "Filter already exists";
                destinationNodeInput.transform.Find("Text Area").transform.Find("Placeholder").GetComponent<TMP_Text>().text = "Filter already exists";
            }
        }
    }

    private void OnResetAllButtonClicked()
    {
        filtersController.RemoveAllFilters();
    }

    //private void OnResetOneButtonClicked()
    //{
    //    // Get the values from the inputs
    //    string sourceValue = sourceNodeInput.text;
    //    string destinationValue = destinationNodeInput.text;

    //    // Get the node GameObjects
    //    GameObject sourceNode = GameObject.Find(sourceValue);
    //    GameObject destinationNode = GameObject.Find(destinationValue);

    //    FiltersController.ContactFilter filter = new FiltersController.ContactFilter(sourceNode, destinationNode);

    //    // Reset the texts
    //    destinationNodeInput.text = "";
    //    sourceNodeInput.text = "";

    //    // Try removing the filter the filter
    //    bool successfullyRemovedFilter = filtersController.RemoveOneContactFilter(filter);

    //    if(successfullyRemovedFilter)
    //    {
    //        // Reset the placeholders
    //        sourceNodeInput.transform.Find("Text Area").transform.Find("Placeholder").GetComponent<TMP_Text>().text = "Source node ID";
    //        destinationNodeInput.transform.Find("Text Area").transform.Find("Placeholder").GetComponent<TMP_Text>().text = "Destination node ID";
    //    }
    //    else
    //    {
    //        // Reset the placeholders to inform the user the removal failed
    //        sourceNodeInput.transform.Find("Text Area").transform.Find("Placeholder").GetComponent<TMP_Text>().text = "No filter found";
    //        destinationNodeInput.transform.Find("Text Area").transform.Find("Placeholder").GetComponent<TMP_Text>().text = "No filter found";
    //    }
    //}

    public void ToggleFiltersWindowVisibility(bool visibility)
    {
        // If another UI window is open, ignore
        if (uiHelpController.isHelpWindowOpen)
            return;

        isFilterWindowOpen = visibility;

        if(visibility)
        {
            canvasFader.FadeIn(uiFiltersCanvas.GetComponent<CanvasGroup>(), 0.2f);
        }
        else
        {
            canvasFader.FadeOut(uiFiltersCanvas.GetComponent<CanvasGroup>(), 0.2f);
        }

        // Reset the inputs when closing the window
        if (!visibility)
        {
            sourceNodeInput.text = "";
            destinationNodeInput.text = "";
            sourceNodeInput.transform.Find("Text Area").transform.Find("Placeholder").GetComponent<TMP_Text>().text = "Any source node ID";
            destinationNodeInput.transform.Find("Text Area").transform.Find("Placeholder").GetComponent<TMP_Text>().text = "Any destination node ID";
        }

        // Show or hide the rest of the UI
        uiController.ToggleMainUIVisibility(!uiController.isMainUiVisible);

        // If we're opening the filter window and the help window is also open, close it
        if (isFilterWindowOpen && uiHelpController.isHelpWindowOpen)
        {
            uiHelpController.ToggleHelpWindowVisibility(false);
        }
    }

    public void AddFilterToToggleList(FiltersController.ContactFilter filter, Transform parent)
    {
        // Instantiate a new list item
        GameObject newFilterListItem = Instantiate(filterTogglePrefab, parent);

        // Set the filter description text
        newFilterListItem.GetComponentInChildren<Text>().text = filter.ToString();

        if(filter.filterType==FiltersController.FilterType.Highlight)
        {
            newFilterListItem.GetComponentInChildren<Text>().color = filter.color;
        }

        Toggle toggle = newFilterListItem.transform.Find("Background").GetComponent<Toggle>();
        toggle.onValueChanged.AddListener(delegate {
            filtersController.SwitchFilterStatus(filter, toggle.isOn);
            Debug.Log("clicked");
        });

        DragAndDropItem dragAndDropController = newFilterListItem.GetComponent<DragAndDropItem>();
        dragAndDropController.filter = filter;

        Button deleteFilter = newFilterListItem.transform.Find("delete").GetComponent<Button>();
        deleteFilter.onClick.AddListener(delegate
        {
            filtersController.RemoveOneContactFilter(filter);
        });

        //Assign a name to the GameObject
        //string filterName = filter.filterType + "_" + filter.sourceNode.name + "_" + filter.destinationNode.name;
        string filterName = filter.ToString();
        newFilterListItem.name = filterName;

        filtersToggleDictionary.Add(filterName, newFilterListItem);

        MoveItemToFirstIndex(newFilterListItem);
    }


    public void MoveItemToFirstIndex(GameObject item)
    {
        // Move the item to the new index
        item.transform.SetSiblingIndex(0);
    }

    public void MoveItemToLastIndex(GameObject item)
    {
        // Get the last index in the content
        int lastIndex = item.transform.parent.childCount - 1;

        // Move the item to the last index
        item.transform.SetSiblingIndex(lastIndex);

        Debug.Log($"Moved {item.name} to the last index ({lastIndex})");
    }

    public void RemoveFilterFromToggleList(FiltersController.ContactFilter filter)
    {
        string filterName = filter.ToString();
        GameObject toggleToRemove = filtersToggleDictionary[filterName];
        filtersToggleDictionary.Remove(filterName);
        Destroy(toggleToRemove);
    }

    // TODO:
    // - Add different tabs for different types of filters (contacts limited to one planet,
    //   only between two (or more) planets...)
    // - Have filters be saved by scenarios in a filter file
    // - Hide bundles on the hidden contacts
}

using UnityEngine;
using System.Collections.Generic;
using System;

public class BundleController : MonoBehaviour
{
    // Pointers to global elements
    IPNvController ipnvController;
    private float currentScale;
    private Camera mainCamera;
    private FiltersController filtersController;

    // Hops
    public List<BundleHop> hops = new List<BundleHop>();
    public int currentHopIndex = 0;
    // BundleHop class to hold each hop information

    public class BundleHop
    {
        public GameObject SenderNode;
        public GameObject ReceiverNode;
        public double SendTime;
    }

    // Render
    private float baseWidth1 = 0.01f;
    public bool visible = true;

    public double[] currentPosNonShifted = new double[] { 0, 0, 0};

    void Start()
    {
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        ipnvController = GameObject.Find("IPNvController").GetComponent<IPNvController>();
        filtersController = GameObject.Find("FiltersController").GetComponent<FiltersController>();
        currentScale = ipnvController.currentScale;
        UpdateBundlePosNonShifted();
    }

    private void Update()
    {
        // Update the value of the variable holding the position of the bundle (without taking into account Origin Shift)
        if (ipnvController.currentStepTime != ipnvController.endTime)
        {
            UpdateBundlePosNonShifted();
        }
    }

    void LateUpdate()
    {
        // Move the bundle based on the current value calculated in Update
        MoveBundle();

        UpdateBundleWidths();
    }

    int CalculateCurrentHopIndex()
    {
        for (int i = hops.Count - 1; i >= 0; i--)
        {
            if (ipnvController.currentContinuousTime >= hops[i].SendTime)
            {
                return i;
            }
        }
        return 0; // Return the first hop if no match is found
    }

/// <summary>
/// Update the value of the variable holding the current position of the bundle (without taking into
/// account Origin Shift). This function DOES NOT move the bundle GameObject, it simply updates the currentPosNonShifted variables.
/// </summary>
/// <remarks>
/// This function MUST be called in Update() since it MUST run before Origin Shift is updated. 
/// </remarks>
public void UpdateBundlePosNonShifted()
    {
        // Recalculate currentHopIndex based on the current time
        currentHopIndex = CalculateCurrentHopIndex();

        if (currentHopIndex < hops.Count)
        {
            var currentHop = hops[currentHopIndex];

            // Over the air
            if (ipnvController.currentContinuousTime > currentHop.SendTime)
            {
                GameObject senderNodeObject = currentHop.SenderNode;
                GameObject receiverNodeObject = currentHop.ReceiverNode;

                Renderer objectRenderer = GetComponent<Renderer>();
                // Set visible:
                if (visible)
                    objectRenderer.enabled = true;
                else
                    objectRenderer.enabled = false;



                // Propagation
                double speedOfLight = 299792; // Speed of light in kms per second

                double[] unscaledPositionSenderNode = Util.MultiplyArrayByFloat(senderNodeObject.GetComponent<NodeController>().globalCurrentPosNonShifted, currentScale);
                double[] unscaledPositionReceiverNode = Util.MultiplyArrayByFloat(receiverNodeObject.GetComponent<NodeController>().globalCurrentPosNonShifted, currentScale);

                double unscaledDistanceToCover = CalculateDistance(unscaledPositionReceiverNode, unscaledPositionSenderNode);

                double lightTravelTime = unscaledDistanceToCover / speedOfLight; // Time for light to travel in seconds

                double elapsedTimeSinceSend = ipnvController.currentContinuousTime - currentHop.SendTime;

                double unscaledHeadDistanceCovered = System.Math.Clamp(elapsedTimeSinceSend / lightTravelTime, 0, 1) * unscaledDistanceToCover;

                double[] unscaledCurrentPosition = Util.LerpDouble(unscaledPositionSenderNode, unscaledPositionReceiverNode, unscaledHeadDistanceCovered / unscaledDistanceToCover);

                currentPosNonShifted = Util.DivideArrayByFloat(unscaledCurrentPosition, currentScale);

                // Check if the bundle has reached the destination of the current hop
                if (unscaledHeadDistanceCovered >= unscaledDistanceToCover)
                {
                    // Move to the next hop if it exists
                    currentHopIndex++;
                    if (currentHopIndex < hops.Count)
                    {
                        // Reset the send time for the next hop
                        currentHop = hops[currentHopIndex];
                    }
                }
            }
            else // At rest
            {
                // Set ingvisible:
                Renderer objectRenderer = GetComponent<Renderer>();
                objectRenderer.enabled = false;

                // State at sender
                GameObject senderNodeObject = currentHop.SenderNode;
                currentPosNonShifted = senderNodeObject.GetComponent<NodeController>().globalCurrentPosNonShifted;

            }
        }
    else
        {
            // State at receiver
            GameObject receiverNodeObject =hops[hops.Count - 1].ReceiverNode;
            currentPosNonShifted = receiverNodeObject.GetComponent<NodeController>().globalCurrentPosNonShifted;
        }
    }

    /// <summary>
    /// Move the bundle based on the currentPosNonShifted variable calculated in UpdateBundlePosNonShifted().
    /// </summary>
    /// <remarks>
    /// This function MUST be called in LateUpdate() since it MUST run after the Origin Shift value has been updated.
    /// </remarks>
    void MoveBundle()
    {
        // Calculate shifted position
        double[] currentPosShifted = Util.SubtractArrays(currentPosNonShifted, ipnvController.originShiftValue);

        // Move the bundle to its new position
        transform.position = Util.DoubleArrayToVector3(currentPosShifted);
    }

    /// <summary>
    /// Calculates the distance between two points in 3D space.
    /// </summary>
    /// <param name="pointA">The first point as a double array [x, y, z].</param>
    /// <param name="pointB">The second point as a double array [x, y, z].</param>
    /// <returns>The distance between the two points.</returns>
    /// <remarks>
    /// Ensure that both pointA and pointB arrays have exactly three elements.
    /// </remarks>
    double CalculateDistance(double[] pointA, double[] pointB)
    {
        if (pointA.Length != 3 || pointB.Length != 3)
        {
            Debug.LogError("Both pointA and pointB must have exactly three elements.");
            return -1; // or throw an exception
        }

        double deltaX = pointB[0] - pointA[0];
        double deltaY = pointB[1] - pointA[1];
        double deltaZ = pointB[2] - pointA[2];

        return Mathf.Sqrt((float)(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ));
    }

    void UpdateBundleWidths(){

        float distancePenalization = 0.000001f; 
        
        Vector3 cameraPosition = mainCamera.transform.position;
        float distanceToCamera = Vector3.Distance(cameraPosition, transform.position);
        float adjustedWidth1 = baseWidth1 * distanceToCamera / (1f + distancePenalization * distanceToCamera);

        if (adjustedWidth1 * 2 < 0.05f)
            adjustedWidth1 = 0.05f;
        // if (adjustedWidth1 * 2 > 100f)
        //     adjustedWidth1 = 100f;

        transform.localScale = new Vector3(adjustedWidth1 * 2, adjustedWidth1 * 2, adjustedWidth1 * 2);
    }

    Vector3 GetClosestPointOnLine(Vector3 lineStart, Vector3 lineEnd, Vector3 point)
    {
        Vector3 lineDirection = lineEnd - lineStart;
        float lineLength = lineDirection.magnitude;
        lineDirection.Normalize();

        float projection = Vector3.Dot(point - lineStart, lineDirection);
        projection = Mathf.Clamp(projection, 0f, lineLength);

        return lineStart + projection * lineDirection;
    }
}

﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class PlanetController : MonoBehaviour
{
    ////////////////
    /// System data
    ////////////////

    //Pilot and camera objects
    IPNvController ipnvController;
    CameraController cam;

    //Global scale variable
    private float currentScale;

    ////////////////
    /// Planet data
    ////////////////

    public string Name;
    public float Radius;
    public double OrbitPeriod;
    public List<GameObject> childNodes = new();
    public Dictionary<double, (double[] position, Quaternion rotation)> events = new();

    //////////////////////
    /// Orbital parmeters
    //////////////////////

    public double SemiMajorAxis;    // Semi-major axis (km)
    public double Eccentricity;     // Eccentricity
    public double Inclination;      // Inclination (deg)
    public double Raan;             // Right ascension of the ascending node (deg)
    public double Argp;             // Argument of periapsis (deg)
    public double MeanAnomaly;      // Mean anomaly (deg) at J2000 epoch
    public double Radii;
    public double Obliquity;
    public double RotationPeriod;

    public double[][] positions;
 
    //////////////////////
    /// Angle variables
    //////////////////////

    //Start and target angles used to interpolate and get the current angle of the planet
    public Quaternion startAngle;
    public Quaternion targetAngle;

    // The current angle of the planet
    public Quaternion currentAngle;


    //////////////////////
    /// Position variables
    //////////////////////

    // Start and target positions used to interpolate and get the current position of the planet
    // without taking into account Origin Shift.
    public double[] startPosNonShifted = new double[] { 0, 0, 0};
    public double[] targetPosNonShifted = new double[] { 0, 0, 0 };

    // The current position of the planet without Origin Shift. This value (and not transform.position)
    // MUST be used when centering the object using Origin Shift since it is in double precision. 
    public double[] currentPosNonShifted = new double[] { 0, 0, 0 };


    //////////////////////
    /// Planet trajectory
    //////////////////////

    public LineRenderer lineRendererTrajectFromFile;
    public LineRenderer lineRendererTrajectFromParams;


//#if UNITY_WEBGL && !UNITY_EDITOR
//        [DllImport("__Internal")]
//        private static extern void SendOriginShiftUpdate(bool originShiftState);
//#endif


    private void Awake()
    {
        // Find IPNvController and camera objects
        ipnvController = GameObject.Find("IPNvController").GetComponent<IPNvController>();
        cam = GameObject.Find("Main Camera").GetComponent<CameraController>();

        // Add LineRenderer component (orbit trajectory from params)
        GameObject childObject1 = new GameObject("TrajectFromParams");
        childObject1.transform.SetParent(transform, false); // Set the current GameObject as the parent
        childObject1.transform.localPosition = Vector3.zero; // Reset local position
        childObject1.transform.localRotation = Quaternion.identity; // Reset local rotation
        childObject1.transform.localScale = Vector3.one; // Reset local scale
        lineRendererTrajectFromParams = childObject1.AddComponent<LineRenderer>();

        // Add LineRenderer component (orbit trajectory from files)
        GameObject childObject2 = new GameObject("TrajectFromFile");
        childObject2.transform.SetParent(transform, false); // Set the current GameObject as the parent
        childObject2.transform.localPosition = Vector3.zero; // Reset local position
        childObject2.transform.localRotation = Quaternion.identity; // Reset local rotation
        childObject2.transform.localScale = Vector3.one; // Reset local scale
        lineRendererTrajectFromFile = childObject2.AddComponent<LineRenderer>();
    }

    private void Start()
    {
        //Get scale from ipnvController
        currentScale = ipnvController.currentScale;

        //Assign non shifted start position variable
        double xPos = events[ipnvController.currentStepTime].position[0] / currentScale;
        double yPos = events[ipnvController.currentStepTime].position[1] / currentScale;
        double zPos = events[ipnvController.currentStepTime].position[2] / currentScale;

        startAngle = transform.rotation;
        startPosNonShifted = new double[] { xPos, yPos, zPos };
        currentPosNonShifted = startPosNonShifted;

        // Move the object to its first position
        transform.position = new Vector3((float)xPos, (float)yPos, (float)zPos);

        GenerateOrbitalTrajectoryFromFilePositions();
        GenerateOrbitalTrajectoryFromOrbitalParams();

        // Update the value of the currentPosNonShifted variable
        UpdateCurrentPosNonShifted();

        // Update the value of the currentAngle variable
        UpdateCurrentAngle();
    }


    private void Update()
    {
        if (ipnvController.currentStepTime != ipnvController.endTime)
        {
            // Update the value of the currentPosNonShifted variable
            UpdateCurrentPosNonShifted();

            // Update the value of the currentAngle variable
            UpdateCurrentAngle();
        }
    }

    private void LateUpdate()
    {
        // Move the planet based on the current values calculated in Update
        MoveAndRotatePlanet();

        GenerateOrbitalTrajectoryFromFilePositions();
        GenerateOrbitalTrajectoryFromOrbitalParams();
    }


    /// <summary>
    /// Update the value of the variable holding the current position of the planet (without taking into
    /// account Origin Shift). This function DOES NOT move the planet GameObject, it simply updates the
    /// currentPosNonShifted variable.
    /// </summary>
    /// <remarks>
    /// This function MUST be called in Update() since it MUST run before Origin Shift is updated. 
    /// </remarks>
    private void UpdateCurrentPosNonShifted()
    {
        // Assign planet target position
        double xTargetPos = events[ipnvController.currentStepTime + ipnvController.step * ipnvController.multiplicationFactor].position[0] / currentScale;
        double yTargetPos = events[ipnvController.currentStepTime + ipnvController.step * ipnvController.multiplicationFactor].position[1] / currentScale;
        double zTargetPos = events[ipnvController.currentStepTime + ipnvController.step * ipnvController.multiplicationFactor].position[2] / currentScale;

        targetPosNonShifted = new double[] { xTargetPos, yTargetPos, zTargetPos };

        // If the simulation timer is between two points in time where the position is exactly known, perform interpolation
        if (ipnvController.elapsedTime < ipnvController.timeToReachTargetPosition)
        {
            // Update the current non shifted position variable using interpolation
            currentPosNonShifted = Util.LerpDouble(startPosNonShifted, targetPosNonShifted, ipnvController.elapsedTime / ipnvController.timeToReachTargetPosition);
        }
        // Else, if the simulation timer reached a point in time where the position is exactly known, assign the exact values
        else
        {
            // Update the current non shifted position variable using the target value
            currentPosNonShifted = targetPosNonShifted;

            // The target value becomes the start value for the next time the function is ran
            startPosNonShifted = targetPosNonShifted;
        }
    }

    /// <summary>
    /// Update the value of the variable holding the current angle of the planet. This function DOES NOT
    /// rotate the planet GameObject, it simply updates the currentAngle variable.
    /// </summary>
    private void UpdateCurrentAngle()
    {
        //Assign planet target angle
        targetAngle = events[ipnvController.currentStepTime + ipnvController.step * ipnvController.multiplicationFactor].rotation;

        // If the simulation timer is between two points in time where the position is exactly known, perform interpolation
        if (ipnvController.elapsedTime < ipnvController.timeToReachTargetPosition)
        {
            // Update current angle value using interpolation
            currentAngle = Quaternion.Lerp(startAngle, targetAngle, ipnvController.elapsedTime / ipnvController.timeToReachTargetPosition); 
        }
        // Else, if the simulation timer reached a point in time where the position is exactly known, assign the exact values
        else
        {
            // Update current angle value using the target value
            currentAngle = targetAngle;

            // The target value becomes the start value for the next time the function is ran
            startAngle = targetAngle;
        }
    }

    /// <summary>
    /// Move and rotate the planet based on the currentPosNonShifted and currentAngle variables calculated in
    /// UpdateCurrentPosNonShifted() and UpdateCurrentAngle().
    /// </summary>
    /// <remarks>
    /// This function MUST be called in LateUpdate() since it MUST run after the Origin Shift value has been updated.
    /// </remarks>
    private void MoveAndRotatePlanet()
    {
        // Calculate the shifted position
        double[] currentPosShifted = Util.SubtractArrays(currentPosNonShifted, ipnvController.originShiftValue);

        // Move and rotate the planet
        transform.position = Util.DoubleArrayToVector3(currentPosShifted);
        transform.rotation = currentAngle; // the angles exported by IPN-d are already relative to the global frame

        /// The rest of this function would be useful if the scenario files contained rotations that
        /// were defined relatively to the planet's local frame and not to the world's global frame.
        /// It might be useful in the future when opening IPN-v to other orbit calculation softwares
        /// with different rotation conventions.
        
        ////Calculate the tilt of the planet's local frame 
        //Quaternion tilt = Quaternion.AngleAxis(currentAngle.eulerAngles.z, Vector3.forward);

        ////Get the planet's rotation in the local frame
        //Quaternion localRotation = currentAngle;

        //// Formula breakdown:
        //// - Quaternion.Inverse(tilt) transforms from the global frame to the local frame,
        //// - localRotation applies the rotation in the local frame,
        //// - tilt transforms back to the global frame.
        //// This formula expresses the intrinsic rotation of the local frame in the global frame.
        //Quaternion globalRotation = tilt * localRotation * Quaternion.Inverse(tilt);

        //// Move and rotate the planet
        //transform.position = Util.DoubleArrayToVector3(currentPosShifted);
        //transform.rotation = globalRotation;
    }


    /// <summary>
    /// Handle immediate planet movement (for example when sliding the time bar)
    /// </summary>
    public void UpdatePositionImmediately()
    {
        if (currentScale == 0) // Called before initialized
            return;

        // Assign planet start angle -         Debug.Log("currentScale A" + currentScale);
        startPosNonShifted = Util.DivideArrayByFloat(events[ipnvController.currentStepTime].position, currentScale);
        startAngle = events[ipnvController.currentStepTime].rotation;

        // Assign planet target angle
        targetPosNonShifted = Util.DivideArrayByFloat(events[ipnvController.currentStepTime + ipnvController.step * ipnvController.multiplicationFactor].position, currentScale);
        targetAngle = events[ipnvController.currentStepTime + ipnvController.step * ipnvController.multiplicationFactor].rotation;

        // Calculate planet current position and rotation
        //double[] calculatedCurrentPosition = LerpDouble(nonShiftedStartPos, nonShiftedTargetPos, ipnvController.elapsedTime / ipnvController.timeToReachTargetPosition);
        //Quaternion calculatedCurrentAngle = Quaternion.Lerp(startAngle, targetAngle, ipnvController.elapsedTime / ipnvController.timeToReachTargetPosition);
        currentPosNonShifted = Util.LerpDouble(startPosNonShifted, targetPosNonShifted, ipnvController.elapsedTime / ipnvController.timeToReachTargetPosition);
        double[] currentPosShifted = Util.SubtractArrays(currentPosNonShifted, ipnvController.originShiftValue);
        currentAngle = Quaternion.Lerp(startAngle, targetAngle, ipnvController.elapsedTime / ipnvController.timeToReachTargetPosition);

        //Move the planet transform to its current position taking into account origin shift and rotate it
        transform.position = Util.DoubleArrayToVector3(currentPosShifted);
        //transform.rotation = currentAngle;

        Quaternion test = currentAngle * Quaternion.Inverse(transform.rotation);

        transform.Rotate(0, test.eulerAngles.y, 0, Space.Self);
    }

    // Populate orbital trajectory
    void GenerateOrbitalTrajectoryFromFilePositions()
    {
        // Set the width of the LineRenderer
        lineRendererTrajectFromFile.startWidth = 0.1f;
        lineRendererTrajectFromFile.endWidth = 0.1f;

        // Set the number of positions
        lineRendererTrajectFromFile.positionCount = events.Count;

        // Create an array to hold the positions
        double[][] positions = new double[events.Count][];

        int index = 0;
        foreach (var entry in events)
        {
            //positions[index] = DivideArrayByFloat(entry.Value.position, currentScale) - ipnvController.originShift;
            positions[index] =  Util.SubtractArrays(Util.DivideArrayByFloat(entry.Value.position, currentScale), ipnvController.originShiftValue);
            index++;
        }

        // Set the positions to the LineRenderer
        lineRendererTrajectFromFile.SetPositions(Util.ConvertToVector3Array(positions));    

        // Set material and color
        lineRendererTrajectFromFile.material = new Material(Shader.Find("Sprites/Default"));
        lineRendererTrajectFromFile.startColor = Color.yellow;
        lineRendererTrajectFromFile.endColor = Color.yellow;

        // Fix width to camera distance
        float baseWidth = 0.005f;
        Camera mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        Vector3 cameraPosition = mainCamera.transform.position;
        Vector3 closestPoint = GetClosestPointOnLine(Util.ConvertToVector3Array(positions), cameraPosition);

        float distanceToCamera = Vector3.Distance(cameraPosition, closestPoint);
        float adjustedWidth = baseWidth * distanceToCamera;

        lineRendererTrajectFromFile.startWidth = adjustedWidth;
        lineRendererTrajectFromFile.endWidth = adjustedWidth;
    }

    void GenerateOrbitalTrajectoryFromOrbitalParams()
    {
        // Set the width of the LineRenderer
        lineRendererTrajectFromParams.startWidth = 0.1f;
        lineRendererTrajectFromParams.endWidth = 0.1f;

        // Number of points to define the rest of the orbit not described in the scenario files
        int numOfAdditionalPoints = 1000;

        // Create an array to hold the positions. The indexes 0 to events.Count will be filled by the position
        // values given in the scenario files to make sure the yellow and grey lines match. The indexes
        // (events.Count + 1) to (events.Count + numPoints) will be filled by newly calculated values.
        positions = new double[events.Count + numOfAdditionalPoints][];

        // Fill the first part of the array with the positions from the scenario file. 
        int index = 0;
        foreach (var entry in events)
        {
            positions[index] = Util.SubtractArrays(Util.DivideArrayByFloat(entry.Value.position, currentScale), ipnvController.originShiftValue);
            index++;
        }
        
        // Time increment for each point not described by the scenario files.
        double timeIncrement = (OrbitPeriod - (ipnvController.endTime - ipnvController.startTime)) / numOfAdditionalPoints;

        // Fill the rest of the array with newly calculated points
        for (int i = index; i < positions.Length; i++)
        {
            double time = ipnvController.endTime + (i - events.Count) * timeIncrement;

            double[] position = CalculatePlanetPositionAtTime(time);
            positions[i] = Util.SubtractArrays(Util.DivideArrayByFloat(position, currentScale), ipnvController.originShiftValue);
        }

        // Ensure the last point is the same as the first to close the circle
        positions[positions.Length-1] = positions[0];

        // Set the positions to the LineRenderer
        lineRendererTrajectFromParams.positionCount = positions.Length;
        lineRendererTrajectFromParams.SetPositions(Util.ConvertToVector3Array(positions));

        // Set material and color
        lineRendererTrajectFromParams.material = new Material(Shader.Find("Sprites/Default"));
        lineRendererTrajectFromParams.startColor = Color.gray;
        lineRendererTrajectFromParams.endColor = Color.gray;

        // Fix width to camera distance
        float baseWidth = 0.005f;
        Camera mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        Vector3 cameraPosition = mainCamera.transform.position;
        Vector3 closestPoint = GetClosestPointOnLine(Util.ConvertToVector3Array(positions), cameraPosition);
        
        float distanceToCamera = Vector3.Distance(cameraPosition, closestPoint);
        float adjustedWidth = baseWidth * distanceToCamera;
        
        lineRendererTrajectFromParams.startWidth = adjustedWidth;
        lineRendererTrajectFromParams.endWidth = adjustedWidth;
    }


    ///////////////////////////////////////////////////////////////////
    /// Update Position from IPN-D to generate orbital trajectory
    ///////////////////////////////////////////////////////////////////
    public double[] CalculatePlanetPositionAtTime(double time)
    {
        //Constants
        double G = 6.6743e-20; // Gravitational constant (km^3/kg/s^2)
        double M_sun = 1.9885e30; // Mass of the Sun (kg)
        
        // Calculate the mean motion (n)      
        double n = Math.Sqrt(G * M_sun / Math.Pow(SemiMajorAxis, 3));

        // Calculate the mean anomaly (M)
        double mean_anomaly_rad = MeanAnomaly * Math.PI / 180; //
        double M = mean_anomaly_rad + n * time;

        // Calculate the eccentric anomaly (E) using Newton-Raphson iteration
        double E = M; // Initial guess
        while (true)
        {
            double E_new = E - (E - Eccentricity * Math.Sin(E) - M) / (1 - Eccentricity * Math.Cos(E));
            if (Math.Abs(E_new - E) < 1e-8)
            { // Convergence criteria
                E = E_new;
                break;
            }
            E = E_new;
        }

        // Calculate the true anomaly (nu)
        double nu = 2 * Math.Atan(Math.Sqrt((1 + Eccentricity) / (1 - Eccentricity)) * Math.Tan(E / 2));

        // Calculate the distance from the Sun (r)
        double r = SemiMajorAxis * (1 - Eccentricity * Math.Cos(E));

        // Calculate the position in the orbital plane (x', y')
        double x_prime = r * Math.Cos(nu);
        double y_prime = r * Math.Sin(nu);

        // Calculate the position in 3D heliosynchronous coordinates (x, y, z)
        double i_rad = Inclination * Math.PI / 180;
        double raan_rad = Raan * Math.PI / 180;
        double argp_rad = Argp * Math.PI / 180;

        double newX = x_prime * (Math.Cos(raan_rad) * Math.Cos(argp_rad) - Math.Sin(raan_rad) * Math.Sin(argp_rad) * Math.Cos(i_rad)) - y_prime * (Math.Sin(raan_rad) * Math.Cos(argp_rad) + Math.Cos(raan_rad) * Math.Sin(argp_rad) * Math.Cos(i_rad));
        double newY = x_prime * (Math.Cos(raan_rad) * Math.Sin(argp_rad) + Math.Sin(raan_rad) * Math.Cos(argp_rad) * Math.Cos(i_rad)) + y_prime * (Math.Cos(raan_rad) * Math.Cos(argp_rad) * Math.Cos(i_rad) - Math.Sin(raan_rad) * Math.Sin(argp_rad));
        double newZ = x_prime * (Math.Sin(raan_rad) * Math.Sin(i_rad)) + y_prime * (Math.Cos(raan_rad) * Math.Sin(i_rad));

        // Return with Z and Y shifted
        return new double[] { newX, newZ, newY };
        //return new Vector3((float)newX, (float)newZ, (float)newY);
    }

    Vector3 GetClosestPointOnLine(Vector3[] positions, Vector3 point)
    {
        Vector3 closestPoint = Vector3.zero;
        float minDistance = float.MaxValue;

        for (int i = 0; i < positions.Length - 1; i++)
        {
            Vector3 segmentStart = positions[i];
            Vector3 segmentEnd = positions[i + 1];
            Vector3 closestPointOnSegment = GetClosestPointOnSegment(segmentStart, segmentEnd, point);
            float distance = Vector3.Distance(point, closestPointOnSegment);

            if (distance < minDistance)
            {
                minDistance = distance;
                closestPoint = closestPointOnSegment;
            }
        }

        return closestPoint;
    }

    Vector3 GetClosestPointOnSegment(Vector3 segmentStart, Vector3 segmentEnd, Vector3 point)
    {
        Vector3 segmentVector = segmentEnd - segmentStart;
        Vector3 pointVector = point - segmentStart;
        float segmentLength = segmentVector.magnitude;
        float projectionLength = Vector3.Dot(pointVector, segmentVector.normalized);

        if (projectionLength < 0)
            return segmentStart;

        if (projectionLength > segmentLength)
            return segmentEnd;

        return segmentStart + segmentVector.normalized * projectionLength;
    }
}
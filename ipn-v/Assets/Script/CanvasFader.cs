﻿using UnityEngine;
using System.Collections;

public class CanvasFader : MonoBehaviour
{
    public void FadeIn(CanvasGroup canvasGroup, float fadeDuration)
    {
        SetChildrenState(canvasGroup, true); // Activate children
        canvasGroup.gameObject.GetComponent<Canvas>().sortingOrder = 1;
        StartCoroutine(FadeCanvas(canvasGroup, 1, fadeDuration));
    }

    public void FadeOut(CanvasGroup canvasGroup, float fadeDuration)
    {
        StartCoroutine(FadeCanvas(canvasGroup, 0, fadeDuration));
        SetChildrenState(canvasGroup, false); // Deactivate children
        canvasGroup.gameObject.GetComponent<Canvas>().sortingOrder = 0;

    }

    private IEnumerator FadeCanvas(CanvasGroup canvasGroup, float targetAlpha, float duration)
    {
        //Make sure the cursor doesn't get stuck in the interactable position
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

        float startAlpha = canvasGroup.alpha;
        float elapsedTime = 0f;

        while (elapsedTime < duration)
        {
            elapsedTime += Time.deltaTime;
            canvasGroup.alpha = Mathf.Lerp(startAlpha, targetAlpha, elapsedTime / duration);
            yield return null;
        }

        canvasGroup.alpha = targetAlpha;
    }

    private void SetChildrenState(CanvasGroup canvasGroup, bool isActive)
    {
        foreach (Transform child in canvasGroup.transform)
        {
            child.gameObject.SetActive(isActive);
        }
    }
}
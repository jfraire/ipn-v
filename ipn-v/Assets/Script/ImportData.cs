﻿using System;
using System.Collections.Generic;
using UnityEngine;

//Class used to hande JSON imports
public class ImportData
{
    //////////
    /// WebGL 
    //////////

    [Serializable]
    public class WebData
    {
        public ImportData.SimulationData configData;
        public ImportData.ContactPlanData contactData;
        public ImportData.BundlePlan bundlePlanData;
        public List<ImportData.PlanetPositionData> planetPositions = new();
        public List<ImportData.NodePositionData> nodePositions = new();
    }


    /////////////////
    /// Config file 
    /////////////////

    [Serializable]
    public class SimulationData //Used to parse the config file
    {
        public List<PlanetData> Planets;
        public StarData Star;
        public TimeData Time;
    }

    [Serializable]
    public class TimeData
    {
        public double SimulationStartTime;
        public double SimulationEndTime;
        public double Step;
    }

    [Serializable]
    public class PlanetData
    {
        public List<NodeData> Nodes = new List<NodeData>();
        public float Radius;
        public string Name;
        public double OrbitPeriod;

        // Orbital params
        public double SemiMajorAxis;           // Semi-major axis (km)
        public double Eccentricity;           // Eccentricity
        public double Inclination;           // Inclination (deg)
        public double Raan;        // Right ascension of the ascending node (deg)
        public double Argp;        // Argument of periapsis (deg)
        public double MeanAnomaly; // Mean anomaly (deg) at J2000 epoch
        public double Radii;
        public double Obliquity;
        public double RotationPeriod;
        public double ArgOfPeriapsis;

        //List of positions
        //[NonSerialized] public Dictionary<double, (Vector3 position, Quaternion rotation)> events = new();
        [NonSerialized] public Dictionary<double, (double[] position, Quaternion rotation)> events = new();

        //[NonSerialized] public double[] precisePosition;

    }

    [Serializable]
    public class NodeData
    {
        public int ID;
        public string Name;
        public double OrbitPeriod;

        [NonSerialized] public PlanetData CentralObject; //Set as NonSerialized to avoid circular reference
        [NonSerialized] public Dictionary<double, Vector3> events = new();//List of positions

    }

    [System.Serializable]
    public class BundlePlan
    {
        public List<BundleData> Bundles;
    }
    [System.Serializable]
    public class BundleData
    {
        public string Name;
        public List<HopData> Hops;
    }

    [System.Serializable]
    public class HopData
    {
        public string SenderNode;
        public string ReceiverNode;
        public double SendTime;
    }

    [Serializable]
    public class StarData
    {
        public string Name;
        public float Radius;
    }

    /////////////////
    /// Contact plan 
    /////////////////

    [Serializable]
    public class ContactPlanData
    {
        public List<ContactData> ContactPlan = new();
    }

    [Serializable]
    public class ContactData
    {
        public int SourceID;
        public int DestinationID;
        public double StartTime;
        public double EndTime;
        public double Duration;
        public float[] Color;
    }

    ////////////////////
    /// Planet positions 
    ////////////////////
    [Serializable]
    public class PlanetPositionData
    {
        public string Name;
        public List<PlanetPositionSpecificTimeData> Positions = new();
    }

    [Serializable]
    public class PlanetPositionSpecificTimeData
    {
        public double Time;
        public double PositionX;
        public double PositionY;
        public double PositionZ;
        public double RotationX;
        public double RotationY;
        public double RotationZ;

        public PlanetPositionSpecificTimeData(double time, double posX, double posY, double posZ, double rotationX, double rotationY, double rotationZ)
        {
            Time = time;
            PositionX = posX;
            PositionY = posY;
            PositionZ = posZ;
            RotationX = rotationX;
            RotationY = rotationY;
            RotationZ = rotationZ;
        }
    }

    ///////////////////
    /// Node positions 
    ///////////////////


    [Serializable]
    public class NodePositionData
    {
        public int ID;
        public List<NodePositionSpecificTimeData> Positions = new();
    }

    [Serializable]
    public class NodePositionSpecificTimeData
    {
        public double Time;
        public double PositionX;
        public double PositionY;
        public double PositionZ;

        public NodePositionSpecificTimeData(double time, double posX, double posY, double posZ)
        {
            Time = time;
            PositionX = posX;
            PositionY = posY;
            PositionZ = posZ;
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System.Runtime.InteropServices;

// VR: Library
using UnityEngine.XR;

public class IPNvController : MonoBehaviour
{
    // Where the scenario folder is located
    //string scenarioFolder = "JSON-small-earth-network/";
    //string scenarioFolder = "JSON-one-orbiter-per-planet/";
    string scenarioFolder = "JSON-earth-constellation/";

    // Object dictionaries
    public Dictionary<string, GameObject> planets = new();
    public Dictionary<int, GameObject> nodes = new();
    public Dictionary<double, List<Contact>> contacts = new();
    public List<Contact> onGoingContacts = new();
    public ImportData.StarData star;
    public Transform starTransform;
    private GameObject StarGameObject;

    // Origin shift 
    public double[] originShiftValue = new double[] { 0, 0, 0 };
    public GameObject centeredObjectOriginShift = null;
    public GameObject lastCenteredObjectOriginShift = null;

    // Origin shift transition
    public double transitionTimeOriginShift = 5;
    public double elapsedTimeTransitionOriginShift = 0;
    public bool isTransitioningOriginShift = false;

    // Contact class
    public class Contact
    {
        public GameObject source;
        public GameObject destination;
        public double startTime;
        public double endTime;
        public double duration;
        public GameObject link;
        public Color color = new Color();
        public bool visible = true;
        public bool highlighted = false;
        public Color highlightedColor = new Color();

        public Contact(GameObject src, GameObject dest, double sT, double eT, double d)
        {
            source = src;
            destination = dest;
            startTime = sT;
            endTime = eT;
            duration = d;
        }
    }

    // Bundle
    public GameObject bundlePrefab;

    // GameObject lists
    public List<GameObject> bundleList = new List<GameObject>(); // list of every bundles in the simulation
    public int currentlyFocusedBundleIndex = 0; // used to iterate of the list to center bundles one after the other

    public List<GameObject> nodeList = new List<GameObject>();
    public int currentlyFocusedNodeIndex = 0; // used to iterate of the list to center nodes one after the other
    public int currentlyFocusedPlanetIndex = 0; 


    // Scale variables
    public float kmsPerGrid = (float)149e6 / 100; // 149e6 = AU
    public float currentScale = 6371 * 2;

    // Time variables (in seconds)
    public double startTime = 0;
    public double endTime = 0;
    public double currentStepTime = 0;
    public double step = 0;
    public double speedUp = 1;
    public double multiplicationFactor = 1;
    public bool pause = true;
    public double currentContinuousTime = 725803267.184;

    // Movement time variables (in seconds)
    public float timeToReachTargetPosition;
    public float elapsedTime = 0;

    // Prefabs
    public GameObject planetPrefab;
    public GameObject starPrefab;
    public GameObject nodePrefab;
    public GameObject linkPrefab;
    public GameObject mercuryPrefab;
    public GameObject venusPrefab;
    public GameObject earthPrefab;
    public GameObject marsPrefab;
    public GameObject jupiterPrefab;
    public GameObject saturnPrefab;
    public GameObject uranusPrefab;
    public GameObject neptunePrefab;
    public GameObject plutoPrefab;

    // Link base color
    Color linkBaseColor = new Color (0.78f, 0.99f, 1.0f, 1.0f);

    // JSON import data
    private ImportData.SimulationData webSimulationData;
    private ImportData.ContactPlanData webContactPlanData;
    private ImportData.BundlePlan webBundlePlanData;
    private Dictionary<string, ImportData.PlanetPositionData> webPlanetsPositionsData = new();
    private Dictionary<int, ImportData.NodePositionData> webNodesPositionsData = new();

    // Camera
    public CameraController cameraController;
    public Transform globalCamera;
    public Camera cameraObject;

    // Canvas
    public Canvas canvas;
    public Canvas uiMainCanvas;
    public Canvas uiFiltersCanvas;
    public Canvas uiHelpCanvas;

    // Start variable
    public bool simulationStarted = false;

    // Framerate measurement
    public float lastFramerate = 0.0f;
    public float refreshTime = 1f;

    // Reference to other components
    public UIController uiController;
    public UIHelpController uiHelpController;
    public UIFiltersController uiFiltersController;

    public int numberOfLinks;
    private double lastTime = 0;

    // LayerMask for obstacles
    public LayerMask obstacle;

    // VR: Controllers
    private InputDevice rightController;
    private InputDevice leftController;
    // Track previous state of the triggers
    private bool previousRTriggerState = false;
    private bool previousLTriggerState = false;
    // Track previous state of the triggers
    private bool previousRGripState = false;
    private bool previousLGripState = false;
    private Canvas vrCanvas;
    private TextMeshProUGUI vrMessageText;
    private TextMeshProUGUI vrTimeText;

    // Audio:
    public AudioSource audioSourceStart;
    public AudioSource audioSourceClick;
    public AudioSource audioSourceArrived;
    
    // Demo: Object to focus on
    List<GameObject> focusObjects;
    int currentFocusIndex = 0;

#if UNITY_WEBGL
    [DllImport("__Internal")]
    private static extern void SendReady();
#endif

    void Awake()
    {
        Debug.Log("***********[IPNv]*********** Awake()");
        Initialize();
        simulationStarted = true;
        ParseConfigFile();
        centeredObjectOriginShift = GameObject.Find("Sun");
    }


    // Runs once at the beginning of the simulation
    void Start()
    {
        Debug.Log("***********[IPNv]*********** Start()");

#if UNITY_WEBGL && !UNITY_EDITOR
        SendReady();
#endif

        // Audio: initialize
        audioSourceStart = GameObject.Find("AudioStart").GetComponent<AudioSource>();
        audioSourceClick = GameObject.Find("AudioClick").GetComponent<AudioSource>();
        audioSourceArrived = GameObject.Find("AudioArrived").GetComponent<AudioSource>();

        // VR: Initialize the controllers for Oculus Quest
        if (XRSettings.isDeviceActive){
            StartCoroutine(CheckForRightController());
            StartCoroutine(CheckForLeftController());
            InitializeVRCanvas();
            UpdateVRMessage(""); // Clear the message initially
        }

        // Demo: Initialize the focusObjects list in Start
        focusObjects = new List<GameObject>
        {
            GameObject.Find("Earth"),
            GameObject.Find("Mars"),
            GameObject.Find("Bundle_401_1")
        };

        // Demo: Start smooth focusing on Earth
        if(!isTransitioningOriginShift && !cameraController.cameraIsZoomingDuringTransition)
        {
            TriggerOriginShift(GameObject.Find("Earth"));
        }

        PickSpecificTime(startTime);
    }

    // VR: Initialize a basic UI in VR
    private void InitializeVRCanvas()
    {
        // Create Canvas GameObject
        GameObject canvasObject = new GameObject("VRCanvas");
        vrCanvas = canvasObject.AddComponent<Canvas>();
        vrCanvas.renderMode = RenderMode.WorldSpace;

        // Set the canvas as a child of the camera so it moves with the camera
        canvasObject.transform.SetParent(cameraObject.transform);

        // Set Canvas dimensions and position in front of the camera
        RectTransform canvasRect = vrCanvas.GetComponent<RectTransform>();
        canvasRect.sizeDelta = new Vector2(400, 200); // Set canvas size
        canvasRect.localPosition = new Vector3(0, -70f, 40f); // Position 40 in front in local coordinates

        // Create TextMeshPro text GameObject
        GameObject textObject = new GameObject("VRMessageText");
        textObject.transform.SetParent(canvasObject.transform);
        textObject.transform.localRotation = Quaternion.Euler(0, 180, 0);

        // Add TextMeshProUGUI component
        vrMessageText = textObject.AddComponent<TextMeshProUGUI>();
        vrMessageText.text = ""; // Initial text
        vrMessageText.fontSize = 2; // Font size
        vrMessageText.alignment = TextAlignmentOptions.Center;

        // Set RectTransform properties for the first TextMeshProUGUI
        RectTransform textRect = vrMessageText.GetComponent<RectTransform>();
        textRect.sizeDelta = new Vector2(400, 100); // Set size for each text
        textRect.localPosition = new Vector3(0, 50, 0); // Position the first text slightly higher

        // Create the second text object (for time display)
        GameObject bottomTextObject = new GameObject("VRTimeText");
        bottomTextObject.transform.SetParent(canvasObject.transform);
        bottomTextObject.transform.localRotation = Quaternion.Euler(0, 180, 0);

        // Add TextMeshProUGUI component for the time text
        vrTimeText = bottomTextObject.AddComponent<TextMeshProUGUI>();
        vrTimeText.fontSize = 2; // Font size
        vrTimeText.alignment = TextAlignmentOptions.Center;

        // Set RectTransform properties for the second TextMeshProUGUI
        RectTransform timeTextRect = vrTimeText.GetComponent<RectTransform>();
        timeTextRect.sizeDelta = new Vector2(400, 100);
        timeTextRect.localPosition = new Vector3(0, 45, 0); // Position it below the first text
    }

    // VR: Function to update VR message text
    public void UpdateVRMessage(string message)
    {
        if (vrMessageText != null)
        {
            vrMessageText.text = message;
            CancelInvoke(nameof(ClearVRMessage));
            Invoke(nameof(ClearVRMessage), 4f); // Clear message after 3 seconds
        }
    }

    // VR: Clear VR message
    private void ClearVRMessage()
    {
        if (vrMessageText != null)
        {
            vrMessageText.text = "";
        }
    }

    // VR: Initialize the right controller for Oculus Quest
    private IEnumerator CheckForRightController()
    {
        while (!rightController.isValid) // Poll until we get a valid controller
        {
            List<InputDevice> devices = new List<InputDevice>();
            InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.Right | InputDeviceCharacteristics.Controller, devices);
            
            foreach (var device in devices)
            {
                rightController = device; // Assign the right controller when found
            }

            yield return new WaitForSeconds(1.0f); // Check again in 1 second
        }

        Debug.Log("***********[IPN]*********** Axis Right controller detected: " + rightController.name);
    }
    // VR: Initialize the right controller for Oculus Quest
    private IEnumerator CheckForLeftController()
    {
        while (!leftController.isValid) // Poll until we get a valid controller
        {
            List<InputDevice> devices = new List<InputDevice>();
            InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.Left | InputDeviceCharacteristics.Controller, devices);
            
            foreach (var device in devices)
            {
                leftController = device; // Assign the right controller when found
            }

            yield return new WaitForSeconds(1.0f); // Check again in 1 second
        }

        Debug.Log("***********[IPN]*********** Axis Left controller detected: " + leftController.name);
    }

    // Runs once every frame
    private void Update()
    {
        if (simulationStarted)
        {
            HandleSimulationTime();
            HandleOriginShiftTransitionTime();
            HandleInput();
        }

        // VR:
        if (XRSettings.isDeviceActive){
            DateTime currentDateTime = new DateTime(2000, 1, 1, 11, 58, 55, 816).AddSeconds(currentContinuousTime);
            if (centeredObjectOriginShift.name == "Bundle_401_1")
                vrTimeText.text = $"{currentDateTime:yyyy/MM/dd - HH:mm:ss} ({speedUp}x)\nCentered on: Your Bundle (ETA: 00:18:24)";
            else
                vrTimeText.text = $"{currentDateTime:yyyy/MM/dd - HH:mm:ss} ({speedUp}x)\nCentered on: {centeredObjectOriginShift.name}";
        }
    }

    // Runs once every frame after Update()
    private void LateUpdate()
    {
        HandleOriginShiftValueUpdate();
        UpdateContacts();
    }
    /// <summary>
    /// Handle update of the simulation time.
    /// </summary>
    /// <remarks>
    /// This function MUST be called in Update, since it MUST run before planet movements are updated.
    /// This function MUST be called first in the Update function.
    /// </remarks>
    private void HandleSimulationTime()
    {
        if (currentStepTime < endTime)
        {
            if (!pause)
            {
                // If the next known time step has not been reached yet
                if (elapsedTime < timeToReachTargetPosition)
                {
                    // Start by calculating the current continous simulation time (i.e. interpolate between the current and next known
                    // time step to get the value of the time at that frame). This MUST be done before updating elapsedTime.
                    double nextTime = currentStepTime + step * multiplicationFactor;
                    currentContinuousTime = currentStepTime + (nextTime - currentStepTime) * (elapsedTime / timeToReachTargetPosition);

                    // Update the (real life) time elapsed since the last known step in simulation time was reached. This is done by
                    // adding, at every frame, the interval in seconds from the last frame to the current one.
                    elapsedTime += Time.unscaledDeltaTime;           
                }
                // If the next known time step has been reached
                else
                {
                    // Reset the time elasped
                    elapsedTime = 0;
                    lastTime = currentStepTime;

                    // The next time step has been reached and it becomes the current time step
                    currentStepTime += multiplicationFactor * step;

                    // The continous time becomes the new current time step
                    currentContinuousTime = currentStepTime;
                }
            }
        }
        else
        {
            // Go back to the beginning of the simulation
            PickSpecificTime(startTime);
        }
    }

    /// <summary>
    /// Handle update of the origin shift transition time when moving from one centered object to the other.
    /// A timer (elapsedTimeTransitionOriginShift) that goes from 0 to transitionTimeOriginShift is used to know
    /// how advanced we are in the transition. Based on this time value, the Origin Shift value will be updated
    /// in HandleOriginShiftValueUpdate to have smooth transition from one origin to the other.
    /// </summary>
    /// <remarks>
    /// This function MUST be called in Update, since it MUST run before the update of the Origin Shift value.
    /// </remarks>
    private void HandleOriginShiftTransitionTime()
    {
        // If a shift origin transition has started (i.e. if we are going from one object to the other)
        if (isTransitioningOriginShift)
        {
            // If we are in the middle of the transition
            if (elapsedTimeTransitionOriginShift < transitionTimeOriginShift)
            {
                // Update the time elapsed since the origin shift transition started
                elapsedTimeTransitionOriginShift += Time.unscaledDeltaTime;

                // Make sure the camera is looking at the objet during the transition
                cameraController.KeepObjectInFocusDuringTransition();
            }
            // If the transition time is reached (i.e. if the newly centered object has reached the origin)
            else
            {
                // Reset the elapsed time for the next transition
                elapsedTimeTransitionOriginShift = 0;

                // Transition is over
                isTransitioningOriginShift = false;

                // Focus the camera on the object if the camera transition is not taking care of it
                if(!cameraController.cameraIsZoomingDuringTransition)
                {
                    cameraController.lookingAtObject = centeredObjectOriginShift;
                }
            }
        }
    }

    /// <summary>
    /// Handle update of the origin shift value.
    /// </summary>
    /// <remarks>
    /// This function MUST be called in LateUpdate, since it MUST run after object movements are updated.
    /// This function MUST be called after simulation time and origin shift transition time have been updated
    /// </remarks>
    private void HandleOriginShiftValueUpdate()
    {
        if (centeredObjectOriginShift!=null)
        {
            // Get the current non shifted position of the planet
            double[] centeredObjectCurrentNonShiftedPos = getObjectNonShiftedPosition(centeredObjectOriginShift);

            // If there is no ongoing Origin Shift transition
            if (!isTransitioningOriginShift)
            {
                // Assign the object's position as the Origin Shift value
                originShiftValue = centeredObjectCurrentNonShiftedPos;
            }
            //If there is an ongoing Origin Shift transition
            else
            {
                // Get the current non shifted position of the last object that was centered 
                double[] lastCenteredObjectPosition = getObjectNonShiftedPosition(lastCenteredObjectOriginShift);

                // Lerp between the current non shifted position of the last object centered and the target Origin Shift value (the current
                // non shifted position of the object to be centered), based on the Origin Shift timer. This will ensure there is a smooth transition.
                originShiftValue = Util.SmoothLerpDouble(lastCenteredObjectPosition, centeredObjectCurrentNonShiftedPos, elapsedTimeTransitionOriginShift / transitionTimeOriginShift);
            }
        }
       
        // If no object is centered
        else
        {
            originShiftValue = new double[] { 0, 0, 0 };
        }

        //TODO handle freefly Origin Shift
    }

    /// <summary>
    /// Get the object's non shifted position variable (depending on if it's a planet, node or bundle
    /// </summary>
    double[] getObjectNonShiftedPosition(GameObject obj)
    {
        double[] objectPosition = { 0, 0, 0 }; ;

        switch (obj.tag)
        {
            case "planet":
                objectPosition = obj.GetComponent<PlanetController>().currentPosNonShifted;
                break;

            case "node":
                objectPosition = obj.GetComponent<NodeController>().globalCurrentPosNonShifted;
                break;

            case "bundle":
                objectPosition = obj.GetComponent<BundleController>().currentPosNonShifted;
                break;
        }

        return objectPosition;
    }

    /// <summary>
    /// Function called when user asks to center a planet/node/bundle.
    /// </summary>
    public void TriggerOriginShift(GameObject objectToBeCentered)
    {
        // Remember what was the last object that was centered
        lastCenteredObjectOriginShift = centeredObjectOriginShift;

        if (centeredObjectOriginShift == objectToBeCentered)
            return;

        if (!isTransitioningOriginShift && !cameraController.cameraIsZoomingDuringTransition)
        {
            // Remember which object was centered last centered
            cameraController.lastObjectLookedAt = cameraController.lookingAtObject;

            float distanceLastObjectCenteredAndNewObjectCentered = Vector3.Distance(centeredObjectOriginShift.transform.position, objectToBeCentered.transform.position);

            // If the two objects are superimposed, skip the transition
            if (distanceLastObjectCenteredAndNewObjectCentered == 0)
            {
                cameraController.lookingAtObject = objectToBeCentered;
            }

            // If they are close to each other, don't perform zoom in / zoom out for the camera and make a short transition
            else if (distanceLastObjectCenteredAndNewObjectCentered < 10)
            {
                Debug.Log("Object is close");
                transitionTimeOriginShift = 2;

                // The look at parameter will be set when the camera transition is over
                cameraController.lookingAtObject = null;

                // Start the transition for the IPNv controller
                isTransitioningOriginShift = true;
            }

            // If they are far away from each other, make a long transition and zoom in / zoom out
            else
            {
                transitionTimeOriginShift = 3;

                // The look at parameter will be set when the camera transition is over
                cameraController.lookingAtObject = null;

                // Start the transition for the IPNv controller
                isTransitioningOriginShift = true;

                // Start the transition for the camera 
                cameraController.cameraIsZoomingDuringTransition = true;
            }

            // The passed GameObject is the object that will be centered after transition
            centeredObjectOriginShift = objectToBeCentered;
        }
    }

    //For testing purposes
    //void TriggerOriginShift(GameObject objectToBeCentered)
    //{
    //    if (centeredObjectOriginShift == objectToBeCentered)
    //        return;

    //    isTransitioningOriginShift = true;

    //    cameraController.lastObjectLookedAt = cameraController.lookingAtObject;

    //    //cameraController.lookingAtObject = objectToBeCentered;


    //    // The passed GameObject is the object that will be centered after transition
    //    centeredObjectOriginShift = objectToBeCentered;
    //}

    /// <summary>
    /// Handle user input.
    /// </summary>
    private void HandleInput()
    {
        ///////////////
        // UI functions
        ///////////////
        // Open filters window
        if (!uiHelpController.isHelpWindowOpen)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                uiFiltersController.ToggleFiltersWindowVisibility(!uiFiltersController.isFilterWindowOpen);
            }
        }
        // Open help window
        //if (Input.GetKeyDown(KeyCode.H) || (leftController.isValid && leftController.TryGetFeatureValue(CommonUsages.primaryButton, out bool isXPressed) && isXPressed))
        if (!uiFiltersController.isFilterWindowOpen)
        {
            if (Input.GetKeyDown(KeyCode.H) ||
            (leftController.isValid &&
            (leftController.TryGetFeatureValue(CommonUsages.primaryButton, out bool isXPressed) && isXPressed) ||
            (leftController.TryGetFeatureValue(CommonUsages.secondaryButton, out bool isYPressed) && isYPressed)))
            {
                uiHelpController.ToggleHelpWindowVisibility(!uiHelpController.isHelpWindowOpen);
            }
        }

        // Only allow for other type of inputs if the filters window is closed
        if (!uiFiltersController.isFilterWindowOpen && !uiHelpController.isHelpWindowOpen)
        {
            //////////////////////// 
            // Time functions
            ////////////////////////

            // Pause
            if (Input.GetKeyDown(KeyCode.Space)) PauseSimulation(!pause);

            // Speedup
            if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.UpArrow)) SpeedUpHandler("true");
            if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.DownArrow)) SpeedUpHandler("false");

            // Pick times
            if (Input.GetKeyDown(KeyCode.LeftArrow)) PickSpecificTime(currentStepTime - step);
            if (Input.GetKeyDown(KeyCode.RightArrow)) PickSpecificTime(currentStepTime + step);

            //////////////////////// 
            // VR functions
            ////////////////////////
            
            // if (rightController.isValid && rightController.TryGetFeatureValue(CommonUsages.triggerButton, out bool isRTriggerPressed) && isRTriggerPressed) PauseSimulation(!pause);
            // if (leftController.isValid && leftController.TryGetFeatureValue(CommonUsages.triggerButton, out bool isLTriggerPressed) && isLTriggerPressed) PauseSimulation(!pause);

            // // Check right controller trigger press (single trigger down)
            // if (rightController.isValid && rightController.TryGetFeatureValue(CommonUsages.triggerButton, out bool isRTriggerPressed))
            // {
            //     if (isRTriggerPressed && !previousRTriggerState)
            //     {
            //         PauseSimulation(!pause);
            //         UpdateVRMessage(pause ? "Simulation\nPaused" : "Simulation\nRunning");
            //         audioSourceClick.Play();
            //     }
            //     previousRTriggerState = isRTriggerPressed;
            // }

            // // Check left controller trigger press (single trigger down)
            // if (leftController.isValid && leftController.TryGetFeatureValue(CommonUsages.triggerButton, out bool isLTriggerPressed))
            // {
            //     if (isLTriggerPressed && !previousLTriggerState)
            //     {
            //         PauseSimulation(!pause);
            //         UpdateVRMessage(pause ? "Simulation\nPaused" : "Simulation\nRunning");
            //         audioSourceClick.Play();
            //     }
            //     previousLTriggerState = isLTriggerPressed;
            // }

            // Check right controller trigger press
            bool isRTriggerPressed = false;
            if (rightController.isValid && rightController.TryGetFeatureValue(CommonUsages.triggerButton, out isRTriggerPressed))
            {
                // Update the previous state outside the if condition for both triggers
            }

            // Check left controller trigger press
            bool isLTriggerPressed = false;
            if (leftController.isValid && leftController.TryGetFeatureValue(CommonUsages.triggerButton, out isLTriggerPressed))
            {
                // Update the previous state outside the if condition for both triggers
            }

            // Check if both triggers are pressed simultaneously, and if they weren't both pressed in the previous frame
            if (isRTriggerPressed && isLTriggerPressed && (!previousRTriggerState || !previousLTriggerState))
            {
                // Toggle the simulation pause state
                PauseSimulation(!pause);
                UpdateVRMessage(pause ? "Simulation\nPaused" : "Simulation\nRunning");
                audioSourceClick.Play();
            }

            // Update previous states after checking both triggers
            previousRTriggerState = isRTriggerPressed;
            previousLTriggerState = isLTriggerPressed;

            // if (rightController.isValid && rightController.TryGetFeatureValue(CommonUsages.gripButton, out bool isRGripPressed) && isRGripPressed) SpeedUpHandler("true");
            if (rightController.isValid && rightController.TryGetFeatureValue(CommonUsages.gripButton, out bool isRGripPressed))
            {
                if (isRGripPressed && !previousRGripState)
                {
                    SpeedUpHandler("true");
                    UpdateVRMessage(speedUp == 1 ? "SpeedUp: " + speedUp + "x\n(real time)" : "SpeedUp: " + speedUp + "x");
                    audioSourceClick.Play();
                }
                previousRGripState = isRGripPressed;
            }
            // if (leftController.isValid && leftController.TryGetFeatureValue(CommonUsages.gripButton, out bool isLGripPressed) && isLGripPressed) SpeedUpHandler("false");
            if (leftController.isValid && leftController.TryGetFeatureValue(CommonUsages.gripButton, out bool isLGripPressed))
            {
                if (isLGripPressed && !previousLGripState)
                {
                    SpeedUpHandler("false");
                    UpdateVRMessage(speedUp == 1 ? "SpeedUp: " + speedUp + "x\n(real time)" : "SpeedUp: " + speedUp + "x");
                    audioSourceClick.Play();
                }
                previousLGripState = isLGripPressed;
            }

            ////////////////////////////////
            // Transition functions (full)
            ////////////////////////////////
            if (!isTransitioningOriginShift && !cameraController.cameraIsZoomingDuringTransition)
            {

                // Keyboard: Centering a bundle
                if (Input.GetKeyDown(KeyCode.B) || (rightController.isValid && rightController.TryGetFeatureValue(CommonUsages.primaryButton, out bool isAPressed) && isAPressed))
                {
                    GameObject bundleToCenter = bundleList[currentlyFocusedBundleIndex];

                    TriggerOriginShift(bundleToCenter);

                    // Next time we press B, the next bundle in the list will be focused
                    if (currentlyFocusedBundleIndex < bundleList.Count - 1)
                        currentlyFocusedBundleIndex++;
                    else
                        currentlyFocusedBundleIndex = 0;
                }

                // Keyboard: Centering a node
                if (Input.GetKeyDown(KeyCode.N) || (rightController.isValid && rightController.TryGetFeatureValue(CommonUsages.secondaryButton, out bool isBPressed) && isBPressed))
                {
                    GameObject nodeToCenter = nodeList[currentlyFocusedNodeIndex];

                    TriggerOriginShift(nodeToCenter);

                    // Next time we press N, the next node in the list will be focused
                    if (currentlyFocusedNodeIndex < nodeList.Count - 1)
                        currentlyFocusedNodeIndex++;
                    else
                        currentlyFocusedNodeIndex = 0;
                }
            }

            //////////////////////////////////////////////////////////////
            // Demo: Transition functions (limited to Earth, Mars and Bundle for the demo)
            //////////////////////////////////////////////////////////////
            //if (!isTransitioningOriginShift && !cameraController.cameraIsZoomingDuringTransition)
            //{
            //    // Action A: Iterate between Mars, Earth and Bundle
            //    if (Input.GetKeyDown(KeyCode.B) || (rightController.isValid && rightController.TryGetFeatureValue(CommonUsages.primaryButton, out bool isAPressed) && isAPressed))
            //    {
            //        // Get the current object to center
            //        GameObject objectToCenter = focusObjects[currentFocusIndex];

            //        // Trigger the origin shift to this object
            //        TriggerOriginShift(objectToCenter);
            //        audioSourceClick.Play();

            //        if (objectToCenter.name == "Bundle_401_1"){
            //            UpdateVRMessage("Transitioning to:\nYour Bundle");
            //        } else {
            //            UpdateVRMessage("Transitioning to:\n" + objectToCenter.name);
            //        }

            //        // Update the index for next focus
            //        currentFocusIndex = (currentFocusIndex + 1) % focusObjects.Count;
            //    }

            //    // Action B: Iterate between all planets
            //    if (Input.GetKeyDown(KeyCode.N) || (rightController.isValid && rightController.TryGetFeatureValue(CommonUsages.secondaryButton, out bool isBPressed) && isBPressed)){

            //        // Convert dictionary keys to a list
            //        List<string> planetKeys = new List<string>(planets.Keys);

            //        // Access the key at the given index
            //        string planetKey = planetKeys[currentlyFocusedPlanetIndex];

            //        // Get the GameObject associated with the key
            //        GameObject planetToCenter = planets[planetKey];

            //        TriggerOriginShift(planetToCenter);
            //        UpdateVRMessage("Transitioning to:\n" + planetToCenter.name);
            //        audioSourceClick.Play();

            //        // Next time, the next node in the list will be focused
            //        if (currentlyFocusedPlanetIndex < planets.Count - 1)
            //            currentlyFocusedPlanetIndex++;
            //        else
            //            currentlyFocusedPlanetIndex = 0;
            //    }
            //}
        }
    }

    private void Initialize()
    {
        cameraController = GameObject.Find("Camera Rig").GetComponent<CameraController>();
        cameraObject = GameObject.Find("Main Camera").GetComponent<Camera>();

        cameraObject.clearFlags = CameraClearFlags.Skybox;
        cameraObject.GetComponent<Skybox>().enabled = true;

        uiController = uiMainCanvas.GetComponent<UIController>();
        uiFiltersController = uiFiltersCanvas.GetComponent<UIFiltersController>();
        uiHelpController = uiHelpCanvas.GetComponent<UIHelpController>();

        pause = true;
    }


    public void PauseSimulation(bool setPause){
        pause = setPause;
        uiController.UpdateButtonState();
        Debug.Log("***********[IPN]*********** Pause: " + pause);
    }

    /// <summary>
    /// Create/destroy links based on the list of contacts. Hide/show contacts based on the activation
    /// status set by the filters.
    /// </summary>
    private void UpdateContacts()
    {
        if (!pause)
        {
            if (lastTime != currentStepTime)
            {
                // Check for contacts that have started between the current step time and the last one
                // (take into account the multiplication factor in case points are being skipped to
                // accelerate the simulation).
                for (double t = currentStepTime; t >= currentStepTime - multiplicationFactor * step; t -= step)
                {
                    if (contacts.ContainsKey(t))
                    {
                        foreach (Contact c in contacts[t])
                        {
                            // If the ongoing contact has not been added to the list of ongoing contacts yet,
                            // add it and instantiate the link.
                            if (!onGoingContacts.Contains(c))
                            {
                                onGoingContacts.Add(c);
                                GameObject newLink = Instantiate(linkPrefab, c.source.transform);
                                LinkController linkController = newLink.GetComponent<LinkController>();
                                linkController.contact = c;

                                if (c.color != new Color(0, 0, 0, 0))
                                {
                                    Material linkMat = newLink.GetComponent<Renderer>().material;
                                    linkMat.SetColor("_EmissionColor", c.color * 2);
                                    linkMat.SetColor("_BaseColor", c.color);
                                }
                                c.link = newLink;
                            }
                        }
                    }
                }
            }

            // Destroy the link if the contact came to an end.
            foreach (Contact c in onGoingContacts.ToList())
            {
                if (c.endTime + c.link.GetComponent<LinkController>().lightTravelTime <= currentStepTime)
                {
                    onGoingContacts.Remove(c);
                    Destroy(c.link);
                }

            }

            lastTime = currentStepTime;
        }

        // Hide/show the contact based on the status set by the filters.
        foreach (Contact c in onGoingContacts)
        {
            LinkController linkController = c.link.GetComponent<LinkController>();

            linkController.ToggleLinkVisibility(c.visible);
            if (c.highlighted == true)
            {
                linkController.ChangeLinkColor(c.highlightedColor);
            }
            if (c.highlighted == false)
            {
                linkController.ChangeLinkColor(linkBaseColor);
            }
        }

    }

        public void PickSpecificTime(double chosenTime)
    {
        double closestValue = FindClosestTimestamp(chosenTime);
        double percentage = (chosenTime - closestValue) / step;
        elapsedTime = (float)percentage * timeToReachTargetPosition;

        //Debug.Log("PickSpecificTime: currentContinuousTime -> " + currentContinuousTime);
        currentContinuousTime = chosenTime;
        //Debug.Log("PickSpecificTime: currentContinuousTime -> " + currentContinuousTime);
        currentStepTime = closestValue;

        UpdatePositionsImmediately();
        RecreateLinks(closestValue);
    }

    private double FindClosestTimestamp(double chosenTime)
    {
        double closestValue = startTime;
        double minDifference = Math.Abs(chosenTime - startTime);

        for (double i = startTime; i <= endTime; i += step)
        {
            double difference = chosenTime - i;
            if (difference >= 0 && difference < minDifference)
            {
                minDifference = difference;
                closestValue = i;
            }
        }
        return closestValue;
    }

    private void UpdatePositionsImmediately()
    {
        foreach (var kvp in planets)
        {
            kvp.Value.GetComponent<PlanetController>().UpdatePositionImmediately();
        }

        //TODO check if this is still useful
        foreach (var kvp in nodes)
        {
            kvp.Value.GetComponent<NodeController>().UpdatePositionImmediately();
        }

    }

    private void RecreateLinks(double closestValue)
    {
        foreach (Contact c in onGoingContacts)
        {
            Destroy(c.link);
        }
        onGoingContacts.Clear();

        for (double t = closestValue; t >= startTime; t -= step)
        {
            if (contacts.ContainsKey(t))
            {
                foreach (Contact c in contacts[t])
                {
                    if (c.endTime > closestValue)
                    {
                        onGoingContacts.Add(c);
                        GameObject newLink = Instantiate(linkPrefab, c.source.transform);
                        LinkController linkController = newLink.GetComponent<LinkController>();
                        linkController.contact = c;
                        c.link = newLink;

                        if (c.color != new Color(0, 0, 0, 0))
                        {
                            Material linkMat = newLink.GetComponent<Renderer>().material;
                            linkMat.SetColor("_EmissionColor", c.color);
                            linkMat.SetColor("_BaseColor", c.color);
                        }
                    }
                }
            }
        }
    }

    public void SpeedUpHandler(string increase)
    {
        double targetSpeedUp = increase == "true" ? speedUp * 2 : speedUp / 2;

        // Limit speedUp to reasonable values
        if(targetSpeedUp < 1 || targetSpeedUp > 10000) return;

        bool multiplicationFactorReachedMin = increase == "false" && multiplicationFactor == 1 ? true : false; 

        if (timeToReachTargetPosition >= Time.deltaTime || multiplicationFactorReachedMin)
        {
            float percentage = elapsedTime / timeToReachTargetPosition;
            timeToReachTargetPosition = (float)(step / targetSpeedUp * multiplicationFactor);
            elapsedTime = timeToReachTargetPosition * percentage;
        }
        else
        {
            multiplicationFactor = increase == "true" ? multiplicationFactor * 2 : multiplicationFactor / 2;
        }
        speedUp = step / timeToReachTargetPosition * multiplicationFactor;

        Debug.Log("***********[IPN]*********** SpeedUp: " + speedUp);
    }

    ///////////////////// 
    /// Parsing in WebGL
    ///////////////////// 

    //Called from the web app
    void GetJsonDataFromWeb(string received)
    {
        ImportData.WebData globalJsonObject = JsonUtility.FromJson<ImportData.WebData>(received);
        webSimulationData = globalJsonObject.configData;

        Debug.Log(webSimulationData.Time.SimulationStartTime);

        webContactPlanData = globalJsonObject.contactData;
        webBundlePlanData = globalJsonObject.bundlePlanData;

        foreach (ImportData.PlanetPositionData planet in globalJsonObject.planetPositions)
        {
            string planetName = planet.Name;
            webPlanetsPositionsData.Add(planetName, planet);
        }

        foreach (ImportData.NodePositionData node in globalJsonObject.nodePositions)
        {
            int nodeId = node.ID;
            webNodesPositionsData.Add(nodeId, node);
        }

        WebParseConfigFile();

        cameraObject.clearFlags = CameraClearFlags.Skybox;
        cameraObject.GetComponent<Skybox>().enabled = true;

        simulationStarted = true;

#if UNITY_WEBGL && !UNITY_EDITOR
        SendReady();
#endif
    }

    void WebParseConfigFile()
    {
        star = webSimulationData.Star;
        GameObject NewStar = Instantiate(starPrefab, new Vector3(0f, 0f, 0f), Quaternion.identity);
        StarGameObject = NewStar;
        NewStar.transform.localScale = new Vector3(2f, 2f, 2f) * star.Radius / currentScale;

        NewStar.name = star.Name;
        starTransform = NewStar.transform;

        GameObject.Find("Sun FX").gameObject.SetActive(false);

        startTime = webSimulationData.Time.SimulationStartTime;
        currentStepTime = startTime;
        endTime = webSimulationData.Time.SimulationEndTime;
        step = webSimulationData.Time.Step;
        timeToReachTargetPosition = (float)step;
        speedUp = step / timeToReachTargetPosition * multiplicationFactor * 1;

        foreach (ImportData.PlanetData p in webSimulationData.Planets)
        {
            WebParseJsonPlanetFile(p);
            InstantiatePlanetFromData(p);

            foreach (ImportData.NodeData n in p.Nodes)
            {
                n.CentralObject = p;

                GameObject parent = planets[p.Name];

                WebParseJsonNodeFile(n);
                InstantiateNodeFromData(n, parent);
            }
        }
        WebParseJsonContactFile();
        WebParseJsonBundlePlanFile();
        cameraController.transform.position = new Vector3(0, 0, NewStar.transform.lossyScale.x * 30);
        cameraController.transform.LookAt(NewStar.transform);
    }

    void WebParseJsonBundlePlanFile()
    {
        foreach (ImportData.BundleData bundleData in webBundlePlanData.Bundles)
        {
            GameObject newBundle = Instantiate(bundlePrefab);
            newBundle.name = bundleData.Name;
            newBundle.tag = "bundle";

            bundleList.Add(newBundle);

            BundleController controller = newBundle.GetComponent<BundleController>();

            // Loop through each hop and set it up in the BundleController
            foreach (var hopData in bundleData.Hops)
            {
                // Create a new BundleHop
                BundleController.BundleHop hop = new BundleController.BundleHop();

                // Assign the sender and receiver nodes based on their IDs
                hop.SenderNode = GameObject.Find(hopData.SenderNode);
                hop.ReceiverNode = GameObject.Find(hopData.ReceiverNode);
                hop.SendTime = hopData.SendTime;

                // Add the hop to the controller's list
                controller.hops.Add(hop);
            }
        }


    }

    void WebParseJsonContactFile()
    {
        try
        {
            foreach (ImportData.ContactData contact in webContactPlanData.ContactPlan)
            {
                GameObject source = GameObject.Find(contact.SourceID.ToString());
                GameObject destination = GameObject.Find(contact.DestinationID.ToString());

                double contactStartTime = contact.StartTime;
                double contactEndTime = contact.EndTime;
                double contactDuration = contact.Duration;

                Contact newContact = new(source, destination, contactStartTime, contactEndTime, contactDuration);

                if (contacts.ContainsKey(contactStartTime))
                {
                    contacts[contactStartTime].Add(newContact);
                }
                else
                {
                    contacts[contactStartTime] = new List<Contact> { newContact };
                }

                if (contact.Color.Length != 0) 
                {
                    Debug.Log(contact.Color[1] / 255);
                    Color linkColor = new Color(contact.Color[0] / 255, contact.Color[1] / 255, contact.Color[2] / 255);
                    newContact.color = linkColor;
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log("Error parsing the contact plan : " + e.Message);
        }
    }

    void WebParseJsonNodeFile(ImportData.NodeData n)
    {
        ImportData.NodePositionData currentNodePositionData = webNodesPositionsData[n.ID];

        foreach (ImportData.NodePositionSpecificTimeData entry in currentNodePositionData.Positions)
        {
            double time = entry.Time;
            Vector3 currentPosition = new Vector3((float)entry.PositionX, (float)entry.PositionY, (float)entry.PositionZ);
            n.events[time] = currentPosition;
        }
    }

    void WebParseJsonPlanetFile(ImportData.PlanetData p)
    {
        ImportData.PlanetPositionData currentPlanetPositionData = webPlanetsPositionsData[p.Name];
        foreach (ImportData.PlanetPositionSpecificTimeData entry in currentPlanetPositionData.Positions)
        {
            double time = entry.Time;
            Vector3 currentPosition = new Vector3((float)entry.PositionX, (float)entry.PositionY, (float)entry.PositionZ);
            Quaternion currentRotation = Quaternion.Euler((float)entry.RotationX, (float)entry.RotationY, (float)entry.RotationZ);
            //COMMENTED
            //p.events[time] = (currentPosition, currentRotation);
        }
    }

    ///////////////////// ////
    /// Parsing in the Editor
    /// (useless in webGL)
    ////////////////////////// 

    void ParseConfigFile()
    {
        // string configFile = File.ReadAllText(scenarioPath + "config.json");
        TextAsset configTextAsset = Resources.Load<TextAsset>(scenarioFolder + "config");
        string configFile = configTextAsset.text;

        ImportData.SimulationData simulationData = JsonUtility.FromJson<ImportData.SimulationData>(configFile);

        star = simulationData.Star;
        GameObject NewStar = Instantiate(starPrefab, new Vector3(0f, 0f, 0f), Quaternion.identity);
        NewStar.transform.localScale = new Vector3(2f, 2f, 2f) * star.Radius / currentScale;
        //NewStar.transform.localScale = new Vector3(4000f, 4000f, 4000f);

        //GameObject cameraObject = GameObject.Find("global camera (following)");
        //cameraObject.transform.SetParent(NewStar.transform);

        NewStar.name = star.Name;
        StarGameObject = NewStar;

        starTransform = NewStar.transform;
        GameObject.Find("Sun FX").gameObject.SetActive(false);

        startTime = simulationData.Time.SimulationStartTime;
        currentStepTime = startTime;
        endTime = simulationData.Time.SimulationEndTime;
        step = simulationData.Time.Step;
        timeToReachTargetPosition = (float)step;
        speedUp = step / timeToReachTargetPosition * multiplicationFactor * 1;

        foreach (ImportData.PlanetData p in simulationData.Planets)
        {
            ParseJsonPlanetFile(p);
            InstantiatePlanetFromData(p);

            foreach (ImportData.NodeData n in p.Nodes)
            {
                n.CentralObject = p;

                GameObject parent = planets[p.Name];

                ParseJsonNodeFile(n);
                InstantiateNodeFromData(n, parent);
            }
        }

        ParseJsonContactFile();
        ParseBundlePlan();

        cameraController.transform.position = new Vector3(0, 0, NewStar.transform.lossyScale.x * 30);
        cameraController.transform.LookAt(NewStar.transform);
    }

    
    void ParseJsonContactFile()
    {
        try
        {
            // string contactPlanJson = File.ReadAllText(scenarioPath + "contactPlan.json");
            TextAsset configTextAsset = Resources.Load<TextAsset>(scenarioFolder + "contactPlan");
            string contactPlanJson = configTextAsset.text;

            ImportData.ContactPlanData contactPlanData = JsonUtility.FromJson<ImportData.ContactPlanData>(contactPlanJson);

            foreach (ImportData.ContactData contact in contactPlanData.ContactPlan)
            {
                GameObject source = GameObject.Find(contact.SourceID.ToString());
                GameObject destination = GameObject.Find(contact.DestinationID.ToString());

                double contactStartTime = contact.StartTime;
                double contactEndTime = contact.EndTime;
                double contactDuration = contact.Duration;

                Contact newContact = new(source, destination, contactStartTime, contactEndTime, contactDuration);


                if (contacts.ContainsKey(contactStartTime))
                {
                    contacts[contactStartTime].Add(newContact);
                }
                else
                {
                    contacts[contactStartTime] = new List<Contact> { newContact };
                }

                if (contact.Color.Length != 0)
                {
                    Color linkColor = new Color(contact.Color[0] / 255, contact.Color[1] / 255, contact.Color[2] / 255);
                    newContact.color = linkColor;
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log("Error parsing the contact plan : " + e.Message);
        }
    }

    void ParseBundlePlan(){

        // string jsonText = File.ReadAllText(scenarioPath + "bundlePlan.json");
        TextAsset configTextAsset = Resources.Load<TextAsset>(scenarioFolder + "bundlePlan");
        string jsonText = configTextAsset.text;

        ImportData.BundlePlan bundlePlan = JsonUtility.FromJson<ImportData.BundlePlan>(jsonText);

        foreach (ImportData.BundleData bundleData in bundlePlan.Bundles)
        {
            GameObject newBundle = Instantiate(bundlePrefab);
            newBundle.name = bundleData.Name;
            newBundle.tag = "bundle";

            bundleList.Add(newBundle);

            BundleController controller = newBundle.GetComponent<BundleController>();
            
            // Loop through each hop and set it up in the BundleController
            foreach (var hopData in bundleData.Hops)
            {
                // Create a new BundleHop
                BundleController.BundleHop hop = new BundleController.BundleHop();

                // Assign the sender and receiver nodes based on their IDs
                hop.SenderNode = GameObject.Find(hopData.SenderNode);
                hop.ReceiverNode = GameObject.Find(hopData.ReceiverNode);
                hop.SendTime = hopData.SendTime;

                // Add the hop to the controller's list
                controller.hops.Add(hop);
            }
        }


    }

    void ParseJsonNodeFile(ImportData.NodeData n)
    {
        // string filePath = scenarioPath + "nodes/" + n.ID + ".json";
        try
        {
            // string currentNodeJson = File.ReadAllText(filePath);
            TextAsset configTextAsset = Resources.Load<TextAsset>(scenarioFolder + "nodes/" + n.ID);
            string currentNodeJson = configTextAsset.text;

            ImportData.NodePositionData nodePositionData = JsonUtility.FromJson<ImportData.NodePositionData>(currentNodeJson);

            foreach (ImportData.NodePositionSpecificTimeData entry in nodePositionData.Positions)
            {
                double time = entry.Time;
                Vector3 currentPosition = new Vector3((float)entry.PositionX, (float)entry.PositionY, (float)entry.PositionZ);
                n.events[time] = currentPosition;
            }
        }
        catch (Exception e)
        {
            Debug.Log($"Error parsing file {scenarioFolder + "nodes/" + n.ID}: {e.Message}");
        }
    }

    void ParseJsonPlanetFile(ImportData.PlanetData p)
    {
        // string filePath = scenarioPath + "planets/" + p.Name + ".json";
        try
        {
            // string currentPlanetJson = File.ReadAllText(filePath);
            TextAsset configTextAsset = Resources.Load<TextAsset>(scenarioFolder + "planets/" + p.Name);
            string currentPlanetJson = configTextAsset.text;

            ImportData.PlanetPositionData planetPositionData = JsonUtility.FromJson<ImportData.PlanetPositionData>(currentPlanetJson);

            foreach (ImportData.PlanetPositionSpecificTimeData entry in planetPositionData.Positions)
            {
                double time = entry.Time;
                Vector3 currentPosition = new Vector3((float)entry.PositionX, (float)entry.PositionY, (float)entry.PositionZ);
                Quaternion currentRotation = Quaternion.Euler((float)entry.RotationX, (float)entry.RotationY, (float)entry.RotationZ);
                double[] DcurrentPosition = new double[] { entry.PositionX, entry.PositionY, entry.PositionZ };  
                p.events[time] = (DcurrentPosition, currentRotation);
            }
        }
        catch (Exception e)
        {
            Debug.Log($"Error parsing file {scenarioFolder + "planets/" + p.Name}: {e.Message}");
        }
    }

    ////////////////////////////// 
    /// GameObjects instantiation
    ////////////////////////////// 

    void InstantiateNodeFromData(ImportData.NodeData n, GameObject p)
    {
        Vector3 scaledCurrentPosition = n.events[currentStepTime] / currentScale;

        GameObject NewNode = Instantiate(nodePrefab, scaledCurrentPosition, Quaternion.identity);
        NewNode.tag = "node";

        nodeList.Add(NewNode);

        NodeController nodeController = NewNode.GetComponentInParent<NodeController>();
        nodeController.events = n.events;
        nodeController.CentralObject = p;
        nodeController.NodeName = n.Name;
        nodeController.OrbitPeriod = n.OrbitPeriod;
        NewNode.name = n.ID.ToString();
        NewNode.transform.SetParent(p.transform);

        p.GetComponent<PlanetController>().childNodes.Add(NewNode);

        nodes.Add(n.ID, NewNode);

        float nodeScale = 0.15f;
        NewNode.transform.localScale = new Vector3(1, 1, 1) * nodeScale;
    }

    void InstantiatePlanetFromData(ImportData.PlanetData p)
    {
        Dictionary<string, GameObject> planetWithNumPrefabDictionary = new Dictionary<string, GameObject>
        {
            { "1-Mercury", mercuryPrefab },
            { "2-Venus", venusPrefab },
            { "3-Earth", earthPrefab },
            { "4-Mars", marsPrefab },
            { "5-Jupiter", jupiterPrefab },
            { "6-Saturn", saturnPrefab },
            { "7-Uranus", uranusPrefab },
            { "8-Neptune", neptunePrefab },
            { "9-Pluto", plutoPrefab }
        };

        Dictionary<string, GameObject> planetPrefabDictionary = new Dictionary<string, GameObject>
        {
            { "Mercury", mercuryPrefab },
            { "Venus", venusPrefab },
            { "Earth", earthPrefab },
            { "Mars", marsPrefab },
            { "Jupiter", jupiterPrefab },
            { "Saturn", saturnPrefab },
            { "Uranus", uranusPrefab },
            { "Neptune", neptunePrefab },
            { "Pluto", plutoPrefab }
        };

        Vector3 scaledCurrentPosition = Util.DoubleArrayToVector3(Util.DivideArrayByFloat(p.events[currentStepTime].position, currentScale));
        Quaternion currentRotation = p.events[currentStepTime].rotation;
        Vector3 scaledPlanetDiameter = new Vector3(2f, 2f, 2f) * p.Radius / currentScale;
        //Vector3 scaledPlanetDiameter = new Vector3(4000f, 4000f, 4000f);

        GameObject NewPlanet;

        if (planetPrefabDictionary.ContainsKey(p.Name))
        {
            NewPlanet = Instantiate(planetPrefabDictionary[p.Name], scaledCurrentPosition, currentRotation);
            NewPlanet.transform.localScale = 0.5f * scaledPlanetDiameter;
            NewPlanet.tag = "planet";

            for (int i = 0; i < NewPlanet.transform.childCount; i++)
            {
                if (NewPlanet.transform.GetChild(i).name == "Glow")
                {
                    break;
                }
            }
        }
        else if (planetWithNumPrefabDictionary.ContainsKey(p.Name))
        {
            NewPlanet = Instantiate(planetWithNumPrefabDictionary[p.Name], scaledCurrentPosition, currentRotation);
            NewPlanet.transform.localScale = 0.5f * scaledPlanetDiameter;

            for (int i = 0; i < NewPlanet.transform.childCount; i++)
            {
                if (NewPlanet.transform.GetChild(i).name == "Glow")
                {
                    NewPlanet.transform.GetChild(i).gameObject.SetActive(false);
                    break;
                }
            }
        }
        else
        {
            NewPlanet = Instantiate(planetPrefab, scaledCurrentPosition, currentRotation);
            NewPlanet.transform.localScale = scaledPlanetDiameter;
        }

        PlanetController planetController = NewPlanet.GetComponent<PlanetController>();

        planetController.Name = p.Name;
        planetController.events = p.events;
        planetController.Radius = p.Radius;
        planetController.OrbitPeriod = p.OrbitPeriod;

        // Orbital params
        planetController.SemiMajorAxis = p.SemiMajorAxis;
        planetController.Eccentricity = p.Eccentricity;
        planetController.Inclination = p.Inclination;
        planetController.Raan = p.Raan;
        planetController.Argp = p.ArgOfPeriapsis;
        planetController.MeanAnomaly = p.MeanAnomaly;

        planetController.Obliquity = p.Obliquity;
        planetController.RotationPeriod = p.RotationPeriod;

        NewPlanet.name = p.Name;

        planets.Add(p.Name, NewPlanet);
    }
}
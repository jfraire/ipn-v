﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIHelpController : MonoBehaviour
{
    // Reference to other components
    UIFiltersController uiFiltersController;
    UIController uiController;
    CanvasFader canvasFader;

    // Canvas
    public Canvas uiMainCanvas;
    public Canvas uiFiltersCanvas;
    public Canvas uiHelpCanvas;

    public bool isHelpWindowOpen = false;

    // Buttons
    public Button closeHelpWindowButton;

    public Image imageManual;

    // Use this for initialization
    void Start()
    {
        uiFiltersController = GameObject.Find("UI Filters Canvas").GetComponent<UIFiltersController>();
        uiController = GameObject.Find("UI Main Canvas").GetComponent<UIController>();
        closeHelpWindowButton.onClick.AddListener(() => ToggleHelpWindowVisibility(false));
        canvasFader = gameObject.GetComponent<CanvasFader>();

        uiFiltersController = uiFiltersCanvas.GetComponent<UIFiltersController>();
        uiController = uiMainCanvas.GetComponent<UIController>();
    }

    public void ToggleHelpWindowVisibility(bool visibility)
    {
        if (uiFiltersController.isFilterWindowOpen)
            return;

        if (visibility)
        {
            canvasFader.FadeIn(uiHelpCanvas.GetComponent<CanvasGroup>(), 0.2f);
        }
        else
        {
            canvasFader.FadeOut(uiHelpCanvas.GetComponent<CanvasGroup>(), 0.2f);
        }

        //Hide the rest of the UI
        uiController.ToggleMainUIVisibility(!uiController.isMainUiVisible);

        isHelpWindowOpen = visibility; // Toggle the state

    }
}

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UITimeSlider : Slider
{
    public bool isDragging;

    // Override OnPointerDown to capture click on slider
    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        isDragging = true;
        // Block raycasts to other UI elements
        EventSystem.current.SetSelectedGameObject(this.gameObject);
    }

    // Override OnPointerUp to release the capture
    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        isDragging = false;
        // Allow raycasts to other UI elements again
        EventSystem.current.SetSelectedGameObject(null);
    }

    // Override OnDrag to handle dragging on the slider
    public override void OnDrag(PointerEventData eventData)
    {
        if (isDragging)
        {
            base.OnDrag(eventData);
        }
    }

    // You can also override OnPointerEnter and OnPointerExit if needed
}

﻿using UnityEngine;
using System.Collections.Generic;
using System;


public class NodeController : MonoBehaviour
{
    ////////////////
    /// System data
    ////////////////

    //IPN-v Controller and camera objects
    public IPNvController ipnvController;
    public CameraController cam;

    //Global scale variable
    float currentScale;

    ///////////////////////
    /// Position variables
    ///////////////////////

    // Start and target positions used to interpolate and get the current position of the node
    // (relative to its planet) without taking into account Origin Shift.
    public Vector3 localStartPosNonShifted;
    public Vector3 localTargetPosNonShifted;

    // The current position of the node without Origin Shift (relative to its planet).
    public Vector3 localCurrentPosNonShifted = Vector3.zero;

    // The current position of the node without Origin Shift (global position). This value
    // (and not transform.position) MUST be used when centering the object using Origin Shift
    // since it is in double precision.
    public double[] globalCurrentPosNonShifted;

    ///////////////
    /// Node data
    ///////////////

    public int ID;
    public string NodeName;
    public double OrbitPeriod;
    public GameObject CentralObject; 
    public Dictionary<double, Vector3> events = new(); //List of positions
    public bool visible = false;

    ////////////////////
    /// Node trajectory
    ////////////////////
 
    LineRenderer lineOrbitRenderer;
    int necessaryPoints;

    private void Awake()
    {
        ipnvController = GameObject.Find("IPNvController").GetComponent<IPNvController>();
        cam = GameObject.Find("Main Camera").GetComponent<CameraController>();
    }

    void Start()
    {
        //Get scale from ipnvController
        currentScale = ipnvController.currentScale;

        //Move node to its initial position
        transform.position = (events[ipnvController.currentStepTime])/currentScale + transform.parent.position;

        localStartPosNonShifted = (events[ipnvController.currentStepTime]) / currentScale;
        localCurrentPosNonShifted = localStartPosNonShifted;

        //Line orbit renderer
        if (OrbitPeriod != 0) // AKA if the node is an orbiter
        {
            //Calculate the number of points necessary to trace the full orbit
            necessaryPoints = (int)(OrbitPeriod / ipnvController.step + 2);
         
            //If there are more points necessary to trace the full orbit than timestamps in the simulation, limit the number of points and trace a partial orbit
            if (necessaryPoints > events.Count)
                necessaryPoints = events.Count;

            lineOrbitRenderer = gameObject.AddComponent<LineRenderer>();
            lineOrbitRenderer.positionCount = necessaryPoints;
            lineOrbitRenderer.material = new Material(Shader.Find("Sprites/Default"));
            lineOrbitRenderer.startWidth = 0.1f * transform.lossyScale.x; // make sure the orbit is always 10% of the node's width
            lineOrbitRenderer.endWidth = 0.1f * transform.lossyScale.x;
            lineOrbitRenderer.startColor = Color.gray;
            lineOrbitRenderer.endColor = Color.gray;
        }
        // Update the values of variables holding the local and global positions of the node (without taking into account Origin Shift)
        UpdateLocalAndGlobalCurrentPosNonShifted();
    }

    private void Update()
    {
        if (ipnvController.currentStepTime != ipnvController.endTime)
        {
            // Update the values of variables holding the local and global positions of the node (without taking into account Origin Shift)
            UpdateLocalAndGlobalCurrentPosNonShifted();
        }
    }


    private void LateUpdate()
    {
        // Move the node based on the current values calculated in Update
        MoveNode();

        UpdateLineOrbit();

        //HandleNodeVisibility();
    }

    /// <summary>
    /// Show/hide the node depending on the visibility status set by the filters
    /// </summary>
    private void HandleNodeVisibility()
    {
        Debug.Log("doing it " + visible);
        gameObject.GetComponent<Renderer>().enabled = visible;

        foreach (IPNvController.Contact c in ipnvController.onGoingContacts)
        {
            if(c.source == gameObject || c.destination == gameObject)
            {
            }
        }
    }

    /// <summary>
    /// Update the value of the variables holding the current local and global positions of the node (without taking into
    /// account Origin Shift). This function DOES NOT move the node GameObject, it simply updates the localCurrentPosNonShifted
    /// and globalCurrentPosNonShifted variables.
    /// </summary>
    /// <remarks>
    /// This function MUST be called in Update() since it MUST run before Origin Shift is updated. 
    /// </remarks>
    public void UpdateLocalAndGlobalCurrentPosNonShifted()
    {
        localTargetPosNonShifted = (events[ipnvController.currentStepTime + ipnvController.step * ipnvController.multiplicationFactor]) / currentScale;

        // If the simulation timer is between two points in time where the position is exactly known, perform interpolation
        if (ipnvController.elapsedTime < ipnvController.timeToReachTargetPosition)
        {
            // Update local position value
            localCurrentPosNonShifted = Vector3.Lerp(localStartPosNonShifted, localTargetPosNonShifted, ipnvController.elapsedTime / ipnvController.timeToReachTargetPosition);

            // Update absolute position value (sum of the parent's current non shifted position and the node's local position)
            globalCurrentPosNonShifted  = Util.AddArrays(
                transform.parent.transform.GetComponent<PlanetController>().currentPosNonShifted,
                new double[] { localCurrentPosNonShifted.x, localCurrentPosNonShifted.y, localCurrentPosNonShifted.z }
            );

        }
        // Else, if the simulation timer reached a point in time where the position is exactly known, assign the exact values
        else
        {
            // Assign global position value
            globalCurrentPosNonShifted = Util.AddArrays(
              transform.parent.transform.GetComponent<PlanetController>().currentPosNonShifted,
              new double[] { localTargetPosNonShifted.x, localTargetPosNonShifted.y, localTargetPosNonShifted.z }
            );

            // Assign local position value
            localCurrentPosNonShifted = localTargetPosNonShifted;

            // The target position becomes the new start position for the next frame
            localStartPosNonShifted = localTargetPosNonShifted;
        }
    }

    /// <summary>
    /// Move the node based on the localCurrentPosNonShifted variable calculated in UpdateLocalAndGlobalCurrentPosNonShifted().
    /// </summary>
    /// <remarks>
    /// This function MUST be called in LateUpdate() since it MUST run after the Origin Shift value has been updated.
    /// </remarks>
    public void MoveNode()
    {
        transform.position = Util.DoubleArrayToVector3(Util.SubtractArrays(globalCurrentPosNonShifted, ipnvController.originShiftValue));
    }

    /// <summary>
    /// Handle immediate node movement (for example when sliding the time bar)
    /// </summary>
    public void UpdatePositionImmediately()
    {
        if (currentScale == 0) // Called before initialized
            return;
  
        ////Update start and target transition variables
        localStartPosNonShifted = events[ipnvController.currentStepTime] / currentScale;
        localTargetPosNonShifted = events[ipnvController.currentStepTime + ipnvController.step * ipnvController.multiplicationFactor] / currentScale;

        //Move node to the correct position between start and target
        transform.position = Vector3.Lerp(localStartPosNonShifted, localTargetPosNonShifted, ipnvController.elapsedTime / ipnvController.timeToReachTargetPosition) + transform.parent.position;

        UpdateLineOrbit();
    }


    public void UpdateLineOrbit()
    {
        if (OrbitPeriod != 0)
        {
            Vector3[] points = new Vector3[necessaryPoints];

            for (int i = 0; i < necessaryPoints; i++)
            {
                double time = ipnvController.startTime + ipnvController.step * i;
                points[i] = (events[time]) / currentScale + CentralObject.transform.position; // local orbital position + parent planet transform.position
            }

            lineOrbitRenderer.SetPositions(points);
        }
    }
}


using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

// VR: Library
using UnityEngine.XR;

public class UIController : MonoBehaviour
{
    // References to other components
    private IPNvController ipnvController;
    private CameraController cameraController;
    private Camera mainCamera;
    private UIFiltersController uiFiltersController;
    private UIHelpController uiHelpController;
    private CanvasFader canvasFader;

    // Other canvas
    public Canvas uiMainCanvas;
    public Canvas uiFiltersCanvas;
    public Canvas uiHelpCanvas;
    public Canvas canvasPlanetButtons;
    // Buttons
    public Button speedUpButton;
    public Button speedDownButton;
    public Button playStopButton;
    public Button rotateRightButton;
    public Button rotateLeftButton;
    public Button zoomInButton;
    public Button zoomOutButton;
    public Button buttonFilters;
    public Button buttonHelp;

    // Raw images
    public RawImage playIcon;
    public RawImage pauseIcon;


    // Slider
    public UITimeSlider timeSlider;

    // UI elements
    private TextMeshProUGUI bottomText;
    private Dictionary<string, TextMeshProUGUI> nodesTextMeshProUGUIDict;
    private Dictionary<string, TextMeshProUGUI> planetTextMeshProUGUIDict;


    // Prefabs
    public GameObject PlanetButtonPrefab;


    // State variables
    private bool firstUpdate = true;
    private double currentScale;
    public bool isMainUiVisible = true;

    // // VR: Controllers
    // private InputDevice rightController;
    // private InputDevice leftController;

    // Initialization
    void Start()
    {
        // // VR: Initialize the controllers for Oculus Quest
        // StartCoroutine(CheckForRightController());
        // StartCoroutine(CheckForLeftController());

        ipnvController = GameObject.Find("IPNvController").GetComponent<IPNvController>();
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        cameraController = GameObject.Find("Camera Rig").GetComponent<CameraController>();

        uiFiltersController = uiFiltersCanvas.GetComponent<UIFiltersController>();
        uiHelpController = uiHelpCanvas.GetComponent<UIHelpController>();

        currentScale = ipnvController.currentScale;
        canvasFader = gameObject.GetComponent<CanvasFader>();

        // Buttons
        speedUpButton.onClick.AddListener(SpeedUp);
        speedDownButton.onClick.AddListener(SpeedDown);
        playStopButton.onClick.AddListener(TogglePlayStop);
        buttonFilters.onClick.AddListener(OpenFiltersWindow);
        buttonHelp.onClick.AddListener(() => uiHelpController.ToggleHelpWindowVisibility(true));
        rotateLeftButton.onClick.AddListener(cameraController.ToggleContinuousRotationRight);
        rotateRightButton.onClick.AddListener(cameraController.ToggleContinuousRotationLeft);
        zoomInButton.onClick.AddListener(cameraController.ToggleContinuousZoomIn);
        zoomOutButton.onClick.AddListener(cameraController.ToggleContinuousZoomOut);

        // Play/pause button
        UpdateButtonState();

        // Planet buttons
        CreatePlanetButtons();

        // Time slider start
        timeSlider.minValue = (float)ipnvController.startTime;
        timeSlider.maxValue = (float)ipnvController.endTime;
        timeSlider.value = (float)ipnvController.currentStepTime;
        // Programmatically add the OnSliderValueChanged listener
        timeSlider.onValueChanged.AddListener(OnSliderValueChanged);

        // VR: Disable childs visibility if XR defice is present
        if (XRSettings.isDeviceActive)
        {
            ToggleMainUIVisibility(false); 
        }
        else
        {
            ToggleMainUIVisibility(true); 
        }
    }

    public void OpenFiltersWindow()
    {
        uiFiltersController.ToggleFiltersWindowVisibility(true);
    }

    public void ToggleMainUIVisibility(bool visibility)
    {
        if (visibility)
        {
            canvasFader.FadeIn(uiMainCanvas.GetComponent<CanvasGroup>(), 0.2f);
        }
        else
        {
            canvasFader.FadeOut(uiMainCanvas.GetComponent<CanvasGroup>(), 0.2f);
        }

        isMainUiVisible = visibility;

    }

    // // VR: Initialize the right controller for Oculus Quest
    // private IEnumerator CheckForRightController()
    // {
    //     while (!rightController.isValid) // Poll until we get a valid controller
    //     {
    //         List<InputDevice> devices = new List<InputDevice>();
    //         InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.Right | InputDeviceCharacteristics.Controller, devices);

    //         foreach (var device in devices)
    //         {
    //             //Debug.Log("Connected device: " + device.name + ", characteristics: " + device.characteristics);
    //             rightController = device; // Assign the right controller when found
    //         }

    //         yield return new WaitForSeconds(1.0f); // Check again in 1 second
    //     }

    //     Debug.Log("***********[IPN]*********** Button Right controller detected: " + rightController.name);
    // }
    // // VR: Initialize the right controller for Oculus Quest
    // private IEnumerator CheckForLeftController()
    // {
    //     while (!leftController.isValid) // Poll until we get a valid controller
    //     {
    //         List<InputDevice> devices = new List<InputDevice>();
    //         InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.Left | InputDeviceCharacteristics.Controller, devices);

    //         foreach (var device in devices)
    //         {
    //             //Debug.Log("Connected device: " + device.name + ", characteristics: " + device.characteristics);
    //             leftController = device; // Assign the right controller when found
    //         }

    //         yield return new WaitForSeconds(1.0f); // Check again in 1 second
    //     }

    //     Debug.Log("***********[IPN]*********** Button Left controller detected: " + leftController.name);
    // }


    // Update UI elements
    void LateUpdate()
    {
        if (ipnvController.simulationStarted)
        {
            if (!XRSettings.isDeviceActive){

                if (firstUpdate)
                {
                    InitializeUIElements();
                    firstUpdate = false;
                }

                timeSlider.onValueChanged.RemoveListener(OnSliderValueChanged);
                timeSlider.value = (float)ipnvController.currentStepTime;
                timeSlider.onValueChanged.AddListener(OnSliderValueChanged);

                UpdatePlanetLabels();
                //UpdateNodeLabels();
                UpdateBottomText();

                timeSlider.onValueChanged.RemoveListener(OnSliderValueChanged);
                timeSlider.value = (float)ipnvController.currentStepTime;
                timeSlider.onValueChanged.AddListener(OnSliderValueChanged);
            }

            // // VR: Check if the Right A button is pressed
            // if (rightController.isValid && rightController.TryGetFeatureValue(CommonUsages.primaryButton, out bool isAPressed) && isAPressed)
            // {
            //     ipnvController.centeredObjectOriginShift = GameObject.Find("Earth");
            //     cameraController.lookingAtObject = ipnvController.centeredObjectOriginShift;
            // }
            // // VR: Check if the Right B button is pressed
            // if (rightController.isValid && rightController.TryGetFeatureValue(CommonUsages.secondaryButton, out bool isBPressed) && isBPressed)
            // {
            //     ipnvController.centeredObjectOriginShift = GameObject.Find("Mars");
            //     cameraController.lookingAtObject = ipnvController.centeredObjectOriginShift;
            // }
            // // VR: Check if the Left X button is pressed
            // if (leftController.isValid && leftController.TryGetFeatureValue(CommonUsages.primaryButton, out bool isXPressed) && isXPressed)
            // {
            //     //TogglePlayStop();
            // }
            // // VR: Check if the Left Y button is pressed
            // if (leftController.isValid && leftController.TryGetFeatureValue(CommonUsages.secondaryButton, out bool isYPressed) && isYPressed)
            // {
            //     //TogglePlayStop();
            // }
        }
    }

    // Initialize UI elements for planets and nodes
    private void InitializeUIElements()
    {
        planetTextMeshProUGUIDict = new Dictionary<string, TextMeshProUGUI>();
        foreach (var kvp in ipnvController.planets)
        {
            GameObject planet = kvp.Value;
            string planetName = planet.name;

            TextMeshProUGUI planetTextMeshProUGUI = CreateTextMeshProElement(planetName, 18, Color.white);
            planetTextMeshProUGUIDict.Add(planet.name, planetTextMeshProUGUI);
        }

        nodesTextMeshProUGUIDict = new Dictionary<string, TextMeshProUGUI>();
        foreach (var kvp in ipnvController.nodes)
        {
            GameObject node = kvp.Value;
            string nodeName = node.name;

            TextMeshProUGUI nodesTextMeshProUGUI = CreateTextMeshProElement(nodeName, 15, Color.white, false);
            nodesTextMeshProUGUIDict.Add(node.name, nodesTextMeshProUGUI);
        }

        bottomText = CreateBottomTextElement();
    }

    void TogglePlayStop()
    {
        ipnvController.PauseSimulation(!ipnvController.pause);
    }

    public void UpdateButtonState()
    {
        if (!ipnvController.pause)
        {
            playIcon.gameObject.SetActive(false);
            pauseIcon.gameObject.SetActive(true);
        }
        else
        {
            playIcon.gameObject.SetActive(true);
            pauseIcon.gameObject.SetActive(false);
        }
    }

    void SpeedUp()
    {
        ipnvController.SpeedUpHandler("true");
    }

    void SpeedDown()
    {
        ipnvController.SpeedUpHandler("false");
    }

    public void OnSliderValueChanged(float value)
    {
        ipnvController.PickSpecificTime(value);
    }

    // Create a TextMeshProUGUI element for planets and nodes
    private TextMeshProUGUI CreateTextMeshProElement(string name, int fontSize, Color color, bool enabled = true)
    {
        GameObject textObject = new GameObject(name + "TextMeshProUGUI");
        TextMeshProUGUI textMeshProUGUI = textObject.AddComponent<TextMeshProUGUI>();
        textObject.transform.SetParent(uiMainCanvas.transform, false);

        textMeshProUGUI.rectTransform.anchoredPosition = new Vector2(0, 0);
        textMeshProUGUI.text = name;
        textMeshProUGUI.fontSize = fontSize;
        textMeshProUGUI.color = color;
        textMeshProUGUI.verticalAlignment = VerticalAlignmentOptions.Middle;
        textMeshProUGUI.horizontalAlignment = HorizontalAlignmentOptions.Center;
        textMeshProUGUI.enabled = enabled;

        return textMeshProUGUI;
    }

    // Create the bottom text element for displaying time, speed, etc.
    private TextMeshProUGUI CreateBottomTextElement()
    {
        GameObject bottomTextObject = new GameObject("bottomTextMeshProUGUI");
        TextMeshProUGUI bottomText = bottomTextObject.AddComponent<TextMeshProUGUI>();
        bottomTextObject.transform.SetParent(uiMainCanvas.transform, false);

        RectTransform rectTransform = bottomText.rectTransform;
        rectTransform.anchorMin = new Vector2(0.5f, 0f);
        rectTransform.anchorMax = new Vector2(0.5f, 0f);
        rectTransform.pivot = new Vector2(0.5f, 0f);
        rectTransform.anchoredPosition = new Vector2(0, 40);

        bottomText.alignment = TextAlignmentOptions.Center;
        bottomText.fontSize = 15;
        bottomText.color = Color.white;

        return bottomText;
    }

    // Update planet labels based on their positions relative to the camera
    private void UpdatePlanetLabels()
    {
        foreach (var kvp in ipnvController.planets)
        {
            GameObject planet = kvp.Value;
            Vector3 screenPos = mainCamera.WorldToScreenPoint(planet.transform.position + new Vector3(0, 1.5f * (float)(planet.GetComponent<PlanetController>().Radius / (currentScale * 2f)), 0));

            if (screenPos.z > 0)
            {
                UpdateTextMeshProPosition(planetTextMeshProUGUIDict[planet.name], screenPos);
                planetTextMeshProUGUIDict[planet.name].enabled = true;
            }
            else
            {
                planetTextMeshProUGUIDict[planet.name].enabled = false;
            }
        }
    }

    // Update node labels for the currently selected planet
    private void UpdateNodeLabels()
    {
        GameObject currentObject = cameraController.lookingAtObject;

        if (ShouldHidePreviousNodeLabels(currentObject))
        {
            HideNodeLabelsForPreviousPlanet();
        }

        if (cameraController.lookingAtObject != null && !cameraController.freeFly)
        {
            if (cameraController.lookingAtObject.GetComponent<PlanetController>() != null)
                UpdateNodeLabelsForCurrentPlanet(currentObject);   
        }
    }

    // Check if node labels from the previous planet should be hidden
    private bool ShouldHidePreviousNodeLabels(GameObject currentObject)
    {
        return (cameraController.lastObjectLookedAt != null && currentObject != cameraController.lastObjectLookedAt) ||
               (currentObject == null && cameraController.lastObjectLookedAt != null) ||
               (cameraController.freeFly && cameraController.lastObjectReached);
    }

    // Hide node labels for the previously looked-at planet
    private void HideNodeLabelsForPreviousPlanet()
    {
        if (cameraController.lastObjectLookedAt.GetComponent<PlanetController>() != null)
            foreach (GameObject node in cameraController.lastObjectLookedAt.GetComponent<PlanetController>().childNodes)
            {
                nodesTextMeshProUGUIDict[node.name].enabled = false;
            }
    }

    // Update node labels for the currently looked-at planet
    private void UpdateNodeLabelsForCurrentPlanet(GameObject currentObject)
    {
        foreach (GameObject node in currentObject.GetComponent<PlanetController>().childNodes)
        {
            if (CheckIfInField(node, mainCamera))
            {
                Vector3 screenPos = mainCamera.WorldToScreenPoint(node.transform.position);
                UpdateTextMeshProPosition(nodesTextMeshProUGUIDict[node.name], screenPos);
                nodesTextMeshProUGUIDict[node.name].enabled = true;
            }
            else
            {
                nodesTextMeshProUGUIDict[node.name].enabled = false;
            }
        }
    }

    // Update the bottom text with the current time, speed, and frame rate
    private void UpdateBottomText()
    {
        DateTime currentDateTime = new DateTime(2000, 1, 1, 11, 58, 55, 816).AddSeconds(ipnvController.currentContinuousTime);
        string speedUpString = FormatSpeedUp(ipnvController.speedUp);
        bottomText.text = $"\n{currentDateTime:yyyy/MM/dd - HH:mm:ss}\n{speedUpString}x \n Centered on " + ipnvController.centeredObjectOriginShift.name;
    }

    // Helper method to format the speed-up value
    private string FormatSpeedUp(double speedUp)
    {
        if (speedUp >= 1000000)
            return (speedUp / 1000000f).ToString("###.#") + "M";
        else if (speedUp >= 1000)
            return (speedUp / 1000f).ToString("###.#") + "k";
        else
            return speedUp.ToString("###.#");
    }

    // Update the position of a TextMeshProUGUI element on the screen
    private void UpdateTextMeshProPosition(TextMeshProUGUI textMeshProUGUI, Vector3 screenPos)
    {
        RectTransform canvasRect = textMeshProUGUI.canvas.GetComponent<RectTransform>();
        Vector2 screenPosInCanvas;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRect, screenPos, textMeshProUGUI.canvas.worldCamera, out screenPosInCanvas);
        textMeshProUGUI.rectTransform.anchoredPosition = screenPosInCanvas;
    }

    // Check if a GameObject is in the camera's field of view and unobstructed
    public static bool CheckPlanetInField(GameObject obj, Camera camera)
    {
        Vector3 viewportPos = camera.WorldToViewportPoint(obj.transform.position);
        bool inFieldOfVision = viewportPos.x >= 0 && viewportPos.x <= 1 &&
                               viewportPos.y >= 0 && viewportPos.y <= 1 &&
                               viewportPos.z > 0;

        if (!inFieldOfVision)
            return false;

        RaycastHit hit;
        Vector3 direction = obj.transform.position - camera.transform.position;

        if (Physics.Raycast(camera.transform.position, direction, out hit))
        {
            if (hit.collider.gameObject != obj)
            {
                inFieldOfVision = false;
            }
        }
        return inFieldOfVision;
    }

    // Check if a GameObject is within the camera's field of view
    public static bool CheckIfInField(GameObject obj, Camera camera)
    {
        Renderer renderer = obj.GetComponent<Renderer>();
        if (renderer == null)
        {
            Debug.LogError("The GameObject does not have a Renderer component.");
            return false;
        }

        Vector3 viewportPos = camera.WorldToViewportPoint(obj.transform.position);
        return viewportPos.x >= 0 && viewportPos.x <= 1 &&
               viewportPos.y >= 0 && viewportPos.y <= 1 &&
               viewportPos.z > 0;
    }

    private void CreatePlanetButtons()
    {
        foreach (var kvp in ipnvController.planets)
        {
            string planetName = kvp.Key;

            // Create the button
            GameObject buttonGameObject = Instantiate(PlanetButtonPrefab, canvasPlanetButtons.transform);
            buttonGameObject.name = "ButtonPlanet" + planetName;

            // Set the planet's first name as the button's text
            TextMeshProUGUI planetText = buttonGameObject.GetComponentInChildren<TextMeshProUGUI>();
            planetText.text = planetName.Substring(0, 1); // First letter of planet name

            // Add OnClick listener
            Button button = buttonGameObject.GetComponent<Button>();
            button.onClick.AddListener(() =>
            {
                if (!ipnvController.isTransitioningOriginShift && !cameraController.cameraIsZoomingDuringTransition)
                {
                    ipnvController.TriggerOriginShift(GameObject.Find(planetName));
                }
            });
        }
    }
}

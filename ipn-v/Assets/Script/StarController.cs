using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarController : MonoBehaviour
{
    IPNvController ipnvController;

    // Start is called before the first frame update
    void Start()
    {
        ipnvController = GameObject.Find("IPNvController").GetComponent<IPNvController>();
    }

    void LateUpdate()
    {
        // The sun is always placed at the center of the system
        transform.position = -Util.DoubleArrayToVector3(ipnvController.originShiftValue);
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using static IPNvController;
using System.Linq;

public class FiltersController : MonoBehaviour
{
    // Other components
    IPNvController ipnvController;
    UIFiltersController uiFiltersController;

    // Canvas
    public Canvas uiFiltersCanvas;

    // Available filter types
    public enum FilterType
    {
        Highlight,
        Show,
        Hide
    }

    // Class defining a contact filter
    public class ContactFilter
    {
        // Filter for two nodes
        public GameObject sourceNode;
        public GameObject destinationNode;

        // Type of filter (Show or Highlight)
        public FilterType filterType;

        // Highlight filter
        public Color color;

        // Is the filter currently active
        public bool isActive;

        public ContactFilter(GameObject sourceNode, GameObject destinationNode, FilterType filterType)
        {
            this.sourceNode = sourceNode;
            this.destinationNode = destinationNode;
            this.filterType = filterType;
        }

        override
        public string ToString()
        {
            if (sourceNode == null)
                return filterType + " any to " + destinationNode.name;
            if (destinationNode == null)
                return filterType + " " + sourceNode.name + " to any";
            else
                return filterType + " " + sourceNode.name + " to " + destinationNode.name;
        }
    }

    // List of all filters currently active
    public List<ContactFilter> existingFilters = new();

    // Priority list of highlight filters
    public List<ContactFilter> prioritiesHighlightFilters = new();

    //Priority list of visibility (show/hide) filters
    public List<ContactFilter> prioritiesVisibilityFilters = new();

    void Start()
    {
        ipnvController = GameObject.Find("IPNvController").GetComponent<IPNvController>();
        uiFiltersController = uiFiltersCanvas.GetComponent<UIFiltersController>();
    }


    /////////////////////////////////////////////////////////
    /// Functions to add/remove/activate/deactivate filters
    /////////////////////////////////////////////////////////

    /// <summary>
    /// Add (if it does not exist yet) the filter from one node to another node.
    /// </summary>
    /// <param name="filter">The filter we are trying to add</param>
    /// <returns>True if the filter was succefully added, false otherwise</returns>
    public bool AddTwoNodesContactFilter(ContactFilter filter)
    {
        /////////////////////////
        /// Check filter validity
        /////////////////////////

        // Check if there isn't already a duplicate filter in the list
        FiltersController.ContactFilter matchedFilter = existingFilters.FirstOrDefault(comparedFilter =>
           comparedFilter.sourceNode == filter.sourceNode && comparedFilter.destinationNode == filter.destinationNode && comparedFilter.filterType == filter.filterType
       );

        // If there is a duplicate or if one of the nodes does not exist, return;
        if(matchedFilter != null || filter.sourceNode == null || filter.destinationNode == null)
        {
            return false;
        }

        /////////////////
        /// Add filter
        /////////////////

        AddContactFilter(filter);

        return true;
    }


    /// <summary>
    /// Add (if it does not exist yet) the filter from/to one node.
    /// </summary>
    /// <param name="filter">The filter we are trying to add</param>
    /// <param name="onlySource">True if the filter is source only</param>
    /// <param name="onlyDestination">True if the filter is destination only</param>
    /// <returns>True if the filter was succefully added, false otherwise</returns>
    public bool AddOneNodeContactFilter(ContactFilter filter, bool onlySource, bool onlyDestination)
    {
        // Check if there isn't already a duplicate filter in the list
        FiltersController.ContactFilter matchedFilter = existingFilters.FirstOrDefault(comparedFilter =>
           comparedFilter.sourceNode == filter.sourceNode && comparedFilter.destinationNode == filter.destinationNode && comparedFilter.filterType == filter.filterType
       );

        /////////////////////////
        /// Check filter validity
        /////////////////////////

        // If there is a duplicate, break
        if (matchedFilter != null)
            return false;
        
        // If the filter is source only and the source node doesn't exist, break
        if(onlySource && filter.sourceNode == null)
            return false;
        
        // If the filter is destination only and the destination node doesn't exist, break
        if (onlyDestination && filter.destinationNode == null)
            return false;

        /////////////////
        /// Add filter
        /////////////////

        AddContactFilter(filter);

        return true;
    }

    /// <summary>
    /// Apply the filter.
    /// </summary>
    /// <param name="filter">The filter we are trying to add</param>
    public void AddContactFilter(ContactFilter filter)
    {
        if (filter.filterType == FilterType.Show)
        {
            // Check if there are any pre-existing show filters that are active
            FiltersController.ContactFilter existingShowFilters = existingFilters.FirstOrDefault(comparedFilter =>
                comparedFilter.filterType == FilterType.Show && comparedFilter.isActive == true
            );

            // If this is the first show filter to be applied, start by setting all contacts to invisible.
            // If there are pre-existing filters, the new one will superimpose itself to the rest.
            if (existingShowFilters == null)
            {
                MakeAllContactsInvisible();
            }

            // Set all from/to the node as visible. We don't need to re-apply all filters since this one
            // will take the highest priority.
            ShowContacts(filter);

            //Show filter in the UI list
            uiFiltersController.AddFilterToToggleList(filter, uiFiltersController.listContentVisibilityFilters);

            //Set the new filter to the highest priority in its category
            prioritiesVisibilityFilters.Insert(0, filter);
        }
        if(filter.filterType == FilterType.Hide)
        {
            // Check if there are any pre-existing show filters that are active
            FiltersController.ContactFilter existingShowFilters = existingFilters.FirstOrDefault(comparedFilter =>
                comparedFilter.filterType == FilterType.Hide && comparedFilter.isActive == true
            );

            // Set all from/to the node as invisible. We don't need to re-apply all filters since this one
            // will take the highest priority.
            HideContacts(filter);

            //Show filter in the UI list
            uiFiltersController.AddFilterToToggleList(filter, uiFiltersController.listContentVisibilityFilters);

            //Set the new filter to the highest priority in  its category
            prioritiesVisibilityFilters.Insert(0, filter);
        }

        else if (filter.filterType == FilterType.Highlight)
        {
            //Highlight all contacts selected by the filter
            HighlightContacts(filter);

            //Show filter in the UI list
            uiFiltersController.AddFilterToToggleList(filter, uiFiltersController.listContentHighlightFilters);

            //Set the new filter to the highest priority in  its category
            prioritiesHighlightFilters.Insert(0, filter);
        }

        filter.isActive = true;
        existingFilters.Add(filter);
    }

    /// <summary>
    /// Remove (if it exists) the filter matching the parameter.
    /// </summary>
    /// <param name="filter">The filter we are trying to remove</param>
    /// <returns>True if the filter was succefully removed, false otherwise</returns>
    public bool RemoveOneContactFilter(ContactFilter filter)
    {
        // Check that there is a filter matching the request
        FiltersController.ContactFilter matchedFilter = existingFilters.FirstOrDefault(comparedFilter =>
           comparedFilter.sourceNode == filter.sourceNode && comparedFilter.destinationNode == filter.destinationNode && comparedFilter.filterType == filter.filterType
           ) ;

        if(matchedFilter == null)
        {
            return false;
        }

        existingFilters.Remove(matchedFilter);
        uiFiltersController.RemoveFilterFromToggleList(filter);

        if (filter.filterType == FilterType.Show)
        {
            prioritiesVisibilityFilters.Remove(filter);
            // Check if there are any show filters that are still active
            FiltersController.ContactFilter existingShowFilters = existingFilters.FirstOrDefault(comparedFilter =>
                comparedFilter.filterType == FilterType.Show && comparedFilter.isActive == true
            );

            // If there are no more active show filters, make all contacts visible by default. The hide filters
            // will be applied in the next step
            if (existingShowFilters == null)
                MakeAllContactsVisible();
            else
                HideContacts(filter);


            // Re-apply all filters. Any other show or hide filter will be applied based on their priorities.
            ReApplyAllVisibilityFilters();
        }
        if(filter.filterType == FilterType.Hide)
        {
            prioritiesVisibilityFilters.Remove(filter);

            ShowContacts(filter);

            // Re-apply all filters. Any other show or hide filter will be applied based on their priorities.
            ReApplyAllVisibilityFilters();
        }
        if (filter.filterType == FilterType.Highlight)
        {
            prioritiesHighlightFilters.Remove(matchedFilter);
            DeHighlightContacts(filter);
        }

        return true;
    }

    /// <summary>
    /// Removes all active filters.
    /// </summary>
    public void RemoveAllFilters()
    {
        // Make all contacts visible
        MakeAllContactsVisible();

        // Remove all highlights
        DeHighlightAllContacts();

        // Remove all filters from the list
        foreach (ContactFilter filter in existingFilters.ToList())
        {
            existingFilters.Remove(filter);
            uiFiltersController.RemoveFilterFromToggleList(filter);

            if(filter.filterType == FilterType.Show || filter.filterType == FilterType.Hide)
            {
                prioritiesVisibilityFilters.Remove(filter);
            }
            if(filter.filterType == FilterType.Highlight)
            {
                prioritiesHighlightFilters.Remove(filter);
            }
        }
    }

    /// <summary>
    /// Activate/deactivate a filter without deleting it
    /// </summary>
    /// <param name="filter">The filter we want to activate/deactivate</param>
    /// <param name="status">True to activate, false to deactivate the filter</param>
    public void SwitchFilterStatus(ContactFilter filter, bool status)
    {
        if (status == true)
        {
            if (filter.filterType == FilterType.Show)
            {
                // Start by checking if there are any already active show filters.
                bool isThereAnyActiveShowFilter = existingFilters.Any(comparedFilter =>
                    comparedFilter.isActive == true && comparedFilter.filterType == FilterType.Show
                );

                // If there are no active show filters yet, hide all contacts by default so that the newly
                // applied show filter restricts the visible contacts to the one it concerns only. 
                if (!isThereAnyActiveShowFilter)
                    MakeAllContactsInvisible();
                else
                    ShowContacts(filter);

                // Set this filter's status to true
                filter.isActive = status;

                // Re-apply all filters. Any other show or hide filter will be applied based on their priorities.
                ReApplyAllVisibilityFilters();
            }
            if(filter.filterType == FilterType.Hide)
            {
                // Set this filter's status to true
                filter.isActive = status;

                HideContacts(filter);

                // Re-apply all filters. Any other show or hide filter will be applied based on their priorities.
                ReApplyAllVisibilityFilters();
            }
            if(filter.filterType == FilterType.Highlight)
            {
                HighlightContacts(filter);

                // Set this filter's status to true
                filter.isActive = status;
            }
        }
        else
        {
            if (filter.filterType == FilterType.Show)
            {
                // Start by setting this filter's status to false;
                filter.isActive = status;

                // Check if there are any other active filters
                bool isThereAnyActiveFilter = existingFilters.Any(comparedFilter =>
                    comparedFilter.isActive == true && comparedFilter.filterType == FilterType.Show
                );

                // If there are no more active show filters, make all contacts visible by default. The Hide filters
                // will be applied in the next step
                if (!isThereAnyActiveFilter)
                    MakeAllContactsVisible(); 
                else
                    HideContacts(filter);

                // Re-apply all filters. Any other show or hide filter will be applied based on their priorities.
                ReApplyAllVisibilityFilters();

            }
            if(filter.filterType == FilterType.Hide)
            {
                // Start by setting this filter's status to false;
                filter.isActive = status;

                ShowContacts(filter);

                // Re-apply all filters. Any other show or hide filter will be applied based on their priorities.
                ReApplyAllVisibilityFilters();
            }
            if (filter.filterType == FilterType.Highlight)
            {
                // Start by setting this filter's status to false;
                filter.isActive = status;

                DeHighlightContacts(filter);
            }
        }
    }


    //////////////////////////////////////////////////
    /// Functions to highlight/dehighlight contacts
    //////////////////////////////////////////////////

    /// <summary>
    /// Highlight contacts for a given filter (while respecting higher priority filters).
    /// </summary>
    /// <param name="filter">The filter to use</param>
    void HighlightContacts(ContactFilter filter)
    {
        foreach (List<Contact> listContacts in ipnvController.contacts.Values)
        {
            foreach (Contact c in listContacts)
            {
                //Check if there is no filter of higher priority setting the color of the contact
                int priority = prioritiesHighlightFilters.IndexOf(filter);

                ContactFilter higherPriorityFilter = prioritiesHighlightFilters.FirstOrDefault(comparedFilter =>
                    (comparedFilter.sourceNode == c.source && comparedFilter.destinationNode == c.destination && comparedFilter.isActive == true
                        && comparedFilter.filterType == FilterType.Highlight && prioritiesHighlightFilters.IndexOf(comparedFilter) < priority)
                    ||
                    (comparedFilter.sourceNode == c.source && comparedFilter.destinationNode == null && comparedFilter.isActive == true
                        && comparedFilter.filterType == FilterType.Highlight && prioritiesHighlightFilters.IndexOf(comparedFilter) < priority)
                    ||
                    (comparedFilter.sourceNode == null && comparedFilter.destinationNode == c.destination && comparedFilter.isActive == true
                    && comparedFilter.filterType == FilterType.Highlight && prioritiesHighlightFilters.IndexOf(comparedFilter) < priority)
                );

                if (higherPriorityFilter !=null)
                {
                    // do nothing
                }
                if (higherPriorityFilter == null)
                {

                    if (filter.sourceNode == null && c.destination == filter.destinationNode)
                    {
                        c.highlighted = true;
                        c.highlightedColor = filter.color;
                    }

                    else if (filter.destinationNode == null && c.source == filter.sourceNode)
                    {
                        c.highlighted = true;
                        c.highlightedColor = filter.color;
                    }

                    else if (c.source == filter.sourceNode && c.destination == filter.destinationNode && filter.sourceNode != null && filter.destinationNode != null)
                    {
                        c.highlighted = true;
                        c.highlightedColor = filter.color;
                    }
                }
            }
        }
    }

    /// <summary>
    /// De-highlight contacts for a given filter (while respecting higher priority filters).
    /// </summary>
    /// <param name="filter">The filter to use</param>
    void DeHighlightContacts(ContactFilter filter)
    {
        foreach (List<Contact> listContacts in ipnvController.contacts.Values)
        {
            foreach (Contact c in listContacts)
            {
                //We only want to de-highlight a contact if there is no other filter highlighting it. This function will return 
                //the first filter it finds matching the conditions in the priorities list, meaning that the next filter with the
                //highest priority will be used for this contact.
                ContactFilter otherFilterActivatedForThisContact = prioritiesHighlightFilters.FirstOrDefault(comparedFilter =>
                    (comparedFilter.sourceNode == c.source && comparedFilter.destinationNode == c.destination && comparedFilter.isActive == true)
                    ||
                    (comparedFilter.sourceNode == c.source && comparedFilter.destinationNode == null && comparedFilter.isActive == true)
                    ||
                    (comparedFilter.sourceNode == null && comparedFilter.destinationNode == c.destination && comparedFilter.isActive == true)
                );

                if (otherFilterActivatedForThisContact == null)
                {
                    if (filter.sourceNode == null && c.destination == filter.destinationNode)
                        c.highlighted = false;

                    else if (filter.destinationNode == null && c.source == filter.sourceNode)
                        c.highlighted = false;

                    else if (c.source == filter.sourceNode && c.destination == filter.destinationNode && filter.sourceNode != null && filter.destinationNode != null)
                        c.highlighted = false;
                }
                else
                {
                    if (filter.sourceNode == null && c.destination == filter.destinationNode)
                        c.highlightedColor = otherFilterActivatedForThisContact.color;

                    else if (filter.destinationNode == null && c.source == filter.sourceNode)
                        c.highlightedColor = otherFilterActivatedForThisContact.color;

                    else if (c.source == filter.sourceNode && c.destination == filter.destinationNode && filter.sourceNode != null && filter.destinationNode != null)
                        c.highlightedColor = otherFilterActivatedForThisContact.color;
                }
            }
        }
    }

    /// <summary>
    /// De-highlight all contacts.
    /// </summary>
    void DeHighlightAllContacts()
    {
        foreach (List<Contact> listContacts in ipnvController.contacts.Values)
        {
            foreach (Contact c in listContacts)
            {
                c.highlighted = false;
            }
        }
    }


    //////////////////////////////////////////////////
    /// Functions to show/hide contacts
    //////////////////////////////////////////////////

    /// <summary>
    /// Make all contacts invisible.
    /// </summary>
    void MakeAllContactsInvisible()
    {
        foreach (List<Contact> listContacts in ipnvController.contacts.Values)
        {
            foreach (Contact c in listContacts)
            {
                c.visible = false;
            }
        }
    }

    /// <summary>
    /// Make all contacts visible.
    /// </summary>
    void MakeAllContactsVisible()
    {
        foreach (List<Contact> listContacts in ipnvController.contacts.Values)
        {
            foreach (Contact c in listContacts)
            {
                c.visible = true;
            }
        }
    }

    /// <summary>
    /// Show contacts for a given filter (while respecting higher priority filters).
    /// </summary>
    /// <param name="filter">The filter to use</param>
    void ShowContacts(ContactFilter filter)
    {
        int priority = prioritiesVisibilityFilters.IndexOf(filter);

        foreach (List<Contact> listContacts in ipnvController.contacts.Values)
        {
            foreach (Contact c in listContacts)
            {
                //We only want to show a contact if there is no other filter of higher priority hiding it
                bool higherPriorityFilterActivatedForThisContact = existingFilters.Any(comparedFilter =>
                    (comparedFilter.sourceNode == c.source && comparedFilter.destinationNode == c.destination && comparedFilter.isActive == true
                        && comparedFilter.filterType == FilterType.Hide && prioritiesVisibilityFilters.IndexOf(comparedFilter) < priority)
                    ||
                    (comparedFilter.sourceNode == c.source && comparedFilter.destinationNode == null && comparedFilter.isActive == true
                        && comparedFilter.filterType == FilterType.Hide && prioritiesVisibilityFilters.IndexOf(comparedFilter) < priority)
                    ||
                    (comparedFilter.sourceNode == null && comparedFilter.destinationNode == c.destination && comparedFilter.isActive == true
                        && comparedFilter.filterType == FilterType.Hide && prioritiesVisibilityFilters.IndexOf(comparedFilter) < priority)
                );

                if (!higherPriorityFilterActivatedForThisContact)
                {
                    if (filter.sourceNode == null && c.destination == filter.destinationNode)
                        c.visible = true;

                    else if (filter.destinationNode == null && c.source == filter.sourceNode)
                        c.visible = true;

                    else if (c.source == filter.sourceNode && c.destination == filter.destinationNode && filter.sourceNode != null && filter.destinationNode != null)
                        c.visible = true;
                }
            }
        }
    }

    /// <summary>
    /// Hide contacts for a given filter (while respecting higher priority filters).
    /// </summary>
    /// <param name="filter">The filter to use</param>
    void HideContacts(ContactFilter filter)
    {
        int priority = prioritiesVisibilityFilters.IndexOf(filter);

        foreach (List<Contact> listContacts in ipnvController.contacts.Values)
        {
            foreach (Contact c in listContacts)
            {
                bool higherPriorityFilterActivatedForThisContact = false;

                // If this function was called with a Show filter, it means that we're trying to remove/deactivate it.
                // We should therefore try to hide all contacts included by this filter except if there is a higher
                // priority Show filter showing it.
                if (filter.filterType == FilterType.Show)
                {
                    //We only want to hide a contact if there is no other filter of higher priority showing it
                    higherPriorityFilterActivatedForThisContact = existingFilters.Any(comparedFilter =>
                        (comparedFilter.sourceNode == c.source && comparedFilter.destinationNode == c.destination && comparedFilter.isActive == true
                            && comparedFilter.filterType == FilterType.Show)
                        ||
                        (comparedFilter.sourceNode == c.source && comparedFilter.destinationNode == null && comparedFilter.isActive == true
                            && comparedFilter.filterType == FilterType.Show)
                        ||
                        (comparedFilter.sourceNode == null && comparedFilter.destinationNode == c.destination && comparedFilter.isActive == true
                            && comparedFilter.filterType == FilterType.Show)
                    );
                }

                // If this function was called with a Hide filter, then we should try to hide all the contacts included
                // in this filter 
                else if (filter.filterType == FilterType.Hide)
                {
                    //We only want to hide a contact if there is no other filter of higher priority showing it
                    higherPriorityFilterActivatedForThisContact = existingFilters.Any(comparedFilter =>
                        (comparedFilter.sourceNode == c.source && comparedFilter.destinationNode == c.destination && comparedFilter.isActive == true
                            && comparedFilter.filterType == FilterType.Show && prioritiesVisibilityFilters.IndexOf(comparedFilter) < priority)
                        ||
                        (comparedFilter.sourceNode == c.source && comparedFilter.destinationNode == null && comparedFilter.isActive == true
                            && comparedFilter.filterType == FilterType.Show && prioritiesVisibilityFilters.IndexOf(comparedFilter) < priority)
                        ||
                        (comparedFilter.sourceNode == null && comparedFilter.destinationNode == c.destination && comparedFilter.isActive == true
                            && comparedFilter.filterType == FilterType.Show && prioritiesVisibilityFilters.IndexOf(comparedFilter) < priority)
                    );
                }

                if (!higherPriorityFilterActivatedForThisContact)
                {
                    if (filter.sourceNode == null && c.destination == filter.destinationNode)
                        c.visible = false;

                    else if (filter.destinationNode == null && c.source == filter.sourceNode)
                        c.visible = false;

                    else if (c.source == filter.sourceNode && c.destination == filter.destinationNode && filter.sourceNode != null && filter.destinationNode != null)
                        c.visible = false;
                }
            }
        }
    }

    //////////////////////////////////////
    /// Functions to re-apply all filters
    //////////////////////////////////////

    /// <summary>
    /// Re-apply all currently active highlight filters. Used when the priority list is modified
    /// by the user (activating/deactivating a filter or drag and drop action in the list).
    /// </summary>
    public void ReCalculateAllHighlightFilters()
    {
        foreach (ContactFilter filter in prioritiesHighlightFilters)
        {
            if (filter.isActive)
            {
                HighlightContacts(filter);
            }
        }
    }

    /// <summary>
    /// Re-apply all currently active visibility filters. Used when the priority list is modified
    /// by the user (activating/deactivating a filter or drag and drop action in the list).
    /// </summary>
    public void ReApplyAllVisibilityFilters()
    {
        foreach (ContactFilter filter in prioritiesVisibilityFilters)
        {
            if (filter.isActive)
            {
                if (filter.filterType == FilterType.Show)
                    ShowContacts(filter);
                if (filter.filterType == FilterType.Hide)
                    HideContacts(filter);
            }
        }
    }


    //////////////////////
    /// Bundle functions
    //////////////////////

    void ToggleBundleVisibility(GameObject sourceNode, GameObject destinationNode, bool visibility)
    {
        foreach (GameObject bundle in ipnvController.bundleList)
        {
            BundleController bundleController = bundle.GetComponent<BundleController>();
            BundleController.BundleHop currentHop = bundleController.hops[bundleController.currentHopIndex];
            if (sourceNode == null && currentHop.ReceiverNode == destinationNode)
                bundleController.visible = visibility;

            else if (destinationNode == null && currentHop.SenderNode == sourceNode)
                bundleController.visible = visibility;

            else if (currentHop.SenderNode == sourceNode && currentHop.ReceiverNode == destinationNode && sourceNode != null && destinationNode != null)
                bundleController.visible = visibility;
        }
    }
}

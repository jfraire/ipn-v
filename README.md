# IPN-V

The **Interplanetary Network Visualiser (IPN-v)** is a tool built in Unity that allows users to visualise realistic space network topologies. It takes into account the inherent challenges of space communication, like time-varying latencies and link disruptions.  

![Mars-Earth Network](./documentation/mars-earth-network.png)

The user-friendly interface of IPN-v lets users travel around the solar system and visualise the state of the network throughout time, including:
- ongoing contacts between nodes, represented by a blue line,
- positions and orbits of planets and network nodes,
- bundles sent through the network.

A 3D scene is built by IPN-v using a scenario folder, which contains all the data needed (network description, nodes and planets positions, contact plan, bundle plan...) inside JSON files. Scenario examples are given in the _scenario_ folder. A parallel tool, the **Interplanetary Network Designer (IPN-d)**, is used to generate those scenarios. Users can visualise their own custom network topologies using IPN-d, simply by specifying the orbital parameters of the nodes to be included. IPN-d also supports exporting contact plans compatible with established space communication standards, including NASA's ION format.

## Online demo

A demo of the tool is available on https://ipnv.net. To access it, please reach out to alice.le-bihan@insa-lyon.fr or juan.fraire@inria.fr. 

## Navigating in a simulation

To navigate inside a 3D scene, you can **select a planet you want to visit** using the planet buttons at the top of the page. 

A **time bar** at the bottom of the page allows for time navigation in the simulation. 

You can change the **simulation speed** using the two arrow buttons at the bottom of the page or by pressing _shift+UpArrow_ (increase speed) and _shift+DownArrow_ (decrease speed). 

To **pause**, press the _Space_ key or the pause button. 

Users can create **custom filters to hide, show, or highlight specific contacts**. The filter menu can be accessed through the "Filters" button at the top left of the page, or by pressing _F_.  

## Opening the projects in Unity

To open the IPN-v project in Unity:
- Open **Unity Hub** and click on "Add project from disk"
- Choose the **"ipn-v"** folder downloaded from the repository and confirm
- You should open the project using **Unity 6 version 6000.0.36f1**
- Once it is opened, go to **Assets/Scenes** and double click on **SampleScene** 

The same process can be repeated to open IPN-d.

## IPN-D Usage

IPN-Designer (IPN-D) generates the scenario files, including the contact plan, using a two-body Keplerian propagator by starting at the initial orbits and then stepping through frame by frame, calculating the next position and roation of each satellite, planet, and lander. IPN-D uses a line-of-sight (LOS) visibility model between nodes to establish contact opportunities.

IPN-D uses a scenario CSV file as input, containing the initial orbits of each node. These files can be handmade or automatically generated using https://github.com/jason-gerard/orbit-generator.

IPN-D outputs four artifacts. A high level config file, a contact plan, and two folders containing the orbital trajectories of the nodes and planets. These make up a configuration scenario that can be used in IPN-V. After running IPN-D you can easily save the output to a new configuration scenario for IPN-V by using the `transfer_scenario.sh` script found in the `scenarios` folder. The script takes a single parameter which is the name of the configuration scenario, an example call is below, ensure the script is run from the `scenarios` folder so that the relative pathing works properly.

```bash
cd scenarios
./transfer_scenario.sh configuration_earth_constellation
```
Please note that IPN-d was built to test the visualisation capabilities of IPN-v, not to replace a state-of-the-art trajectory calculation tool. The trajectories generated might therefore not be exact, as we are still debugging some issues in IPN-d. 

## GitLab usage

As IPN-v and IPN-d are still under development, it's best to regularly pull from the repository and regenerate scenario files in order to get the latest updates and fixes. The repository is usually updated several times a week.

## Features under development

The following features are currently under development:
- Adding moons to any planet, and adding nodes to them.
- Advanced contact filtering.
- Extending GODOT (the European Space Agency's astrodynamics library) to export scenario files in the IPN-v format, as an alternative for IPN-d.

## Contact us

If you have any questions, please contact alice.le-bihan@insa-lyon.fr or juan.fraire@inria.fr.

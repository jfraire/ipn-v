import './login.html';
import './login.css';


Template.login.events
({
	'click #button_show_password' (e,tpl)
	{
		Session.set("showPassword", !Session.get("showPassword"));
	}, 
	
	'click #login-button'(e,tpl)
	{
		e.preventDefault();
		Meteor.loginWithPassword(tpl.$("#email_input").val(), tpl.$("#password_input").val(), function (er)
			{
				if (er) alert ("Connection failed.");
			});
		return false;
	},
});

Template.login.rendered = function ()
{
	const tpl = this;
	tpl.$("#email_input").focus();
}

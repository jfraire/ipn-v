import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
import { FlowRouterMeta, FlowRouterTitle } from 'meteor/ostrio:flow-router-meta';

import "./router.js";
import './index.html';
import "./index.css";
import "./v1/v1.js";
import "./_client_collections";
import "./login/login.js";

const ipn = {};
window.ipn = ipn;
var toSend = {};
window.toSend = toSend;

var movingTimeBar = false;
var simulationPaused = false;

Meteor.startup(function() 
{
  console.log("New :D");
  
  
  // Session.set("allowUpload", false);
  Session.set("simulationState", "loading");
  window.addEventListener('keyup', function(event) 
  {
    if (event.keyCode === 32 && !movingTimeBar) 
      {
        //console.log('Space key pressed!');
        simulationPaused = !simulationPaused;
        ipn.instance.SendMessage("Pilot", "PauseSimulation", simulationPaused.toString());
      }
    
    ////Allow DB upload  
    // if(event.keyCode==221)
    // {
    //   var newValue = !Session.get("allowUpload") 
    //   Session.set("allowUpload", newValue);
    //   alert(Session.get("allowUpload"));
    // }  
      
    else
      {
        //console.log(event.code);
        switch (event.code) 
        {
          case 'Digit1': case 'Numpad1':
            try {
              ipn.instance.SendMessage("Main Camera", "LookAtObject", currentSimulationConfig.Planets[0].Name); 
            } catch (error) {
              console.error(error);
            }
            break;
          case 'Digit2': case 'Numpad2':
            try {
              ipn.instance.SendMessage("Main Camera", "LookAtObject", currentSimulationConfig.Planets[1].Name); 
            } catch (error) {
              console.error(error);
            }
            break;
          case 'Digit3': case 'Numpad3':
            try {
              ipn.instance.SendMessage("Main Camera", "LookAtObject", currentSimulationConfig.Planets[2].Name); 
            } catch (error) {
              console.error(error);
            }
            break;
          case 'Digit4': case 'Numpad4':
            try {
              ipn.instance.SendMessage("Main Camera", "LookAtObject", currentSimulationConfig.Planets[3].Name); 
            } catch (error) {
              console.error(error);
            }
            break;
          case 'Digit5': case 'Numpad5':
            try {
              ipn.instance.SendMessage("Main Camera", "LookAtObject", currentSimulationConfig.Planets[4].Name); 
            } catch (error) {
              console.error(error);
            }
            break;
          case 'Digit6': case 'Numpad6':
            try {
              ipn.instance.SendMessage("Main Camera", "LookAtObject", currentSimulationConfig.Planets[5].Name); 
            } catch (error) {
              console.error(error);
            }
            break;
          case 'Digit7': case 'Numpad7':
            try {
              ipn.instance.SendMessage("Main Camera", "LookAtObject", currentSimulationConfig.Planets[6].Name); 
            } catch (error) {
              console.error(error);
            }
            break;
          case 'Digit8': case 'Numpad8':
            try {
              ipn.instance.SendMessage("Main Camera", "LookAtObject", currentSimulationConfig.Planets[7].Name); 
            } catch (error) {
              console.error(error);
            }
            break;
          case 'Digit9': case 'Numpad9':
            try {
              ipn.instance.SendMessage("Main Camera", "LookAtObject", currentSimulationConfig.Planets[8].Name); 
            } catch (error) {
              console.error(error);
            }
            break;
          default:
            return;
        }
      }
     
  });
  
});

Template.registerHelper ("is_localhost", function(){
  return window.location.hostname.includes("localhost");
});

Template.registerHelper ("session_get", function (pChaine)
  {
	return Session.get (pChaine);
  });
  
Template.registerHelper ("session_equals", function (pA, pB)
  {
  return Session.equals(pA, pB);
  });
  
  Template.registerHelper ("session_equals_two_options", function (pA, pB, pC)
  {
    if(Session.equals(pA,pB) || Session.equals(pA,pC))
    {
      return true
    }
    return false
  });
  
Template.home.created = function()
{
  Session.set("displayLoader", true);
  const tpl=this;
  tpl.subscribe("allConfigs", function onReady()
  {
    //console.table(Configs.find().fetch());
  });
  
  
}


Template.home.events
({
  "click #sendData" (e, tpl) 
  {
    //console.log(e.currentTarget);
  },	
  
  "click #increaseSpeedUp" (e,tpl)
  {
    ipn.instance.SendMessage("Pilot", "SpeedUpHandler", "true");
  },
  
  "click #decreaseSpeedUp" (e,tpl)
  {
    ipn.instance.SendMessage("Pilot", "SpeedUpHandler", "false");
  },
  
  async "click #exampleFileButton" (e, tpl)
  {
    const exampleConfig = await Configs.findOne();
    ipn.instance.SendMessage("Pilot", "GetJsonDataFromWeb", JSON.stringify(exampleConfig));
    // Session.set('displayLoader', false);
    // Session.set("displayV1", true);
    
    //console.log(exampleConfig);
  
    globalThis.currentSimulationConfig = exampleConfig.configData;
    
    //Add the planets to the drop down menu
    for (var i =0; i<exampleConfig.configData.Planets.length; i++) 
    {
      addElementToDropdown(exampleConfig.configData.Planets[i].Name, 'planet-dropdown');
    }   
    
    var timeInfo = "Start time: " + secondsToDate(currentSimulationConfig.Time.SimulationStartTime) + "\n End time: " + secondsToDate(currentSimulationConfig.Time.SimulationEndTime);
    addElementToDropdown(timeInfo, "time-dropdown");
    
    var simulationStartTime = exampleConfig.configData.Time.SimulationStartTime;
    var simulationEndTime = exampleConfig.configData.Time.SimulationEndTime;

    var simulationStep = exampleConfig.configData.Time.Step;
    
    document.getElementById("timeBar").min = simulationStartTime;
    document.getElementById("timeBar").max = simulationEndTime;
    document.getElementById("timeBar").step= 1;

//document.getElementById("timeBar").value
  },
  
  "click .planet-dropdown-item"(e)
  {
    //alert(e.target.innerText);
    //console.log("clicked");
    ipn.instance.SendMessage("Main Camera", "LookAtObject", e.target.innerText);
  },
  
  "mousedown #timeBar"(e)
  {
    // if(!Session.get("onGoingOriginShift"))
    // {
      movingTimeBar = true;
      ipn.instance.SendMessage("Pilot", "PauseSimulation", "true");
    //}
  },
  
    
  "input #timeBar"(e)
  {
    // if(!Session.get("onGoingOriginShift"))
    // {
      //console.log(e.target.value);
      var oui = parseFloat(e.target.value);
      ipn.instance.SendMessage("Pilot", "PickSpecificTime",oui);
    //}
  },
  
  "mouseup #timeBar"(e)
  {
    // if(!Session.get("onGoingOriginShift"))
    // {
      if(!simulationPaused) ipn.instance.SendMessage("Pilot", "PauseSimulation", "false");
      movingTimeBar = false;
    //}
  },
  
  
  'click #log_out_button'()
  {
    if(confirm("Do you want to log out?"))
    {
      Meteor.logout();
      location.reload();
    }
  },
  // 
  // "mouseup #timeBar"(e)
  // {
  //   console.log(e.target.value);
  //   var oui = parseFloat(e.target.value);
  //   ipn.instance.SendMessage("Pilot", "PickSpecificTime",oui);
  //   //ipn.instance.SendMessage("Pilot", "PauseSimulation");
  // },
  
  
  
  
  async "change #configFileSelector" (e,tpl)
    {
      
      const selectedFiles = e.currentTarget.files;

      const configFile = findByFileName(selectedFiles, "config.json");
      const contactPlan = findByFileName(selectedFiles, "contactPlan.json")

      try{
        toSend.configData = await readFileAsJson(configFile);
        toSend.contactData = await readFileAsJson(contactPlan);
      }
      catch(err)
      {
        alert("Please upload a directory matching the IPN-V format.")
      }
      
      globalThis.currentSimulationConfig = toSend.configData;
      var timeInfo = "Start time: " + secondsToDate(currentSimulationConfig.Time.SimulationStartTime) + "\n End time: " + secondsToDate(currentSimulationConfig.Time.SimulationEndTime);
      addElementToDropdown(timeInfo, "time-dropdown");
      
      
      
      toSend.planetPositions = []; 
      //toSend.globalTest = {};
      //toSend.globalTest.test2 = [];
      toSend.nodePositions = [];
      
      
      async function processData() {
          for (const planet of toSend.configData.Planets) {
              const planetFile = findByFileName(selectedFiles, planet.Name + ".json");
              const planetPositions = await readFileAsJson(planetFile);
              var newPlanetEntry = {Name: planet.Name};
              newPlanetEntry.Positions = planetPositions.Positions;
              toSend.planetPositions.push(newPlanetEntry);
                
              addElementToDropdown(planet.Name, 'planet-dropdown');
    
                
              for (const node of planet.Nodes) {
                  const nodeFile = findByFileName(selectedFiles, node.ID + ".json");
                  const nodePositions = await readFileAsJson(nodeFile);
                  var newNodeEntry = {ID: node.ID};
                  newNodeEntry.Positions = nodePositions.Positions;
                  toSend.nodePositions.push(newNodeEntry);
              }
          }
          
          //Allow DB upload
          // if(Session.get("allowUpload"))
          // {
          //   Meteor.callAsync("insertInDatabase", toSend);
          // }          
          
          ipn.instance.SendMessage("Pilot", "GetJsonDataFromWeb", JSON.stringify(toSend));
//           Session.set('simulationStart', false);
//           Session.set('displayLoader', false);
// 
//           Session.set("displayV1", true);

          var simulationStartTime = toSend.configData.Time.SimulationStartTime;
          var simulationEndTime = toSend.configData.Time.SimulationEndTime;
          
          var simulationStep = toSend.configData.Time.Step;
          
          document.getElementById("timeBar").min = simulationStartTime;
          document.getElementById("timeBar").max = simulationEndTime;
          document.getElementById("timeBar").step= 1;


      }
      
      processData();
      
  }
  
});


function secondsToDate(seconds) {
    // Create a Date object for January 1, 2000 at 0:00
    // var baseDate = new Date('2000-01-01T00:00:00Z');
    var baseDate = new Date('2000-01-01T11:58:55.816Z'); // J2000
    
    // Calculate milliseconds from seconds
    var milliseconds = seconds * 1000;
    
    // Add milliseconds to base date to get the final date
    var finalDate = new Date(baseDate.getTime() + milliseconds);
    
    return finalDate.toUTCString();
}

async function readFileAsJson (f)
{
  return new Promise ((resolve, reject) =>
    {
    let content = '';
    const reader = new FileReader();
    reader.onloadend = function (e)
      {
      content = e.target.result;
      resolve (JSON.parse(content));
      }
    reader.onerror = function(e)
      {
      reject(e);
      }
    reader.readAsText (f);
  });
}

function findByFileName(list,nameWanted)
{
  var answer = Array.from(list).filter(function(s){
    return s.name == nameWanted;
  });
  
  return answer[0];
}



function addElementToDropdown(elementName, selectedDropdown)
{
  // Find the dropdown-content element
  var dropdownContent = document.querySelector("#"+selectedDropdown);
  
  // Create a new anchor element
  var newElement = document.createElement('a');
  
  // Set inner text
  newElement.innerText = elementName;
  
  // Set id attribute
  newElement.setAttribute('id', elementName);
  newElement.setAttribute('class', selectedDropdown + "-item");

  // Append the new link to the dropdown content
  dropdownContent.appendChild(newElement); 
}

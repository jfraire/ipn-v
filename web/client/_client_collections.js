import {Mongo} from 'meteor/mongo';

Configs = new Mongo.Collection("configs")
ContactPlans = new Mongo.Collection("contactPlans")
Nodes = new Mongo.Collection("nodes")
Planets = new Mongo.Collection("planets")
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
import { FlowRouterMeta, FlowRouterTitle } from 'meteor/ostrio:flow-router-meta';

FlowRouter.route('/',
{
  name: 'home',
  action : function (pPars, pReq)
	  {
		Session.set ("display_home", true);
	  }
});
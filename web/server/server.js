import { Meteor } from 'meteor/meteor';
import "./accounts.js";
import "./_server_collections";

import { Configs } from "./_server_collections.js";

this.Configs = Configs;

Meteor.startup(() =>
  {
    // code to run on server at startup
    
  });


Meteor.publish ("allConfigs", async function (pOp)
{
  const tUserId = this.userId;
  if(!tUserId) return false;
  return Configs.find({});
});

Meteor.methods
  ({
    
    async insertInDatabase(params)
    {
      const tUserId = this.userId;
      if(!tUserId) return false;
      const tRes = await Configs.insertAsync(params);
      console.log(tRes);
    },
  });
  
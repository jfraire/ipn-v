import {Mongo} from 'meteor/mongo';

export const Configs = new Mongo.Collection("configs")
export const ContactPlans = new Mongo.Collection("contactPlans")
export const Nodes = new Mongo.Collection("nodes")
export const Planets = new Mongo.Collection("planets")

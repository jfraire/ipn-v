# Origin shift

For performance reasons, game engines like Unity rarely support float64 (15 to 17 accurate digits); all positions are typically defined using Vector3, which are based on float32 (6 to 9 accurate digits). Because double precision is not supported, rendering becomes inaccurate beyond a certain distance from the origin, which is a problem when trying to model the solar system at its real scale. A common strategy to build large worlds is to use a technique called origin shift. Instead of moving the camera toward a planet, keep the camera at the origin and move the planet toward it. In practice, anything visible by the camera will be close to the origin and, therefore, exempt from floating point errors. IPN-v leverages this technique to allow a smooth rendering of all objects and precise contacts, supporting a visually progressive transition from one origin to another. At the beginning of a simulation, the sun is centred at (0, 0, 0). When an origin shift happens, it will be moved away and replaced by whatever object needs to be centred. 

## How object positions are defined

At every moment in the simulation, objects have two types of position variables that can be accessed: **the double-precision position** (added by IPN-v) and **the single-precision position**  (built-in Unity). They are both updated at each frame, and the second one is derived from the first one. 

### The theoretical double-precision position

The theoretical double-precision position is represented as an array of 3 doubles (x, y, z). It is derived from the input files. The input files define all known positions of an object at specific times [*t<sub>1</sub>*, *t<sub>2</sub>*, *t<sub>3</sub>* ... *t<sub>n</sub>*], and IPN-v then interpolates between those known positions to get a continuous movement. The interpolated value is stored in a variable called `currentPosNonShifted`. This position is "theoretical" since, as its name indicates, it **does not take into account origin shift**, meaning that it is where the object would be if there was no origin shift at all in the simulation.

The double-precision position variable must be updated before the single-precision position during a frame, since the single-precision position depends on it.


### The effective single-precision position

The effective single-precision position controls where the object actually is in the scene, and changing its value effectively moves the object. It is a built-in variable of GameObjects, which can be accessed using `<object>.transform.position`. For objects to be affected by origin shift, this property must be set every frame using the following formula (given for axis x):   
*`<object>.transform.position.x = (float) (currentPosNonShifted[0] - originShiftValue[0])`*

Both `currentPosNonShifted` and `originShiftValue` (see below for how it is calculated) contain double-precision floating points while `<object>.transform.position` is a Vector3, which contains single-precision floating points. This means that their difference must be converted to a single-precision floating point to set the object's effective position. 


## Origin shift implementation in IPN-v

When centring an object in IPN-v, an array of 3 doubles, called `originShiftValue`, is assigned and then updated every frame. It contains the (x, y, z) absolute, non-shifted position of the centred object at the current frame. The value is obtained directly from the `currentPosNonShifted` variable published by the object. For example, if the user wants to center Mars, IPNvController will set, at every frame, `originShiftValue` to the value of Mars' `currentPosNonShifted`. 

In practice, this means that `<centredObject>.transform.position`, the single-precision position variable of the centred object, will always be null, effectively placing the object at (0, 0, 0) in the scene. Other objects will be shifted accordingly and uniformly. 

It is very important to use the precise non-shifted position for the origin shift value and not the position property of the GameObject. Indeed, the position property contains single-precision floating points, and using it as the new origin would cause jittery movements for all objects in the scene (since it is an approximated position, `originShiftValue` would be varying greatly from one frame to the other).

Of course, to give the impression that the user is moving toward its target object, the origin shift transition should not be abrupt. For this reason, when a user decides to centre another object, an origin shift transition begins, which effectively starts a timer going from 0 to the transition time (normally 3 seconds to reach the target object). During this transition period, the origin value is updated each frame by progressively performing a linear interpolation between the position of the last object centred and the position of the target object. 

### The importance of script order

During one frame, different tasks will execute in a specific order, which is crucial for a proper functioning of the origin shift system. In Edit > Project Settings > Execution Order, it is possible to change the order in which the scripts are going to execute in a frame. If we have 3 scripts A, B and C placed in that order, they will run in this same order in all lifecycle methods (like Update or LateUpdate). In IPN-v, the order is the following: 

- IPNvController
- PlanetController
- StarController
- NodeController
- LinkController
- BundleController
- CameraController
- UIController

This script order was dictated by the interdependencies between objects positions: a node position is derived from its center planet position, a link or bundle position is derived from its start and end node positions. Planet positions must therefore be calculated in a frame before node positions, and node positions before bundle and link positions. IPNvController runs first since it handles important functions like updating simulation time, which must be the first action in a frame. 

The order in which origin shift functions (and other useful functions) run in one frame in the lifecycle methods is detailed below.

#### 1) Update() 

**IPNvController**
	
- HandleSimulationTime() :  update the current simulation time (more details in time.md)
		
- HandleOriginShiftTransitionTime() : if there is an ongoing origin shift transition, update the timer used to handle it (more details in time.md)
	
**PlanetController -> StarController -> NodeController -> BundleController**
	
- UpdateCurrentPositionNonShifted() : update the value of the object's currentPosNonShifted variable. This does NOT move the planet in the scene, the moving action is performed in LateUpdate() AFTER calculating the origin shift value. But it is important that objects calculate their non-shifted position in the current frame before the origin shift value is updated, since the origin shift is directly derived from the centred object's non shifted position.


#### 2) LateUpdate()

**IPNvController**

- HandleOriginShiftValueUpdate() : update the value of the origin shift variable using the currently centred object's non-shifted position. If there is no ongoing origin shift transition, it is taken directly. Otherwise, a linear interpolation is performed depending on how advanced we are in the transition.

**PlanetController -> StarController -> NodeController -> BundleController**

- MoveObject() : calculate the object's shifted position (based on the aforementioned formula) and effectively move the object in the scene. It is therefore important that this action runs after calculating the object's non-shifted position and the origin shift value.


NB: in the case of planets, an angle is also calculated in Update() and applied in LateUpdate().


## How to add an object that will be affected by origin shift in IPN-v

To add an object in IPN-v and have it be affected by origin shift, the following steps should be followed:

- Add a script to the new GameObject.
- Define an **UpdateCurrentPosNonShifted()** function and call it from the **Update()** lifecycle method. This function should give, at every frame, a new value to the public variable `currentPosNonShifted` (array of 3 doubles (x, y, z)). This value should be precise, which means that it should either be taken from the input files (like planet positions), or deduced from other objects' precise position (like bundles positions).
- Define a **MoveObject()** function and call it from the **LateUpdate()** lifecycle method. This function should perform the action of actually moving the object in the scene by setting its `<object>.transform.position` variable using the formula above, taking into account origin shift. 
- Set the script's running order. If the object's position does not depend on any other object in the scene, the script should simply be placed anywhere after IPNvController. Otherwise, place it after the objects' scripts it depends on. 
- Add a case to the switch in the getObjectNonShiftedPosition() function of IPNvController.
- Then, you can call TriggerOriginShift(GameObject `objectToCenter`).


*Document written by Alice Le Bihan, last updated 04/02/2025*
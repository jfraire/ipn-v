# Position and rotation

# Planet and node position

In IPN-v, the reference frames used for expressing positions depends on the object:
- planets positions are defined in heliocentric coordinates,
- nodes positions are defined relatively to the center of their parent planet.

# Planet rotation

Planet rotations outputed by IPN-d are expressed in degrees relatively to the global axes of Unity. 
using UnityEngine;

[CreateAssetMenu(fileName = "Progress", menuName = "Scriptable Objects/Progress")]
public class Progress : ScriptableObject
{
    public int percentage;
}

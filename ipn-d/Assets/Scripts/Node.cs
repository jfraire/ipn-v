using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Node
{

    //Local, tilted positions (used in the JSON files)
    public double localX;
    public double localY;
    public double localZ;

    //Global, tilted positions
    public double PosX;
    public double PosY;
    public double PosZ;

    //General node parameters
    public int ID;
    public string Type;
    public string Name;
    public Planet ParentPlanet;
    public Dictionary<Node, Link> OnGoingLinks = new();
    public List<string> Rules = new();

    //Orbiter parameters
    public float SemiMajorAxis; // in km
    public float Eccentricity;
    public float Inclination;
    public float ArgumentOfPeriapsis;
    public float AscendingNodeLongitude;
    public float MeanAnomalyAtEpoch;
    public float OrbitPeriod;
    
    //Lander parameters (geocentric coordinates)
    public float relativeX;
    public float relativeY;
    public float relativeZ;

    //Test
    public List<Vector3> posTest = new();
    public List<string> positionList = new();
    public double M_mars = 6.39e23;
    public double G = 6.6743e-20; // Gravitational constant (km^3/kg/s^2)

    //JSON
    public IPNcController.NodePositionData JSONpositions = new();

    public Node(int id, string type, string name, Planet parentPlanet)
    {
        ID = id;
        Type = type;
        Name = name;
        ParentPlanet = parentPlanet;
    }

    public void UpdateOrbiterPosition(double time)
    {
        //Conversions to radians
        double m_0_rad = MeanAnomalyAtEpoch * Math.PI / 180;
        double i_rad = Inclination * Math.PI / 180;
        double long_rad = AscendingNodeLongitude * Math.PI / 180;
        double argp_rad = ArgumentOfPeriapsis * Math.PI / 180;

        //Calculate the Standard Gravitational Parameter (μ)
        double mu = G * ParentPlanet.Mass;

        //Calculate the Mean Motion (n) 
        double n = Math.Sqrt(mu / Math.Pow(SemiMajorAxis, 3));

        // Calculate the Mean Anomaly (M) from the Mean Anomaly at Epoch (M_0) and the mean motion
        double M = n * time + m_0_rad;

        //Calculate the Eccentric Anomaly (E) using Newton-Raphson iteration
        //Initial guess
        double E = M;
        while (true)
        {
            double E_new = E - (E - Eccentricity * Math.Sin(E) - M) / (1 - Eccentricity * Math.Cos(E));
            if (Math.Abs(E_new - E) < 1e-8)
            { // Convergence criteria
                E = E_new;
                break;
            }
            E = E_new;
        }

        // Calculate the distance from the body (r)
        double r = SemiMajorAxis * (1 - Eccentricity * Math.Cos(E));

        //Calculate the True Anomaly (ν)
        double nu = 2 * Math.Atan(Math.Sqrt((1 + Eccentricity) / (1 - Eccentricity)) * Math.Tan(E / 2));

        //Calculate the node's planetocentric coordinates in its orbital plane (z'=0)
        double x_prime = r * Math.Cos(nu);
        double y_prime = r * Math.Sin(nu);

        //Calculate the node's planetocentric coordinates 
        double newX = x_prime * (Math.Cos(long_rad) * Math.Cos(argp_rad) - Math.Sin(long_rad) * Math.Sin(argp_rad) * Math.Cos(i_rad)) - y_prime * (Math.Sin(argp_rad) * Math.Cos(long_rad) + Math.Cos(argp_rad) * Math.Sin(long_rad) * Math.Cos(i_rad));
        double newY = x_prime * (Math.Cos(argp_rad) * Math.Sin(long_rad) + Math.Sin(argp_rad) * Math.Cos(long_rad) * Math.Cos(i_rad)) + y_prime * (Math.Cos(long_rad) * Math.Cos(argp_rad) * Math.Cos(i_rad) - Math.Sin(long_rad) * Math.Sin(argp_rad));
        double newZ = x_prime * (Math.Sin(argp_rad) * Math.Sin(i_rad)) + y_prime * (Math.Cos(argp_rad) * Math.Sin(i_rad));

        // (x,y,z) position if the parent planet had an Obliquity of 0
        double[] nonTiltedPosition = new double[] { newX, newZ, newY}; // Unity inverses Y and Z axes compared to norms

        // (x,y,z) position with a titl corresponding to the planet's obliquity
        double[] tiltedPosition = ApplyObliquityRotation(nonTiltedPosition, ParentPlanet.Obliquity);

        // Set the local coordinates variables
        localX = tiltedPosition[0];
        localY = tiltedPosition[1]; 
        localZ = tiltedPosition[2];

        //Set the global coordinates variables
        PosX = localX + ParentPlanet.PosX;
        PosY = localY + ParentPlanet.PosY;
        PosZ = localZ + ParentPlanet.PosZ;
    }

    /// <summary>
    /// Rotates a 3D point around the Z-axis by a given angle in degrees.
    /// Used to align a node's trajectory with its planet's tilted reference frame.
    /// This has the same effect as multiplying a Vector3 by a Quaternion,
    /// but here it works with double precision.
    /// </summary>
    /// <returns>A new array {x', y', z'} with the rotated coordinates.</returns>
    double[] ApplyObliquityRotation(double[] position, double angleDegrees)
    {
        double angleRad = angleDegrees * Math.PI / 180.0;
        double cosTheta = Math.Cos(angleRad);
        double sinTheta = Math.Sin(angleRad);

        double x = position[0] * cosTheta - position[1] * sinTheta;
        double y = position[0] * sinTheta + position[1] * cosTheta;
        double z = position[2]; // Z remains unchanged

        return new double[] { x, y, z };
    }

    public void UpdateLanderPosition()
    {
        Vector3 relativePos = new((float)relativeX, (float)relativeY, (float)relativeZ);

        Quaternion rotationQuaternion = Quaternion.Euler((float)ParentPlanet.RotationX, (float)ParentPlanet.RotationY, (float)ParentPlanet.RotationZ);

        // Rotate the initial position using the quaternion
        Vector3 relativeRotatedPos = rotationQuaternion * relativePos;

        localX = relativeRotatedPos.x;
        localY = relativeRotatedPos.y;
        localZ = relativeRotatedPos.z;

        PosX = ParentPlanet.PosX + relativeRotatedPos.x;
        PosY = ParentPlanet.PosY + relativeRotatedPos.y;
        PosZ = ParentPlanet.PosZ + relativeRotatedPos.z;
    }
}

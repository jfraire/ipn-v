using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEditor;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

public class IPNcController : MonoBehaviour
{
    // Performance measurement;
    Stopwatch stopwatch = new Stopwatch();

    //Scriptable objects
    public Progress progress;

    ////////////////////////////////////////////
    //// Input file containing node information
    ////////////////////////////////////////////
    //private String NodeFilePath = "Assets/Data/scenario-full.csv";
    //private String NodeFilePath = "Assets/Data/scenario-1-orbiter-per-planet.csv";
    //private String NodeFilePath = "Assets/Data/scenario-3-nodes-earth-mars-neptune.csv";
    //private String NodeFilePath = "Assets/Data/scenario-1-node-per-planet.csv";
    //private String NodeFilePath = "Assets/Data/scenario-orbiters.csv";
    private String NodeFilePath = "Assets/Data/earth-constellation.csv";
    //private String NodeFilePath = "Assets/Data/small-earth-network.csv";

    //////////////////////////////////////////////
    //// Input file containing planet information
    //////////////////////////////////////////////
    private String PlanetFilePath = "Assets/Data/planets.csv"; // planets input file
    private String MoonsFilePath = "Assets/Data/moons.csv"; // planets input file

    ///////////////////////
    /// JSON export classes
    ///////////////////////

    [Serializable]
    public class ContactPlanData
    {
        public List<ContactData> ContactPlan = new();
    }

    [Serializable]
    public class ContactData
    {
        public int SourceID;
        public int DestinationID;
        public double StartTime;
        public double EndTime;
        public double Duration;
        public float[] Color;
    }

    [Serializable]
    public class PlanetPositionData
    {
        public List<PlanetPositionSpecificTimeData> Positions = new();
    }

    [Serializable]
    public class PlanetPositionSpecificTimeData
    {
        public double Time;
        public double PositionX;
        public double PositionY;
        public double PositionZ;
        public double RotationX;
        public double RotationY;
        public double RotationZ;

        public PlanetPositionSpecificTimeData(double time, double posX, double posY, double posZ, double rotationX, double rotationY, double rotationZ)
        {
            Time = time;
            PositionX = posX;
            PositionY = posY;
            PositionZ = posZ;
            RotationX = rotationX;
            RotationY = rotationY;
            RotationZ = rotationZ;
        }
    }

    [Serializable]
    public class MoonPositionData
    {
        public List<MoonPositionSpecificTimeData> Positions = new();
    }

    [Serializable]
    public class MoonPositionSpecificTimeData
    {
        public double Time;
        public double PositionX;
        public double PositionY;
        public double PositionZ;
        public double RotationX;
        public double RotationY;
        public double RotationZ;

        public MoonPositionSpecificTimeData(double time, double posX, double posY, double posZ, double rotationX, double rotationY, double rotationZ)
        {
            Time = time;
            PositionX = posX;
            PositionY = posY;
            PositionZ = posZ;
            RotationX = rotationX;
            RotationY = rotationY;
            RotationZ = rotationZ;
        }
    }

    [Serializable]
    public class NodePositionData
    {
        public List<NodePositionSpecificTimeData> Positions = new();
    }

    [Serializable]
    public class NodePositionSpecificTimeData
    {
        public double Time;
        public double PositionX;
        public double PositionY;
        public double PositionZ;

        public NodePositionSpecificTimeData(double time, double posX, double posY, double posZ)
        {
            Time = time;
            PositionX = posX;
            PositionY = posY;
            PositionZ = posZ;
        }
    }

    [Serializable]
    public class ConfigData
    {
        public TimeConfigData Time;
        public StarConfigData Star;
        public List<PlanetConfigData> Planets = new();
    }

    [Serializable]
    public class TimeConfigData
    {
        public double SimulationStartTime;
        public double SimulationEndTime;
        public double Step;

        public TimeConfigData(double startTime, double endTime, double step)
        {
            SimulationStartTime = startTime;
            SimulationEndTime = endTime;
            Step = step;
        }
    }

    [Serializable]
    public class StarConfigData
    {
        public string Name;
        public double Radius;

        public StarConfigData(string name, double radius)
        {
            Name = name;
            Radius = radius;
        }
    }

    [Serializable]
    public class PlanetConfigData
    {
        public string Name;
        public double Radius;
        public double OrbitPeriod;
        public double SemiMajorAxis;   // Add new properties
        public double Eccentricity;    // Add new properties
        public double Inclination;     // Add new properties
        public double Raan;            // Add new properties
        public double ArgOfPeriapsis;  // Add new properties
        public double MeanAnomaly;     // Add new properties
        public double Obliquity;       // Add new properties
        public double RotationPeriod;  // Add new properties
        public List<NodeConfigData> Nodes = new();
        public List<MoonConfigData> Moons = new();
    }

    [Serializable]
    public class NodeConfigData
    {
        public int ID;
        public string Name;
        public double OrbitPeriod;
    }

    [Serializable]
    public class MoonConfigData
    {
        public string Name;
        public double Radius;
        public List<NodeConfigData> Nodes = new();
    }

    // BundlePlan
    [Serializable]
    public class BundlePlanData
    {
        public List<BundleData> Bundles = new();
    }

    [Serializable]
    public class BundleData
    {
        public string Name;
        public List<HopData> Hops = new();
    }

    [Serializable]
    public class HopData
    {
        public int SenderNode;
        public int ReceiverNode;
        public double SendTime;

        public HopData(int senderNode, int receiverNode, double sendTime)
        {
            SenderNode = senderNode;
            ReceiverNode = receiverNode;
            SendTime = sendTime;
        }
    }

    ////////////////////////////////////
    ////////////////////////////////////

    //JSON export
    ContactPlanData contactPlanData = new();
    ConfigData configData = new();

    //Constants
    public double G = 6.6743e-20; // Gravitational constant (km^3/kg/s^2)
    public double M_sun = 1.9885e30; // Mass of the Sun (kg)
    public double M_mars = 6.39e23;

    // Sun Radius (km)
    public Dictionary<string, double> sunRadii = new()
    {
        { "0-Sun", 696340.0 },
    };

    //Time variables
    public DateTime startDate = new DateTime(2023, 1, 1); // start date of the simulation
    public DateTime endDate = new DateTime(2023, 1, 2); // end date of the simulation

    private int timeStep = 100; // Step between two timestamps: time in second between each position calculation.
                                // A higher value will lead to a higher amount of precise positions (IPN-v will perform
                                // linear interpolations between two known points) but higher computation time for IPN-d.
                                // 100 is a good value for simulations that last a couple of days. 

    public DateTime timeReference = new DateTime(2000, 1, 1, 11, 58, 55, 816); //J200 UTC
    public double simulationStartTime; // start time in secondq, calculated when starting the code using startDate
    public double simulationEndTime; // end time in seconds, calculated when starting the code using endDate
    public double currentTime;
    //Lists
    Dictionary<String, Planet> planets = new();
    Dictionary<String, Moon> moons = new();
    Dictionary<int, Node> nodes = new();
    List<Contact> contacts = new();

    public String log = "";

    //Contact plan in ION format
    public string ionContactCommands = "";
    public string ionRangeCommands = "";

    //Scale tests
    public float kmsPerGrid = (float)149e6 / 100; // 149e6 = AU

    //App state
    bool isStopping = false;

    // Start is called before the first frame update
    // It controls the whole simulation and contains all of the events
    void Start()
    {
        progress.percentage = 0;
        stopwatch.Start();

        Debug.Log("IPN-d is computing...");

        simulationStartTime = (startDate - timeReference).TotalSeconds;
        simulationEndTime = (endDate - timeReference).TotalSeconds;
        double simulationDuration = simulationEndTime - simulationStartTime;
        currentTime = simulationStartTime;

        //////////////////////////////////
        ///Planets and nodes instantiation
        //////////////////////////////////
        planets = InstantiatePlanets();
        moons = InstantiateMoons();
        nodes = InstantiateNodes();

        InitialiseContacts();
    }


    private void FixedUpdate()
    {
        if (currentTime < simulationEndTime + timeStep && !isStopping)
        {
            CalculateAll(currentTime);

            currentTime += timeStep;

            progress.percentage = (int) (100 * (currentTime - simulationStartTime) / (simulationEndTime - simulationStartTime));

            //Debug.Log(progress.percentage);
        }
        if(currentTime == simulationEndTime + timeStep && !isStopping)
        {
            isStopping = true;
            ExportAll();
            stopwatch.Stop();
            progress.percentage = 0;
            UnityEngine.Debug.Log("IPN-d completed its task in " + stopwatch.ElapsedMilliseconds + " ms");
            EditorApplication.isPlaying = false;
        }
    }

    // When stopping the Editor or quiting a Build
    private void OnApplicationQuit()
    {
        progress.percentage = 0;
    }

    /// <summary>
    /// Runs once per Fixed Update. Calculates all planets and nodes positions, as well as contacts
    /// at the current simulation time.
    /// </summary>
    void CalculateAll(double currentTime)
    {
        // Update each planet position and rotation and save it in JSON format
        foreach (var kvp in planets)
        {
            Planet planet = kvp.Value;
            planet.UpdatePosition(currentTime);
            planet.UpdateRotation(currentTime);

            PlanetPositionSpecificTimeData currentData = new(currentTime, planet.PosX, planet.PosY, planet.PosZ, planet.RotationX, planet.RotationY, planet.RotationZ);
            planet.JSONpositions.Positions.Add(currentData);

            String positionReport = $"{currentTime}, {planet.PosX} {planet.PosY} {planet.PosZ}, {planet.RotationX} {planet.RotationY} {planet.RotationZ}";
            planet.positionList.Add(positionReport);

            log += ($"Name: {planet.Name}, Time : {simulationStartTime} position : ({planet.PosX}, {planet.PosY}, {planet.PosZ}), rotation : ({planet.RotationX}, {planet.RotationY}, {planet.RotationZ})\n");
        }

        foreach (var kvp in moons)
        {
            Moon moon = kvp.Value;
            moon.UpdatePosition(currentTime);
            moon.UpdateRotation(currentTime);

            MoonPositionSpecificTimeData currentData = new(currentTime, moon.localX, moon.localY, moon.localZ, moon.RotationX, moon.RotationY, moon.RotationZ);
            moon.JSONpositions.Positions.Add(currentData);

            //Debug.Log(moon.Name + " position :" + moon.localX + ", " + moon.localY + ", " + moon.localZ + " rotation : " + moon.RotationX + ", " + moon.RotationY + ", " + moon.RotationZ);
        }

        // Update each node position and save it in JSON format
        foreach (var kvp in nodes)
        {
            Node node = kvp.Value;
            double scaleTest = 500 / node.ParentPlanet.Radii;
            //scaleTest = 1;
            if (node.Type == "Orbiter")
            {
                node.UpdateOrbiterPosition(currentTime);

                Vector3 positions = new((float)node.PosX, (float)node.PosY, (float)node.PosZ);
                node.posTest.Add(positions);
                string positionReport = $"{currentTime}, {node.PosX} {node.PosY} {node.PosZ}";
                node.positionList.Add(positionReport);
            }
            if (node.Type == "Lander")
            {
                node.UpdateLanderPosition();
                string positionReport = $"{currentTime}, {node.PosX} {node.PosY} {node.PosZ}";
                node.positionList.Add(positionReport);
            }

            NodePositionSpecificTimeData currentData = new(currentTime, node.localX, node.localY, node.localZ);
            node.JSONpositions.Positions.Add(currentData);
        }

        // Update each contact status (started, ended, in progress)
        if (currentTime >= simulationEndTime)
            // Last iteration = True
            UpdateContacts(currentTime, true);
        else
            UpdateContacts(currentTime, false);

        log += "\n";
    }

    /// <summary>
    /// Runs once when the calculations are over. Exports all the data in JSON.
    /// </summary>
    void ExportAll()
    {
        //Config
        InstantiateConfigFile();

        /////////////
        /// Contacts
        /////////////

        using var ionContactPlanWriter = new StreamWriter("Assets/Data/contactPlan.txt", false);
        ionContactPlanWriter.WriteLine(ionContactCommands);
        ionContactPlanWriter.WriteLine(ionRangeCommands);

        //JSON export
        string cpToJson = JsonUtility.ToJson(contactPlanData, true);
        File.WriteAllText("Assets/Data/JSON/contactPlan.json", cpToJson);

        /////////////////////////////
        /// Planets, moons and nodes
        /////////////////////////////

        //Export planet to JSON
        foreach (var kvp in planets)
        {
            Planet p = kvp.Value;
            string planetToJson = JsonUtility.ToJson(p.JSONpositions, true);
            string currentPlanetPath = "Assets/Data/JSON/planets/" + p.Name + ".json";
            File.WriteAllText(currentPlanetPath, planetToJson);
        }

        //Export moon to JSON
        foreach (var kvp in moons)
        {
            Moon m = kvp.Value;
            string moonToJson = JsonUtility.ToJson(m.JSONpositions, true);
            string currentMoonPath = "Assets/Data/JSON/moons/" + m.Name + ".json";
            File.WriteAllText(currentMoonPath, moonToJson);
        }

        foreach (var kvp in nodes)
        {
            Node n = kvp.Value;
            string nodeToJson = JsonUtility.ToJson(n.JSONpositions, true);
            string currentPlanetPath = "Assets/Data/JSON/nodes/" + n.ID + ".json";
            File.WriteAllText(currentPlanetPath, nodeToJson);
        }

        ////////////////////////
        /// Bundle Plan
        ////////////////////////

        // Define the number of bundles to generate per node
        int N = 0;
        BundlePlanData bundlePlanData = new();

        System.Random rand = new System.Random();

        // Iterate over each contact in the contact plan and generate bundles
        foreach (var nodePair in nodes) // nodes -> Dictionary<int, Node>
        {
            Node sourceNode = nodePair.Value;

            for (int i = 0; i < N; i++)
            {
                BundleData bundleData = new();
                bundleData.Name = $"Bundle_{sourceNode.ID}_{i + 1}";

                Node currentSender = sourceNode;
                double currentTime = simulationStartTime;

                while (true)
                {
                    // Find the next contact involving the currentSender node
                    Contact nextContact = null;

                    // If sender is Mars, try forcing Earth
                    if (currentSender.ID == 401)
                    {
                        foreach (var contact in contacts)
                        {
                            if (contact.Source.ID == currentSender.ID && contact.Destination.ID == 301 && contact.StartTime >= currentTime)
                            {
                                nextContact = contact;
                                break;
                            }
                        }
                    }

                    // Else, try any possible pair
                    if (nextContact == null)
                    {
                        foreach (var contact in contacts)
                        {
                            if (contact.Source.ID == currentSender.ID && contact.StartTime >= currentTime)
                            {
                                nextContact = contact;
                                break;
                            }
                        }
                    }

                    if (nextContact == null) break;

                    double waitAfterContactStart = 3;
                    double sendTime = Math.Max(nextContact.StartTime + waitAfterContactStart, currentTime);

                    // Calculate the send time based on the distance and speed of light
                    Planet srcPlanet = nextContact.Source.ParentPlanet;
                    Planet dstPlanet = nextContact.Destination.ParentPlanet;

                    // Update source position at start time
                    srcPlanet.UpdatePosition(sendTime);
                    srcPlanet.UpdateRotation(sendTime);
                    if (nextContact.Source.Type == "Orbiter")
                        nextContact.Source.UpdateOrbiterPosition(sendTime);
                    if (nextContact.Source.Type == "Lander")
                        nextContact.Source.UpdateLanderPosition();

                    // Update destination position at start time
                    dstPlanet.UpdatePosition(sendTime);
                    dstPlanet.UpdateRotation(sendTime);
                    if (nextContact.Destination.Type == "Orbiter")
                        nextContact.Destination.UpdateOrbiterPosition(sendTime);
                    if (nextContact.Destination.Type == "Lander")
                        nextContact.Destination.UpdateLanderPosition();

                    // Compute current position and distance
                    float speedOfLight = 299792; // Speed of light in kms per second
                    Vector3 srcPosition = new((float)nextContact.Source.PosX, (float)nextContact.Source.PosY, (float)nextContact.Source.PosZ);
                    Vector3 dstPosition = new((float)nextContact.Destination.PosX, (float)nextContact.Destination.PosY, (float)nextContact.Destination.PosZ);
                    float distance = Vector3.Distance(srcPosition, dstPosition);
                    float owlt = distance / speedOfLight;

                    // Compute data arrival time
                    double arrivalTime = currentTime + owlt;

                    // Add the hop to the bundle
                    HopData hopData = new HopData(nextContact.Source.ID, nextContact.Destination.ID, sendTime);
                    bundleData.Hops.Add(hopData);

                    // Update the sender for the next hop and current time
                    currentSender = nextContact.Destination;
                    currentTime = arrivalTime;

                    // Randomly decide to stop the bundle progression
                    if (rand.NextDouble() < 0.3) break; // 30% chance to stop after this hop
                }

                bundlePlanData.Bundles.Add(bundleData);
            }
        }

        // Save the bundle plan to a JSON file
        string bundlePlanToJson = JsonUtility.ToJson(bundlePlanData, true);
        File.WriteAllText("Assets/Data/JSON/bundlePlan.json", bundlePlanToJson);
    }

    // Called after the for each node loop updating the positions
    private void UpdateContacts(double time, bool isLastIteration)
    {
        // For each source node
        foreach (var source in nodes)
        {
            Node s = source.Value;

            // For each destination node
            foreach(var destination in nodes)
            {
                Node d = destination.Value;

                // Ignore source to source contact
                if (source.Value == destination.Value)
                    continue;
                else
                {
                    bool senderShouldTransmit = SenderShouldTransmit(source.Value, destination.Value, time, timeStep, planets, nodes);

		            bool isAllowedToCommunicate = (!s.Rules.Contains("localPlanetOnly") && !d.Rules.Contains("localPlanetOnly")) || s.ParentPlanet.Equals(d.ParentPlanet);

                    // If we have LoS and node's rules allow for contact with dst node
                    if(senderShouldTransmit && isAllowedToCommunicate)
                    {
                        // If there is no current link with the destination, create and start one
                        if (s.OnGoingLinks[d] == null)
                        {
                            s.OnGoingLinks[d] = new Link(time);
                        }
                        // If there is a link and it's the last iteration, terminate it
                        else if (s.OnGoingLinks[d] != null && isLastIteration)
                        {
                            // Debug.Log(relTime + "End of link between " + s.Name + " and " + d.Name);
                            double contactStartTime = s.OnGoingLinks[d].StartTime;
                            double contactEndTime = time;
                            double contactDuration = contactEndTime - contactStartTime;
                            Contact newContact = new(s, d, contactStartTime, contactEndTime, contactDuration);
                            contacts.Add(newContact);

			                ionContactCommands += BuildContactCommand(newContact);
                            ionRangeCommands += BuildRangeCommand(newContact);

                            // Json export
                            ContactData contactData = new();
                            contactData.SourceID = s.ID;
                            contactData.DestinationID = d.ID;
                            contactData.StartTime = s.OnGoingLinks[d].StartTime;
                            contactData.EndTime = time;
                            contactData.Duration = contactEndTime - contactStartTime;

                            // Add color for specific node pairs (test)
                            if ((s.ID == 403 && d.ID == 452) || (s.ID == 452 && d.ID == 403))
                            {
                                float[] linkColor = { 255, 153, 0 };
                                contactData.Color = linkColor;
                            }
                            else
                                contactData.Color = null;

                            contactPlanData.ContactPlan.Add(contactData);

                            s.OnGoingLinks[d] = null;
                        }
                        // If there's already a link with the destination and it's not the last iteration, ignore
                        else
                            continue;
                    }

                    // If we don't have LoS
                    if(!senderShouldTransmit)
                    {
                        // If there is no current link with the destination, ignore
                        if (s.OnGoingLinks[d] == null)
                            continue;
                        // If there is a link with the destination, end it
                        else
                        {
                            //Debug.Log(relTime + "End of link between " + s.Name + " and " + d.Name);
                            double contactStartTime = s.OnGoingLinks[d].StartTime;
                            double contactEndTime = time;
                            double contactDuration = contactEndTime - contactStartTime;
                            Contact newContact = new(s, d, contactStartTime, contactEndTime, contactDuration);
                            contacts.Add(newContact);

                            ionContactCommands += BuildContactCommand(newContact);
                            ionRangeCommands += BuildRangeCommand(newContact);

                            // Json export
                            ContactData contactData = new();
                            contactData.SourceID = s.ID;
                            contactData.DestinationID = d.ID;
                            contactData.StartTime = s.OnGoingLinks[d].StartTime;
                            contactData.EndTime = time;
                            contactData.Duration = contactEndTime - contactStartTime;

                            // Add color for specific node pairs (test)
                            if ((s.ID == 403 && d.ID == 452) || (s.ID == 452 && d.ID == 403))
                            {
                                float[] linkColor = { 255, 153, 0 };
                                contactData.Color = linkColor;
                            }
                            else
                                contactData.Color = null;

                            contactPlanData.ContactPlan.Add(contactData);

                            s.OnGoingLinks[d] = null;
                        }
                    }
                }
            }
        }
    }

    private bool SenderShouldTransmit(Node source, Node destination, double currentTime, int timeStep, Dictionary<String, Planet> planets, Dictionary<int, Node> nodes)
    {
        // Version 1) Just LoS at the current position
        // bool inLineOfSightNow = UtilityLibrary.ComputeLOS(source, destination, planets);

        // Version 2) LoS at the apparent position of the destination        
        float speedOfLight = 299792; // Speed of light in kms per second
        Planet dstPlanet = destination.ParentPlanet;

        // Compute current position and distance
        Vector3 srcPosition = new((float)source.PosX, (float)source.PosY, (float)source.PosZ);
        Vector3 dstPosition = new((float)destination.PosX, (float)destination.PosY, (float)destination.PosZ);
        float distance = Vector3.Distance(srcPosition, dstPosition);
        float owlt = distance / speedOfLight;

        // if(source.ID == 301 && destination.ID == 801) // Debug Earth to Neptune
        //     Debug.Log($"{source.ID} ({source.ParentPlanet.Name}) - {destination.ID} ({destination.ParentPlanet.Name}): distance: {distance} - owlt: {owlt} sec");

        // Compute data arrival time
        double arrivalTime = currentTime + owlt;

        // Update target planet and node position
        dstPlanet.UpdatePosition(arrivalTime);
        dstPlanet.UpdateRotation(arrivalTime);
        if (destination.Type == "Orbiter")
            destination.UpdateOrbiterPosition(arrivalTime);
        if (destination.Type == "Lander")
            destination.UpdateLanderPosition();

        Vector3 newDstPosition = new((float)destination.PosX, (float)destination.PosY, (float)destination.PosZ);
        float newDistance = Vector3.Distance(srcPosition, newDstPosition);
        float newOwlt = newDistance / speedOfLight;

        // if(source.ID == 301 && destination.ID == 801) // Debug Earth to Neptune
        //     Debug.Log($"newDistance: {newDistance} - newOwlt: {newOwlt} sec - Delta: {newOwlt - owlt}");
        
        //if(Math.Abs(newOwlt - owlt) > 1)
        //    // If this is the case, we would need to iterate with e.g., Newton method, not yet implemented
        //    Debug.Log($"OWLT Delta: {newOwlt - owlt}s is larger than 1s between {source.ID} ({source.ParentPlanet.Name}) - {destination.ID} ({destination.ParentPlanet.Name})");

        bool inLineOfSightNow = UtilityLibrary.ComputeLOS(source, destination, planets);

        // Restore original target planet and node positions
        dstPlanet.UpdatePosition(currentTime);
        dstPlanet.UpdateRotation(currentTime);
        if (destination.Type == "Orbiter")
            destination.UpdateOrbiterPosition(currentTime);
        if (destination.Type == "Lander")
            destination.UpdateLanderPosition();

        return inLineOfSightNow;
    }

    private void InitialiseContacts()
    {
        foreach (var source in nodes)
        {
            Node s = source.Value;
            foreach (var destination in nodes)
            {
                Node d = destination.Value;
                s.OnGoingLinks.Add(d, null);
            }
        }
    }

    

    void InstantiateConfigFile()
    {
        TimeConfigData timeConfigData = new(simulationStartTime, simulationEndTime, timeStep);
        StarConfigData starConfigData = new("Sun", sunRadii["0-Sun"]);
        configData.Time = timeConfigData;
        configData.Star = starConfigData;

        foreach (var kvp in planets)
        {
            Planet p = kvp.Value;
            PlanetConfigData planetsConfigData = new();
            planetsConfigData.Name = p.Name;
            planetsConfigData.Radius = p.Radii;
            planetsConfigData.OrbitPeriod = p.OrbitPeriod;
            planetsConfigData.SemiMajorAxis = p.SemiMajorAxis;
            planetsConfigData.Eccentricity = p.Eccentricity;
            planetsConfigData.Inclination = p.Inclination;
            planetsConfigData.Raan = p.Raan;
            planetsConfigData.ArgOfPeriapsis = p.Argp;
            planetsConfigData.MeanAnomaly = p.MeanAnomaly;
            planetsConfigData.Obliquity = p.Obliquity;
            planetsConfigData.RotationPeriod = p.RotationPeriod;

            configData.Planets.Add(planetsConfigData);
            if (p.nodeList.Count != 0)
            {
                foreach (Node n in p.nodeList)
                {
                    NodeConfigData nodeConfigData = new();
                    nodeConfigData.Name = n.Name;
                    nodeConfigData.ID = n.ID;

                    if (n.Type == "Orbiter")
                        nodeConfigData.OrbitPeriod = n.OrbitPeriod;
                    
                    planetsConfigData.Nodes.Add(nodeConfigData);
                }

                foreach (Moon m in p.moonList)
                {
                    MoonConfigData moonConfigData = new();
                    moonConfigData.Name = m.Name;
                    moonConfigData.Radius = m.Radii;
                    planetsConfigData.Moons.Add(moonConfigData);
                }
            }
        }
        string configToJson = JsonUtility.ToJson(configData, true);
        File.WriteAllText("Assets/Data/JSON/config.json", configToJson);
    }


    private Dictionary<String, Planet> InstantiatePlanets()
    {
        string[] lines = File.ReadAllLines(PlanetFilePath);

        Dictionary<String, Planet> pList = new();

        // Skip the header line
        for (int i = 1; i < lines.Length; i++)
        {
            string line = lines[i];
            string[] fields = line.Split(',');

            if (fields.Length == 11)
            {
                // Parse fields
                string name = fields[0];
                double semiMajorAxis = double.Parse(fields[1]);
                double eccentricity = double.Parse(fields[2]);
                double inclination = double.Parse(fields[3]);
                double raan = double.Parse(fields[4]);
                double argP = double.Parse(fields[5]);
                double meanAnomaly = double.Parse(fields[6]);
                double radii = double.Parse(fields[7]);
                double obliquity = double.Parse(fields[8]);
                double rotationPeriod = double.Parse(fields[9]);
                double mass = double.Parse(fields[10]);
                double orbitPeriod = 2 * Mathf.PI * Mathf.Sqrt(Mathf.Pow((float)semiMajorAxis*1000, 3) / (6.674e-11f*(float)M_sun)); //Semi major axis *1000 because we need it in meters

                // Create Planet object 
                Planet newPlanet = new(name, semiMajorAxis, eccentricity, inclination, raan, argP, meanAnomaly, radii, obliquity, rotationPeriod, orbitPeriod, mass);

                // Update its position
                newPlanet.UpdatePosition(simulationStartTime);
                newPlanet.UpdateRotation(simulationStartTime);

                // Add it to the planet list
                pList.Add(name, newPlanet);
            }
            else
            {
                Console.WriteLine($"Invalid data format in CSV file: {line}");
            }
        }

        return pList;
    }

    private Dictionary<String, Moon> InstantiateMoons()
    {
        string[] lines = File.ReadAllLines(MoonsFilePath);
        Dictionary<String, Moon> mDic = new();

        // Skip the header line
        for (int i = 1; i < lines.Length; i++)
        {
            string line = lines[i];
            string[] fields = line.Split(',');

            if (fields.Length == 12)
            {
                // Parse fields
                string name = fields[0];
                string centralPlanetName = fields[1];
                double semiMajorAxis = double.Parse(fields[2]);
                double eccentricity = double.Parse(fields[3]);
                double inclination = double.Parse(fields[4]);
                double raan = double.Parse(fields[5]);
                double argP = double.Parse(fields[6]);
                double meanAnomaly = double.Parse(fields[7]);
                double radii = double.Parse(fields[8]);
                double obliquity = double.Parse(fields[9]);
                double rotationPeriod = double.Parse(fields[10]);
                double mass = double.Parse(fields[11]);
                double orbitPeriod = 2 * Mathf.PI * Mathf.Sqrt(Mathf.Pow((float)semiMajorAxis * 1000, 3) / (6.674e-11f * (float)M_sun)); //Semi major axis *1000 because we need it in meters

                //Store central object
                Planet centralPlanet = planets[centralPlanetName];

                // Create Moon object 
                Moon newMoon = new(name, centralPlanet, semiMajorAxis, eccentricity, inclination, raan, argP, meanAnomaly, radii, obliquity, rotationPeriod, orbitPeriod, mass);

                // Add it to the planet's moon list
                centralPlanet.moonList.Add(newMoon);

                // Update its position
                newMoon.UpdatePosition(simulationStartTime);
                newMoon.UpdateRotation(simulationStartTime);


                // Add it to the moon list
                mDic.Add(name, newMoon);
            }
            else
            {
                Console.WriteLine($"Invalid data format in CSV file: {line}");
            }
        }
        return mDic;
    }


    private Dictionary<int, Node> InstantiateNodes()
    {
        Dictionary<int, Node> n = new();

        string[] lines = File.ReadAllLines(NodeFilePath);

        for (int i = 0; i < lines.Length; i++)
        {
            string[] cols = lines[i].Split(','); // Split line into fields

            if (cols[0][0] == '#' || cols.Length <= 1)
                continue;

            if (cols[0] == "Lander")
            {
                string nodeType = cols[0];
                string nodeName = cols[1];
                int nodeID = int.Parse(cols[2]);
                string centralObjectName = cols[3];
                float latDeg = float.Parse(cols[4]);
                float lonDeg = float.Parse(cols[5]);
                float altitudeKm = float.Parse(cols[6]);

                Planet centralObject = planets[centralObjectName];
                
                // Positions
                float latRad = (float)latDeg * Mathf.Deg2Rad;
                float longRad = (float)(lonDeg - 90) * Mathf.Deg2Rad;
                float relX = Mathf.Cos(latRad) * Mathf.Cos(longRad);
                float relY = Mathf.Sin(latRad);
                float relZ = Mathf.Cos(latRad) * Mathf.Sin(longRad);

                float newScale = 2 / kmsPerGrid;


                // Relative positions
                relX *= ((float)centralObject.Radii + altitudeKm);
                relY *= ((float)centralObject.Radii + altitudeKm);
                relZ *= ((float)centralObject.Radii + altitudeKm);

                // Node instantiation
                Node newLander = new Node(nodeID, nodeType, nodeName, centralObject);

                newLander.relativeX = relX;
                newLander.relativeY = relY;
                newLander.relativeZ = relZ;

                newLander.UpdateLanderPosition();

                n.Add(nodeID, newLander);
                centralObject.nodeList.Add(newLander);

                //// Link Constraints
                //nodeController.linkConstraints = new string[cols.Length - 6];
                //for (int j = 6; j < cols.Length; j++)
                //{
                //    nodeController.linkConstraints[j - 6] = cols[j];
                //}
            }

            if (cols[0] == "Orbiter")
            {
                string nodeType = cols[0];
                string nodeName = cols[1];
                int nodeID = int.Parse(cols[2]);
                string centralObjectName = cols[3];
                float altitudeKm = float.Parse(cols[4]);
                float inclination = float.Parse(cols[5]);
                float eccentricity = float.Parse(cols[6]);
                float ascNodeLon = float.Parse(cols[7]);
                float argOfPeriapsis = float.Parse(cols[8]);
                float meanAnomalyAtEpoch = float.Parse(cols[9]);
		// If the rules column exists then split the rules into a list, otherwise store an empty list.
                List<string> rules = new List<string>((cols.Length > 10 ? cols[10] : "").Split('|'));

                Planet centralObject = planets[centralObjectName];

                // Instantiate orbiter
                Node newOrbiter = new(nodeID, nodeType, nodeName, centralObject);

                newOrbiter.Rules = rules;

                // Set orbiter parameters
                newOrbiter.SemiMajorAxis = (float)centralObject.Radii + altitudeKm;
                newOrbiter.Eccentricity = eccentricity;
                newOrbiter.Inclination = inclination;
                newOrbiter.AscendingNodeLongitude = ascNodeLon;
                newOrbiter.ArgumentOfPeriapsis = argOfPeriapsis;
                newOrbiter.MeanAnomalyAtEpoch = meanAnomalyAtEpoch;
                newOrbiter.OrbitPeriod = 2* Mathf.PI * Mathf.Sqrt(Mathf.Pow(newOrbiter.SemiMajorAxis*1000, 3) / (6.674e-11f*(float)centralObject.Mass));

                centralObject.nodeList.Add(newOrbiter);

                n.Add(nodeID, newOrbiter);

                newOrbiter.UpdateOrbiterPosition(simulationStartTime);

                //// Link Constraints
                //nodeController.linkConstraints = new string[cols.Length - 9];
                //for (int j = 9; j < cols.Length; j++)
                //{
                //    nodeController.linkConstraints[j - 9] = cols[j];
                //}
            }
        }
        return n;
    }

    private string BuildContactCommand(Contact contact)
    {
        //Debug.Log($"Contact: {contact.Source.Name} to {contact.Destination.Name}. Start Time: {contact.StartTime}, End Time: {contact.EndTime}, Duration: {contact.Duration}");

        double relativeContactStartTime = contact.StartTime - simulationStartTime;
        double relativeContactEndTime = contact.EndTime - simulationStartTime;

        int bitsPerSecond = 1000;

        string contactInfo = $"a contact +{relativeContactStartTime} +{relativeContactEndTime} {contact.Source.ID} {contact.Destination.ID} {bitsPerSecond}\n";
        //Debug.Log(contactInfo);

        return contactInfo;
    }
    
    private string BuildRangeCommand(Contact contact)
    {
        double relativeContactStartTime = contact.StartTime - simulationStartTime;
        double relativeContactEndTime = contact.EndTime - simulationStartTime;

        double rangeInKm = Math.Sqrt(
            Math.Pow(contact.Source.PosX - contact.Destination.PosX, 2)
            + Math.Pow(contact.Source.PosY - contact.Destination.PosY, 2)
            + Math.Pow(contact.Source.PosZ - contact.Destination.PosZ, 2));
        double rangeInLightSeconds = rangeInKm * 1000 / 299792458;

        return $"a range +{relativeContactStartTime} +{relativeContactEndTime} {contact.Source.ID} {contact.Destination.ID} {rangeInLightSeconds.ToString("F4")}\n";
    }
}

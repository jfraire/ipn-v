using System.Collections;
using System.Collections.Generic;

public class Contact
{
    public Node Source;
    public Node Destination;
    public double StartTime;
    public double EndTime;
    public double Duration;
    public double AverageLatency;

    // Constructor
    public Contact(Node source, Node destination, double startTime, double endTime, double duration)
    {
        Source = source;
        Destination = destination;
        StartTime = startTime;
        EndTime = endTime;
        Duration = duration;
        //AverageLatency = averageLatency;
    }
}

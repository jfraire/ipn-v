﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class UtilityLibrary
{
    //////////////////////////////////////
    // Raycast for LoS in double precision
    //////////////////////////////////////
    public static bool ComputeLOS(Node srcNode, Node dstNode, Dictionary<String, Planet> planets)
    {
        bool inLineOfSight = true;

        // 1) Source position: 
        double pos_double_x = srcNode.PosX;
        double pos_double_y = srcNode.PosY;
        double pos_double_z = srcNode.PosZ;

        // 2) Target position: 
        double target_pos_x = dstNode.PosX;
        double target_pos_y = dstNode.PosY;
        double target_pos_z = dstNode.PosZ;


        foreach (var kvp in planets)
        {
            Planet planet = kvp.Value;
            double planet_x = planet.PosX;
            double planet_y = planet.PosY;
            double planet_z = planet.PosZ;
            double planet_radii = planet.Radii;

            // Compute the distance from the planet to the line segment
            double point_distance = PointLineDistance(
                pos_double_x, pos_double_y, pos_double_z,
                target_pos_x, target_pos_y, target_pos_z,
                planet_x, planet_y, planet_z, true
            );

            if (point_distance <= planet_radii)
                inLineOfSight = false;
        }

        return inLineOfSight;
    }

    /////////////////////////////////
    // Point Line Distance Functions
    /////////////////////////////////
    public static double PointLineDistance(
    double ax, double ay, double az,
    double bx, double by, double bz,
    double cx, double cy, double cz,
    bool segmentCheck)
    {
        // Compute vectors
        double[] ab = { bx - ax, by - ay, bz - az };
        double[] ac = { cx - ax, cy - ay, cz - az };
        double[] bc = { cx - bx, cy - by, cz - bz };

        // Ensure ab is not a zero vector (A and B are not the same point)
        double abMagnitude = Magnitude(ab);
        if (abMagnitude == 0.0)
        {
            Debug.LogError("Points A and B are identical; cannot compute line segment.");
            return double.MaxValue; 
        }

        // Compute the cross product ac x ab
        double[] cross_product = {
        ac[1] * ab[2] - ac[2] * ab[1],
        ac[2] * ab[0] - ac[0] * ab[2],
        ac[0] * ab[1] - ac[1] * ab[0]
    };

        // Compute the distance
        double distance = Magnitude(cross_product) / abMagnitude;

        // Check if the closest point is within the segment AB
        if (segmentCheck)
        {
            double t = DotProduct(ab, ac) / DotProduct(ab, ab);
            if (t < 0 || t > 1)
            {
                // The closest point is not within the segment AB
                distance = double.MaxValue;
            }
        }

        return distance;
    }

    public static double DotProduct(double[] a, double[] b)
    {
        return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
    }

    public static double Magnitude(double[] a)
    {
        return System.Math.Sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
    }

}

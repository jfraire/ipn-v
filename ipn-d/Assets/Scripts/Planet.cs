using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet
{
    public string Name;

    // Positions are expressed in heliocentric coordinates
    public double PosX;
    public double PosY;
    public double PosZ;

    // Rotations are expressed relatively to the world's global frame
    public double RotationX = 0;
    public double RotationY = 0;
    public double RotationZ = 0;

    public double SemiMajorAxis;           // Semi-major axis (km)
    public double Eccentricity;           // Eccentricity
    public double Inclination;           // Inclination (deg)
    public double Raan;        // Right ascension of the ascending node (deg)
    public double Argp;        // Argument of periapsis (deg)
    public double MeanAnomaly; // Mean anomaly (deg) at J2000 epoch
    public double Radii;
    public double Obliquity;
    public double RotationPeriod;

    public double OrbitPeriod;

    public double Mass; //kg

    //Constants
    public double G = 6.6743e-20; // Gravitational constant (km^3/kg/s^2)
    public double M_sun = 1.9885e30; // Mass of the Sun (kg)

    //List of every node of the planet
    public List<Node> nodeList = new List<Node>();
    public List<Moon> moonList = new List<Moon>();

    //List of positions
    public List<String> positionList = new();

    //Json export
    public IPNcController.PlanetPositionData JSONpositions = new();

    //Test
    //public List<Vector3> posTest = new();

    public Planet(string name, double a, double e, double i, double raan, double argp, double meanAnomaly, double radii, double obl, double rp, double op, double m)
    {
        Name = name;
        SemiMajorAxis = a;
        Eccentricity = e;
        Inclination = i;
        Raan = raan;
        Argp = argp;
        MeanAnomaly = meanAnomaly;
        Radii = radii;
        Obliquity = obl;
        RotationPeriod = rp;
        OrbitPeriod = op;
        Mass = m;
    }



    /// <summary>
    /// Compute the (x,y,z) absolute coordinates of the planet at a given time
    /// </summary>
    /// <param name="time"></param>
    public void UpdatePosition(double time)
    {
        // Calculate the mean motion (n)      
        double n = Math.Sqrt(G * M_sun / Math.Pow(SemiMajorAxis, 3));

        // Calculate the mean anomaly (M)
        double mean_anomaly_rad = MeanAnomaly * Math.PI / 180; //
        double M = mean_anomaly_rad + n * time;

        // Calculate the eccentric anomaly (E) using Newton-Raphson iteration
        double E = M; // Initial guess
        while (true)
        {
            double E_new = E - (E - Eccentricity * Math.Sin(E) - M) / (1 - Eccentricity * Math.Cos(E));
            if (Math.Abs(E_new - E) < 1e-8)
            { // Convergence criteria
                E = E_new;
                break;
            }
            E = E_new;
        }

        // Calculate the true anomaly (nu)
        double nu = 2 * Math.Atan(Math.Sqrt((1 + Eccentricity) / (1 - Eccentricity)) * Math.Tan(E / 2));

        // Calculate the distance from the Sun (r)
        double r = SemiMajorAxis * (1 - Eccentricity * Math.Cos(E));

        // Calculate the position in the orbital plane (x', y')
        double x_prime = r * Math.Cos(nu);
        double y_prime = r * Math.Sin(nu);

        // Calculate the position in 3D heliosynchronous coordinates (x, y, z)
        double i_rad = Inclination * Math.PI / 180;
        double raan_rad = Raan * Math.PI / 180;
        double argp_rad = Argp * Math.PI / 180;

        double newX = x_prime * (Math.Cos(raan_rad) * Math.Cos(argp_rad) - Math.Sin(raan_rad) * Math.Sin(argp_rad) * Math.Cos(i_rad)) - y_prime * (Math.Sin(raan_rad) * Math.Cos(argp_rad) + Math.Cos(raan_rad) * Math.Sin(argp_rad) * Math.Cos(i_rad));
        double newY = x_prime * (Math.Cos(raan_rad) * Math.Sin(argp_rad) + Math.Sin(raan_rad) * Math.Cos(argp_rad) * Math.Cos(i_rad)) + y_prime * (Math.Cos(raan_rad) * Math.Cos(argp_rad) * Math.Cos(i_rad) - Math.Sin(raan_rad) * Math.Sin(argp_rad));
        double newZ = x_prime * (Math.Sin(raan_rad) * Math.Sin(i_rad)) + y_prime * (Math.Cos(raan_rad) * Math.Sin(i_rad));

        PosX = newX;
        PosY = newZ;
        PosZ = newY;
    }

    public void UpdateRotation(double time)
    {

        // Calculate the rotation about the planet's local Y axis.
        double localRotationY = 360.0 / (RotationPeriod * 24.0 * 60.0 * 60.0) * time;
        localRotationY = ((localRotationY % 360) + 360) % 360; // return a positive angle between 0 and 360

        // Calculate the tilt of the planet's local frame using its obliquity. In practice, tilt is a rotation in 
        // degrees (the value of the angle is the obliquity) about the forward axis.
        Quaternion tilt = Quaternion.AngleAxis((float)Obliquity, Vector3.forward);

        // Express the planet's rotation in its local frame.
        Quaternion localRotation = Quaternion.Euler(0, (float)localRotationY, (float)Obliquity);

        // Express the planet's rotation in the global frame.
        // Formula breakdown:
        // - Quaternion.Inverse(tilt) transforms from the global frame to the local frame,
        // - localRotation applies the rotation in the local frame,
        // - tilt transforms back to the global frame.
        // Be aware that multiplying quaternions is equivalent to applying one rotation after the other
        Quaternion globalRotation = tilt * localRotation * Quaternion.Inverse(tilt);

        // Get the euler angles from the Quaternion.
        Vector3 eulerGlobalRotation = globalRotation.eulerAngles;

        // Set the angles variables.
        RotationX = eulerGlobalRotation.x;
        RotationY = eulerGlobalRotation.y;
        RotationZ = eulerGlobalRotation.z;
    }

}
